<html lang="en">

<head>
    <title>Create_PW</title>
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css" rel="stylesheet" type="text/css" />
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="#">
                        <div class="p-5"
                            style="background-color:#ffffff; padding: 45px 0 34px 0;   max-width: 500px;">
                            <form action="" class="p-5">
                                <h1>Complete Registration For</h1>
                                <p class="pb-10">username@mail.com</p>
                                <label for="create password" class="form-label">Create Password</label>
                                <div class="input-group d-flex"><input type="password" placeholder="Enter Password"
                                        class="form-control w-90  p-3">
                                    <span class="input-group-text"><a href="#."><i class="fas fa-eye"></i></a></span>
                                </div>
                                <label for="retype password" class="form-label mt-9">Retype Password</label>
                                <div class="input-group d-flex"><input type="password" placeholder="Retype Password"
                                        class="form-control w-90  p-3">
                                    <span class="input-group-text"><a href="#."><i class="fas fa-eye"></i></a></span>
                                </div>
                                <div class="d-flex justify-content-end mt-20 ">
                                    <a type="submit" data-kt-contacts-type="submit" class="btn btn-primary" href="basic_dashboard.php">
                                        <span class="indicator-label">Sign In</span>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>var hostUrl = "../assets/";</script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>