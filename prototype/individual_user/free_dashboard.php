<?php include("header_free.php") ?>

<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Home</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
    </div>
    <!--end::Toolbar container-->
</div>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid pt-0 pb-0" id="kt_content">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Row First Section-->
            <div class="row g-5 mx-0 g-xxl-8">
                <!--begin::Col-->
                <div class="col-xl-6 ps-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading">
                                <h3 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                                    Contracts Summary</h3>
                            </div>
                        </div>
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">8140</div>
                                        <div class="fw-bold text-dark-100">Used</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">140</div>
                                        <div class="fw-bold text-dark-100">Draft</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0 ">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">1340</div>
                                        <div class="fw-bold text-dark-100">Under-Review</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">2233</div>
                                        <div class="fw-bold text-dark-100">Waiting for Sign</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">4312</div>
                                        <div class="fw-bold text-dark-100">Ready</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">108</div>
                                        <div class="fw-bold text-dark-100">Delete</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">05</div>
                                        <div class="fw-bold text-dark-100">Canceled</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0 ">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">02</div>
                                        <div class="fw-bold text-dark-100">Rejected</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>

                        </div>
                    </div>
                    <!--end::Charts Widget 1-->
                </div>
                <!--end::Col-->


                <!--begin::Col-->
                <div class="col-xl-6 pe-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading">
                                <h3 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Contracts Status</h3>
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">11000</div>
                                        <div class="fw-bold text-dark-100">Total</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">8140</div>
                                        <div class="fw-bold text-dark-100">Used</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">2860</div>
                                        <div class="fw-bold text-dark-100">Available</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="d-flex flex-column w-100 mb-7">
                                <div class="d-flex flex-stack mb-2 justify-content-between">
                                    <h6>Usage Percentage</h6>
                                    <span class="text-muted fs-7 fw-bold percent_bar">50%</span>
                                </div>
                                <div class="progress h-6px w-100">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--<div id="kt_mixed_widget_14_chart" class="mt-0"
                                                    style="height: 200px; min-height: 178.7px;">
                                                    <div id="apexchartsewz89b5j"
                                                        class="apexcharts-canvas apexchartsewz89b5j apexcharts-theme-light d-flex justify-content-center">
                                                        <svg id="SvgjsSvg1350" width="254" height="178.7"
                                                            xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                                            xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                                            style="background: transparent;">
                                                            <g id="SvgjsG1352"
                                                                class="apexcharts-inner apexcharts-graphical"
                                                                transform="translate(40, 0)">
                                                                <defs id="SvgjsDefs1351">
                                                                    <clipPath id="gridRectMaskewz89b5j">
                                                                        <rect id="SvgjsRect1354" width="182"
                                                                            height="200" x="-3" y="-1" rx="0" ry="0"
                                                                            opacity="1" stroke-width="0" stroke="none"
                                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                    <clipPath id="gridRectMarkerMaskewz89b5j">
                                                                        <rect id="SvgjsRect1355" width="180"
                                                                            height="202" x="-2" y="-2" rx="0" ry="0"
                                                                            opacity="1" stroke-width="0" stroke="none"
                                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                </defs>
                                                                <g id="SvgjsG1356" class="apexcharts-radialbar">
                                                                    <g id="SvgjsG1357">
                                                                        <g id="SvgjsG1358" class="apexcharts-tracks">
                                                                            <g id="SvgjsG1359"
                                                                                class="apexcharts-radialbar-track apexcharts-track"
                                                                                rel="1">
                                                                                <path id="apexcharts-radialbarTrack-0"
                                                                                    d="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 87.98928506193984 26.607927764323023"
                                                                                    fill="none" fill-opacity="1"
                                                                                    stroke="rgba(201,247,245,0.85)"
                                                                                    stroke-opacity="1"
                                                                                    stroke-linecap="round"
                                                                                    stroke-width="8.97439024390244"
                                                                                    stroke-dasharray="0"
                                                                                    class="apexcharts-radialbar-area"
                                                                                    data:pathorig="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 87.98928506193984 26.607927764323023">
                                                                                </path>
                                                                            </g>
                                                                        </g>
                                                                        <g id="SvgjsG1361">
                                                                            <g id="SvgjsG1365"
                                                                                class="apexcharts-series apexcharts-radial-series"
                                                                                seriesname="Progress" rel="1"
                                                                                data:realindex="0">
                                                                                <path id="SvgjsPath1366"
                                                                                    d="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 26.757474833957374 92.28249454023158"
                                                                                    fill="none" fill-opacity="0.85"
                                                                                    stroke="rgba(27,197,189,0.85)"
                                                                                    stroke-opacity="1"
                                                                                    stroke-linecap="round"
                                                                                    stroke-width="8.97439024390244"
                                                                                    stroke-dasharray="0"
                                                                                    class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                                    data:angle="266" data:value="74"
                                                                                    index="0" j="0"
                                                                                    data:pathorig="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 26.757474833957374 92.28249454023158"
                                                                                    selected="false"></path>
                                                                            </g>
                                                                            <circle id="SvgjsCircle1362"
                                                                                r="56.9048780487805" cx="88" cy="88"
                                                                                class="apexcharts-radialbar-hollow"
                                                                                fill="transparent"></circle>
                                                                            <g id="SvgjsG1363"
                                                                                class="apexcharts-datalabels-group"
                                                                                transform="translate(0, 0) scale(1)"
                                                                                style="opacity: 1;"><text
                                                                                    id="SvgjsText1364"
                                                                                    font-family="Helvetica, Arial, sans-serif"
                                                                                    x="88" y="100" text-anchor="middle"
                                                                                    dominant-baseline="auto"
                                                                                    font-size="30px" font-weight="700"
                                                                                    fill="#5e6278"
                                                                                    class="apexcharts-text apexcharts-datalabel-value"
                                                                                    style="font-family: Helvetica, Arial, sans-serif;">74%</text>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                                <line id="SvgjsLine1367" x1="0" y1="0" x2="176" y2="0"
                                                                    stroke="#b6b6b6" stroke-dasharray="0"
                                                                    stroke-width="1" class="apexcharts-ycrosshairs">
                                                                </line>
                                                                <line id="SvgjsLine1368" x1="0" y1="0" x2="176" y2="0"
                                                                    stroke-dasharray="0" stroke-width="0"
                                                                    class="apexcharts-ycrosshairs-hidden"></line>
                                                            </g>
                                                            <g id="SvgjsG1353" class="apexcharts-annotations"></g>
                                                        </svg>
                                                        <div class="apexcharts-legend"></div>
                                                    </div>
                                                </div>-->
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row First Section-->

            <!--begin::Row Second Section-->
            <div class="row g-5 mx-0 g-xxl-8" style="margin-top: 0px; margin-bottom: 50px;">
                <!--begin::Col Categorywise-->
                <div class="col-xl-5 mb-7 ps-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="card-header align-items-center border-0 payment_sum mt-0 px-4">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Payment
                                        Summary</span>
                                </h3>
                                <div class="d-flex align-items-center">
                                    <input type="date" class="form-control form-control-sm form-control-solid me-3">
                                    <a href="#.">
                                        <i class="fas fa-filter fs-4 text-hover-muted" style="color:#0b3856;"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- <div class="col-xl-12" style="margin-top: 0px; margin-bottom: -20px !important;">
                                                    <a href="premium_contract.php"
                                                        class="card bg-secondary bg-opacity-25">
                                                        <div class="card-body p-3">
                                                            <div class="text-dark-100 fw-bolder fs-2 mb-2">Total Users</div>
                                                            <div class="fw-bold text-dark-100">KWD 8140.000</div>
                                                        </div>
                                                    </a>
                                                </div> -->
                            <div class="col-xl-12 mt-0 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Total</div>
                                        <div class="fs-5">KWD 11000.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Membership - Basic</div>
                                        <div class="fs-5">KWD 8140.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-0">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Contracts</div>
                                        <div class="fs-5">KWD 2860.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Col Categorywise-->


                <!--begin::Col Notifications-->
                <div class="col-xl-7 mb-7 pe-0">
                    <div class="card mb-5 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading d-flex justify-content-between">
                                <h3 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                                    Notifications</h3>
                                <a href="free_notification.php">View All</a>
                            </div>
                            <div class="col-xl-12 mt-5 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_notification.php" class="card bg-secondary bg-opacity-25 dash_notifi_main">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2 dash_notifi_inr1">
                                            <i class="far fa-bell fa-3x text-primary"></i>Your
                                            membership plan expired in 2 days.
                                        </div>
                                        <div class="fw-bold text-dark-100 dash_notifi_inr2">Review now
                                            and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_notification.php" class="card bg-secondary bg-opacity-25 dash_notifi_main">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2 dash_notifi_inr1">
                                            <i class="far fa-bell fa-3x text-primary"></i>Your
                                            membership plan expired in 2 days.
                                        </div>
                                        <div class="fw-bold text-dark-100 dash_notifi_inr2">Review now
                                            and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-0">
                                <!--begin::Statistics Widget 5-->
                                <a href="free_notification.php" class="card bg-secondary bg-opacity-25 dash_notifi_main">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2 dash_notifi_inr1">
                                            <i class="far fa-bell fa-3x text-primary"></i>Your
                                            membership plan expired in 2 days.
                                        </div>
                                        <div class="fw-bold text-dark-100 dash_notifi_inr2">Review now
                                            and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Col Notifications-->
            </div>
            <!--end::Row Second Section-->

        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->





<!-- Start :: script Area-->



<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>


<?php include("footer_free.php") ?>