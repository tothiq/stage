<?php include("header_basic.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Create Contract</h1>
            <!-- <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
							<li class="breadcrumb-item text-muted">
								<a href="basic_dashboard.php" class="text-muted text-hover-primary">Dashboard</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">
								<a href="basic_contract.php" class="text-muted text-hover-primary">Contract</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Create Contracts</li>
						</ul> -->
        </div>
    </div>
</div>
<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 align-items-center">
                <div class="create_contract_step">
                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
                        <li class="nav-item">
                            <a class="nav-link">Step-1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active border-bottom-2 border-primary">Step-2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Step-3</a>
                        </li>
                    </ul>
                </div>
                <div class="create_contract_btn d-flex align-items-center mb-5">
                    <div class="form-check form-check-solid form-switch form-check-custom fv-row" style="margin-right: 10px;">
                        <label class="form-check-label fs-6" for="allowmarketing" style="color: #3f4254;font-weight: 500;">Edit : </label>
                        <input class="form-check-input w-45px h-30px ms-3" type="checkbox" id="allowmarketing" checked="">
                    </div>
                    <a href="basic_create_cont_step3.php" class="btn btn-sm btn-primary" style="margin-right: 10px;">Continue</a>
                    <a href="basic_create_cont_step1.php" class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">Cancel</a>
                </div>
            </div>
            <div class="tab-content" id="myTabContent">
                <!--begin:::Tab pane-->
                <div class="tab-pane fade active show" id="all_contrcats" role="tabpanel">
                    <form id="kt_modal_new_target_form" class="form" action="#">
                        <!--begin::Heading-->
                        <div class="mb-13">
                            <!--begin::Title-->
                            <h1 class="mb-3">Contract Information</h1>
                            <!--end::Description-->
                        </div>
                        <!--end::Heading-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="form-label fs-6 fw-bolder text-dark">
                                <span class="required">Contract Title</span>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control" placeholder="Enter Contract Title" name="target_title">
                        </div>
                        <div class="row mx-0 mb-8">
                            <div class="col-6 ps-0">
                                <!--begin::Label-->
                                <label class="d-flex align-items-center fs-6 fw-bolder text-dark mb-2">
                                    <span class="required">Contract Folders</span>
                                </label>
                                <!--end::Label-->
                                <select class="form-control" id="multilevel-select">
                                    <option value="">Select Folder</option>
                                    <optgroup label="General Folders"></optgroup>
                                    <optgroup label="Rental Aggrement">
                                        <option value="">Home Rent Aggrement</option>
                                        <option value="">Office Rent Aggrement</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-6 pe-0">
                                <!--begin::Label-->
                                <label class="d-flex align-items-center fs-6 fw-bolder text-dark mb-2">
                                    <span class="required">Categories</span>
                                </label>
                                <!--end::Label-->
                                <select class="form-select" aria-label="Select example" placeholder="">
                                    <option value="">Select Category</option>
                                    <option value="1">Accounting & Tax</option>
                                    <option value="2">Agreements</option>
                                    <option value="3">Buisness & Plans</option>
                                    <option value="4">Construction</option>
                                    <option value="5">Consulting</option>
                                    <option value="6">Contracts</option>
                                    <option value="7">Employment</option>
                                    <option value="8">Financial</option>
                                    <option value="9">Healthcare</option>
                                    <option value="10">Manufacturing</option>
                                    <option value="11">Marketing</option>
                                    <option value="12">Real Estate</option>
                                    <option value="13">Sales</option>
                                    <option value="14">Software</option>
                                    <option value="15">Tax form templates</option>
                                    <option value="16">Technology</option>
                                </select>
                            </div>
                        </div>
                        <div class="d-flex flex-column mb-8">
                            <label class="form-label fs-6 fw-bolder text-dark">Contract Description</label>
                            <textarea class="form-control" rows="10" name="target_details" placeholder="Enter Contract Description"></textarea>
                        </div>
                        <div class="row g-9 mb-8">
                            <label class="form-label fs-6 fw-bolder text-dark">Contract Duration</label>
                            <!--begin::Col-->
                            <div class="col-md-3 fv-row m-0">
                                <label class="form-label fs-6 fw-bolder text-dark">Start</label>
                                <input class="form-control " placeholder="Pick date rage" id="kt_daterangepicker_3" />
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col-md-3 fv-row m-0">
                                <label class="form-label fs-6 fw-bolder text-dark">End</label>
                                <input class="form-control " placeholder="Pick date rage" id="kt_daterangepicker_4" />
                            </div>
                            <div class="col-6"></div>
                            <!--end::Col-->
                        </div>
                        <div class="col-md-3 mb-8 fv-row">
                            <label class="fs-6 fw-bolder text-dark mb-2">Contract Valuation</label>
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bolder text-dark mb-2">
                                <span class="">KWD</span>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control" placeholder="Enter KWD" name="target_title">
                        </div>
                    </form>
                </div>
                <!--end:::Tab pane-->
            </div>
        </div>
    </div>
</div>

<!--end::Root-->

<!-- Modal Invite Contact  -->
<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100">
                    <div class=" d-flex flex-wrap">
                        <div class="fv-row p-5 col-12">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Email Address" autocomplete="off" />
                        </div>
                    </div>
                    <div class="text-center btncolorblue pt-10">
                        <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Invite Contact  -->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>

<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>
<script>
    $("#kt_daterangepicker_3").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format("YYYY"), 10)
    }, function(start, end, label) {
        var years = moment().diff(start, "years");
        alert("You are " + years + " years old!");
    });
</script>
<script>
    $("#kt_daterangepicker_4").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format("YYYY"), 10)
    }, function(start, end, label) {
        var years = moment().diff(start, "years");
        alert("You are " + years + " years old!");
    });
</script>
<?php include("footer_basic.php") ?>