<?php include("header_premium.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Create Contract</h1>
            <!-- <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="premium_dashboard.html" class="text-muted text-hover-primary">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
							<li class="breadcrumb-item text-muted">
                                <a href="premium_contract.html" class="text-muted text-hover-primary">Contracts</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Create Contracts</li>
                        </ul> -->
        </div>
    </div>
</div>
<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 align-items-center">
                <div class="create_contract_step">
                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
                        <li class="nav-item">
                            <a class="nav-link">Step-1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Step-2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active border-bottom-2 border-primary">Step-3</a>
                        </li>
                    </ul>
                </div>
                <div class="create_contract_btn d-flex align-items-center mb-5">
                    <div class="form-check form-check-solid form-switch form-check-custom fv-row" style="margin-right: 10px;">
                        <label class="form-check-label fs-6" for="allowmarketing" style="color: #3f4254;font-weight: 500;">Edit : </label>
                        <i class="fa-solid fa-check fa-xl" style="color:#183052;margin-left:5px;"></i>
                    </div>
                    <a href="premium_cont_new&draft.php" class="btn btn-sm btn-primary" style="margin-right:10px;">Continue</a>
                    <a href="premium_create_cont_step2.php" class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">Cancel</a>
                </div>
            </div>
            <div class="tab-content" id="myTabContent">
                <!--begin:::Tab pane-->
                <div class="tab-pane fade active show" id="all_contrcats" role="tabpanel">
                    <form id="kt_modal_new_target_form" class="form mb-10" action="#">
                        <div class="card">
                            <div class="card-body">
                                <div class="">
                                    <h1 class="mb-3">First Parties</h1>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row py-5 col-6">
                                        <label class="form-label fs-6 fw-bolder text-dark">Party Email</label>
                                        <input type="text" class="form-control form-control-white border-secondary" placeholder="name@example.com" />
                                    </div>
                                    <div class="fv-row p-5 col-5">
                                        <label class="form-label fs-6 fw-bolder text-dark">Party Full Name</label>
                                        <input type="text" class="form-control form-control-white border-secondary" placeholder="Alen Jeo" />
                                    </div>
                                    <div class="fv-row p-5 ps-0 col-1 d-flex align-items-end">
                                        <a href="#." class="btn btn-sm btn-primary text-white m-2"><i class="fas fa-plus fa-lg p-0"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mt-5">
                            <div class="card-body">
                                <div class="">
                                    <h1 class="mb-3">Second Parties</h1>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 ps-0 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Civil ID Number</label>
                                        <input type="text" class="form-control form-control-white border-secondary" placeholder="123412341234" />
                                    </div>
                                    <div class="fv-row py-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Name</label>
                                        <input type="text" class="form-control form-control-white border-secondary" placeholder="Alen Jeo" />
                                    </div>
                                    <div class="fv-row p-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                                        <input type="text" class="form-control form-control-white border-secondary" placeholder="username@mail.com" />
                                    </div>
                                    <div class="fv-row p-5 ps-0 col-1 d-flex align-items-end">
                                        <a href="#." class="btn btn-sm btn-primary text-white m-2"><i class="fas fa-plus fa-lg p-0"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--end:::Tab pane-->
            </div>
        </div>
    </div>
</div>

<!--end::Root-->

<!-- Modal Invite Contact  -->
<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100">
                    <div class=" d-flex flex-wrap">
                        <div class="fv-row p-5 col-12">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Email Address" autocomplete="off" />
                        </div>
                    </div>
                    <div class="text-center btncolorblue pt-10">
                        <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Invite Contact  -->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>

<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_premium.php") ?>