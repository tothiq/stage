<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/iu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <div class="d-flex flex-column flex-root">
        <div class="page d-flex flex-row">
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="new_draft kt_header" class="header align-items-stretch">
                    <!-- <div class="row m-0 align-items-center border border-3"> -->
                    <div class="col-1 d-flex align-items-center p-5">
                            <a href="premium_contract.php"><i class="fas fa-angle-double-left fs-1"></i></i></a>
                        </div>
                    <div class="col-5 d-flex justify-content-center align-items-center">
                        <a href="#." class="text-decoration-underline text-primary fs-5 fw-bold" data-bs-toggle="modal"
                            data-bs-target="#create_contract"><i class="fas fa-pen-square fa-lg text-primary"></i></a>
                        <h3 class="mb-0 ms-5">Rental Aggrement For Al-John Tower Office With Tothiq</h3>
                    </div>
                    <div
                        class="col-2 d-flex justify-content-center align-items-center border border-top-0 border-bottom-0 border-3 ps-4">
                        <i class="fas fa-users fa-lg text-warning"></i>
                        <h3 class="mb-0 mx-4">2 Parties</h3>
                        <a href="#." class="text-decoration-underline text-primary fs-5 fw-bold" data-bs-toggle="modal"
                            data-bs-target="#create_contract_1"><i class="fas fa-pen-square fa-lg text-primary"></i></a>
                    </div>
                    <div class="col-4 d-flex  mx-3 align-items-center ">
                        <div class="create_contract_btn ms-20">
                            <button class="btn btn-sm btn-outline btn-outline-danger btn-active-light-danger"
                                data-bs-toggle="modal" type="button" href="#exampleModalToggle4">Save as Draft</button>
                            <a href="premium_upload_cont_under_review_stage.php"
                                class="btn btn-sm btn-primary ms-4 me-5 ">Submit for
                                Review</a>
                        </div>
                    </div>
                    <!-- </div> -->
                    <div id="new_draft kt_aside" class="aside aside-light aside-hoverable bg-secondary"
                        data-kt-drawer="true" data-kt-drawer-name="aside"
                        data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                        data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start"
                        data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                        <div class="position-relative ">
                            
                            <div class="aside-menu flex-column-fluid h-100 ">

                                <div class="">
                                    <div class="contract_left_sec border border-top-0 p-0  ">
                                        <ul class="nav border-0 flex-row flex-md-column mb-3 mb-md-0 fs-6">
                                            <li
                                                class="nav-item  me-0 border-bottom border-3 p-4 d-flex justify-content-between">
                                                <a class="fs-3 fw-bold text-dark">Comments</a>
                                                <i class="fas fa-expand-alt fa-lg align-items-center d-flex"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card  " id="kt_chat_messenger" style="margin-top: 580px;">
                                        <div>
                                            <div class="under_review card-footer p-5 d-flex  "
                                                id="kt_chat_messenger_footer" >
                                                <!--begin::Input-->
                                                <textarea class="form-control p-2 " placeholder="Type your comment"
                                                    rows="1" id="floatingTextarea"></textarea>
                                                <!--end::Input-->
                                                <!--begin:Toolbar-->
                                                <div
                                                    class="d-flex flex-stack align-items-center justify-content-center">
                                                    <!--begin::Actions-->
                                                    <div class="d-flex align-items-center ms-5" style="padding-left:30px;">
                                                        <button
                                                            class="btn bg-primary btn-sm btn-icon btn-active-dark me-1"
                                                            type="button" data-bs-toggle="tooltip" title=""
                                                            data-bs-original-title="">
                                                            <i class="bi bi-upload fs-3 text-white"></i>
                                                        </button>
                                                    </div>
                                                    <!--end::Actions-->
                                                </div>
                                                <!--end::Toolbar-->
                                            </div>

                                            <!--end::Card footer-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="position-relative">
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" class="container-xxl mt-5">
                        <div class="card mx-5 mt-5 mb-20">
                            <div class="card-body">
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Stactic content start</span>
                                </div>
                                <h2 class="lh-base fw-700 mb-15">Rental Agreement For Al-John<br>Tower Office
                                    with Tothiq</h2>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Starting Date : 01-01-2023
                                        12:00:00</span>
                                </div>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Period : 1 Year</span>
                                </div>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Value : KWD 12000.000</span>
                                </div>
                                <div class="card_table my-15">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr class="fw-bold fs-6 text-gray-800">
                                                    <th class="border border-1 border-secondary p-2">Parties
                                                        Information</th>
                                                    <th class="border border-1 border-secondary p-2">Name Of
                                                        Person</th>
                                                </tr>
                                            </thead>
                                            <tbody class="border border-1 border-secondary p-2">
                                                <tr>
                                                    <td class="border border-1 border-secondary p-2">First Party
                                                    </td>
                                                    <td class="border border-1 border-secondary p-2">Aabirah
                                                        Aadab Aadil</td>
                                                </tr>
                                                <tr>
                                                    <td class="border border-1 border-secondary p-2">Second
                                                        Party</td>
                                                    <td class="border border-1 border-secondary p-2">Aadil
                                                        Aabirah Aadab</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Stactic content end</span>
                                </div>
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Upload Document will come here</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer py-4 d-flex  flex-lg-column app-footer position-fixed bottom-0 end-0 w-100" id="kt_footer">
                <!--begin::Container-->
                <div
                    class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                            Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
        </div>
    </div>
    <div class="modal fade" id="create_contract" tabindex="-1" aria-modal="true" role="dialog">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-900px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2>Edit</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="black"></rect>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body py-lg-10 px-lg-10">
                    <div class="popup_select_parent_folder mt-5 mb-5">
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Contract Title</span>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control" placeholder="Enter Contract Title"
                                name="target_title">
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Contract Category</span>
                            </label>
                            <!--end::Label-->
                            <select class="form-select" aria-label="Select example" placeholder="">
                                <option value="1">All</option>
                                <option value="2">Accounting &amp; Tax</option>
                                <option value="4">Agreements</option>
                                <option value="5">Buisness &amp; Plans</option>
                                <option value="6">Construction</option>
                                <option value="7">Consulting</option>
                                <option value="8">Contracts</option>
                                <option value="9">Employment</option>
                                <option value="10">Financial</option>
                                <option value="11">Healthcare</option>
                                <option value="12">Manufacturing</option>
                                <option value="13">Marketing</option>
                                <option value="14">Real Estate</option>
                                <option value="15">Sales</option>
                                <option value="16">Software</option>
                                <option value="17">Tax form templates</option>
                                <option value="18">Technology</option>
                            </select>
                        </div>
                        <div class="d-flex flex-column mb-8">
                            <label class="fs-6 fw-bold mb-2">Contract Description</label>
                            <textarea class="form-control" rows="10" name="target_details" placeholder="Enter Contract Description"></textarea>
                        </div>
                        <div class="row g-9 mb-8">
                            <label class="fs-6 fw-bold mb-2">Contract Duration</label>
                            <!--begin::Col-->
                            <div class="col-md-3 fv-row m-0">
                                <label class="required fs-6 fw-bold mb-2">Start</label>
                                <input class="form-control " placeholder="Pick date rage" id="kt_daterangepicker_3"/>
                            </div>
                            <!--end::Col-->
                            <!--begin::Col-->
                            <div class="col-md-3 fv-row m-0">
                                <label class="required fs-6 fw-bold mb-2">End</label>
                                <input class="form-control " placeholder="Pick date rage" id="kt_daterangepicker_4"/>
                            </div>
                            <div class="col-6"></div>
                            <!--end::Col-->
                        </div>
                        <div class="col-md-3 mb-8 fv-row">
                            <label class="fs-6 fw-bold mb-2">Contract Valuation</label>
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">KWD</span>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control" placeholder="Enter KWD"
                                name="target_title">
                        </div>
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                <span class="required">Folder Name</span>
                            </label>
                            <!--end::Label-->
                            <select class="form-select" aria-label="Select example" placeholder="">
                                <option value="1">General Folder</option>
                                <option value="2">Rental Aggrement</option>
                            </select>
                        </div>
                        <div class="d-flex justify-content-end mt-5">
                            <a href="basic_cont_new&draft.php" class="btn btn-primary btn-sm">Save</a>
                        </div>
                    </div>
                    <!-- <div class="popup_select_folder_name my-10">
                        <h3>Folder Name</h3>
                        <input type="text" class="form-control" placeholder="name@example.com" />
                    </div>
                    <div class="popup_create_folder_btn d-flex justify-content-end">
                        <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal"
                            data-bs-target="#create_contract">Create Folder</a>
                    </div> -->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
    <div class="modal fade" id="create_contract_1" tabindex="-1" aria-modal="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered mw-900px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Edit</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="black"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="popup_select_parent_folder mt-5 mb-5">
                        <form id="kt_modal_new_target_form" class="form mb-10" action="#">
                            <div class="card">
                                <div class="card-body py-0">
                                    <div class="">
                                        <h1 class="mb-3">First Parties</h1>
                                    </div>
                                    <div class="d-flex flex-wrap">
                                        <div class="fv-row py-5 col-6">
                                            <label class="form-label fs-6 fw-bolder text-dark">Party Email</label>
                                            <input type="text" class="form-control form-control-white border-secondary" placeholder="name@example.com"/>
                                        </div>
                                        <div class="fv-row p-5 col-5">
                                            <label class="form-label fs-6 fw-bolder text-dark">Party Full Name</label>
                                            <input type="text" class="form-control form-control-white border-secondary" placeholder="Alen Jeo"/>
                                        </div>
                                        <div class="fv-row p-5 ps-0 col-1 d-flex align-items-end">
                                            <a href="#." class="btn btn-sm btn-primary text-white m-2"><i class="fas fa-plus fa-lg p-0"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-5">
                                <div class="card-body">
                                    <div class="">
                                        <h1 class="mb-3">Second Parties</h1>
                                    </div>
                                    <div class="d-flex flex-wrap">
                                        <div class="fv-row py-5 col-6">
                                            <label class="form-label fs-6 fw-bolder text-dark">Party Email</label>
                                            <input type="text" class="form-control form-control-white border-secondary" placeholder="name@example.com"/>
                                        </div>
                                        <div class="fv-row p-5 col-5">
                                            <label class="form-label fs-6 fw-bolder text-dark">Party Full Name</label>
                                            <input type="text" class="form-control form-control-white border-secondary" placeholder="Alen Jeo"/>
                                        </div>
                                        <div class="fv-row p-5 ps-0 col-1 d-flex align-items-end">
                                            <a href="#." class="btn btn-sm btn-primary text-white m-2"><i class="fas fa-plus fa-lg p-0"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="d-flex justify-content-end mt-5">
                            <a href="basic_cont_new&draft.php" class="btn btn-primary btn-sm">Save</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Invite Contact  -->
    <div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2"
        tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="form w-100">
                        <div class=" d-flex flex-wrap">
                            <div class="fv-row p-5 col-12">
                                <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                                <input class="form-control form-control-lg form-control-solid" type="email"
                                    placeholder="Email Address" autocomplete="off" />
                            </div>
                        </div>
                        <div class="text-center btncolorblue pt-10">
                            <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Invite Contact  -->

    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>