<html lang="en">

<head>
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css" rel="stylesheet" type="text/css" />
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <div class="logo text-center pb-10">
                        <a href="#.">
                            <img alt="Logo" src="../assets/images/logo.png" class="w-100px">
                        </a>
                    </div>
                    <form class="form w-100">
                        <h1 class="mb-10 text-dark text-center">Log In to your Tothiq Account</h1>
                        <div class="fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark required">Civil ID</label>
                            <input class="form-control form-control-lg form-control-solid" type="text" name="ID" placeholder="123412341234"
                                autocomplete="off" />
                        </div>
                       
                        <div class="text-center">
                            <a href="basic_create_cont_step1.php" class="btn btn-lg btn-primary w-30  mb-10">
                                Authentication Account
                            </a>
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <span >Don't have a account?</span>
                            </div>
                            <a href="index.php"
                                class="btn btn-outline btn-outline-primary btn-active-light-primary text-capitalize">register
                                now</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>var hostUrl = "../assets/";</script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>