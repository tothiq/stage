<html lang="en">

<head>
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="shortcut icon" href="images/logo.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css" rel="stylesheet" type="text/css" />
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid">
                <div class="logo d-flex justify-content-center my-20">
                    <img src="../assets/images/logo.png" alt="" class="w-100px">
                </div>
                <div class="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-10 mx-auto">
                    <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="#">

                        <div class="p-5" style="background-color:#ffffff;  max-width: 480px;">
                            <form action="">
                                <tr>
                                    <h3 class="text-primary">Yearly Membership</h3>
                                </tr>
                                <div class="row mt-4 mb-7">
                                    <div class="col-9">
                                        <p>Individual Primum Membership</p>
                                    </div>
                                    <div class="col-3">
                                        KWD 49.000
                                    </div>
                                </div>
                                <div class="fs-6 text-danger mb-2"><a
                                        style="background-color: rgba(255, 184, 70, 0.612);"
                                        class="p-2 text-danger rounded-pill"> Enter Coupon Code</a></div>
                                <div class="row text-danger">
                                    <div class="col-9">
                                        <p>Ramadon Special -20%OFF</p>
                                    </div>
                                    <div class="col-3">
                                        KWD 09.800
                                    </div>
                                </div>
                                <div class="row mt-4 mb-2 border-bottom border-dark">
                                    <div class="col-9">
                                        <p>My Fatoorah Knet Charges</p>
                                    </div>
                                    <div class="col-3">
                                        KWD 00.100
                                    </div>
                                </div>
                                <div class="row my-5" style="border-bottom-style: dashed;">
                                    <div class="col-9">
                                        <p>Total</p>
                                    </div>
                                    <div class="col-3">
                                        KWD 39.300
                                    </div>
                                </div>
                                <h3 class="text-primary mt-4 mb-7">Member Information</h3>
                                <label for="name" class="mt-2 required">Civil ID</label>
                                <input type="text" name="name" id="" class="w-100 form-control form-control-solid"
                                    placeholder="123412341234">
                                <label for="email" class="mt-6 required">Email Address</label>
                                <input type="text" class="w-100 form-control form-control-solid"
                                    placeholder="arc123@gmail.com">
                                <button class="btn btn-primary mt-15 mb-5 w-100 fw-bold" data-bs-toggle="modal"
                                type="button" href="#exampleModalToggle4">Submit</button>
                            </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
        tabindex="-1" data-bs-dismiss="modal">
        <div class="modal-dialog modal-dialog-centered modal-l">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <img class="border border-2" src="../assets/images/process_1.png" alt="">
                    </div>
                    <div class="pt-3 text-end">
                        <a type="button" class="btn btn-primary" href="basic_payment_my_fatoorah.php"
                            >Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>var hostUrl = "../assets/";</script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
</body>

</html>