<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!-- <link rel="shortcut icon" href="tothiq_admin/assets/media/logos/logo.png" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/css/iu_style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/iu_media.css" id="media_style">
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
</head>

<body class="bg-body">

    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="container-fluid py-8 sign_up_heading w-100">
                <h3 class="text-uppercase fs-1 text-primary">Sign Up</h3>
            </div>
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <div class="logo text-center pb-10">
                        <a href="#.">
                            <img alt="Logo" src="../assets/images/logo.png" class="w-100px">
                        </a>
                    </div>
                    <form class="form w-100">
                        <div class=" mb-10 mt-5">
                            <h3 class="text-primary mb-3">Register Account with Tothiq</h3>
                        </div>

                        <div class="fv-row mb-10">
                            <label class="form-label required fs-6 fw-bolder text-dark">Civil ID</label>
                            <input class="form-control form-control-lg form-control-solid" type="text" placeholder="123412341234" autocomplete="off" />
                        </div>
                        <div class="fv-row mb-10">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="username@mail.com" autocomplete="off" />
                        </div>
                        <div class="text-center mb-10">
                            <button class="btn btn-primary w-100" data-bs-toggle="modal" type="button" href="#exampleModalToggle4">Submit</button>
                            <!-- <div class="text-right btncolorblue p-5 w-100">
                                <a class="btn" data-bs-toggle="modal" href="#exampleModalToggle4" role="button">Next</a>
                            </div> -->
                        </div>
                        <div class="text-center">
                            <h5 class="text-dark mb-5">Already have a Tothiq account? <span class="text-hover-primary p-2 rounded-2 fs-4"><a href="free_login_screen.php" class="text-decoration-none text-hover-primary text-dark">Log in</a></span></h5>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-l">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <img class="border border-2" src="../assets/images/process_1.png" alt="">
                    </div>
                    <div class="pt-3 text-end">
                        <a type="button" class="btn btn-primary" href="free_set_password.php">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>

</html>