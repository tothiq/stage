<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/iu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <div class="d-flex flex-column flex-root">
        <div class="page d-flex flex-row">
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="new_draft kt_header" class="header align-items-stretch">
                    <!-- <div class="row m-0 align-items-center border border-3"> -->
                    <!-- <div class="col-1 d-flex align-items-center p-5">
                            <a href="#."><i class="fas fa-bars fa-lg"></i></a>
                        </div> -->
                    <div class="col-6 d-flex justify-content-center align-items-center">
                        <!-- <i class="fas fa-pen-square fa-lg text-primary"></i> -->
                        <h3 class="mb-0 ms-5">Rental Aggrement For Al-John Tower Office With Tothiq</h3>
                    </div>
                    <div
                        class="col-2 d-flex justify-content-center align-items-center border border-top-0 border-bottom-0 border-3">
                        <i class="fas fa-users fa-lg text-warning"></i>
                        <h3 class="mb-0 mx-5">4 Parties</h3>
                    </div>
                    <div class="col-4 d-flex justify-content-evenly align-items-center">
                        <div class="d-flex justify-content-center align-items-center">
                            <span class="fs-6 fw-bold text-success text-decoration-underline">Status : Signed</span>
                        </div>
                        <div class="create_contract_btn">
                            <!-- <i class="fas fa-file-alt fa-lg"></i> -->
                            <a  data-bs-toggle="modal" type="button" href="#exampleModalToggle5" class="btn btn-sm btn-outline btn-outline-danger btn-active-light-danger m-2"><i class="fas fa-ban fa-lg"></i></a>
                            <a href="premium_dashboard.php" class="btn btn-sm btn-primary m-2" ><i class="fas fa-download fa-lg"></i></a>
                        </div>
                    </div>
                    <!-- </div> -->
                    <div id="new_draft kt_aside" class="aside aside-light aside-hoverable bg-secondary" data-kt-drawer="true"
                        data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                        data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                        data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                        <div class="position-relative ">
                           
                        <div class="aside-menu flex-column-fluid h-100 ">
                            
                                <div class="">
                                    <div class="contract_left_sec border border-top-0 p-0  ">
                                        <ul class="nav border-0 flex-row flex-md-column mb-3 mb-md-0 fs-6">
                                            <li
                                                class="nav-item  me-0 border-bottom border-3 p-4 d-flex justify-content-between">
                                                <a class="fs-3 fw-bold text-dark">Comments</a>
                                                <i class="fas fa-expand-alt fa-lg align-items-center d-flex"></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card   " id="kt_chat_messenger">
                                        <!--begin::Card body-->
                                        <div class="card-body p-5" id="kt_chat_messenger_body">
                                            <!--begin::Messages-->
                                            <div class="scroll-y me-n5 pe-5 h-500px h-lg-auto "
                                                data-kt-element="messages" data-kt-scroll="true"
                                                data-kt-scroll-activate="{default: false, lg: true}"
                                                data-kt-scroll-max-height="auto"
                                                data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer"
                                                data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body"
                                                data-kt-scroll-offset="-2px" style="max-height: 195px;">
                                                <!--begin::Message(in)-->
                                                <div class="d-flex justify-content-start mb-10">
                                                    <!--begin::Wrapper-->
                                                    <div class="d-flex flex-column align-items-start">
                                                        <!--begin::User-->
                                                        <div class="d-flex align-items-center mb-2">
                                                            <!--begin::Details-->
                                                            <div>
                                                                <a href="#"
                                                                    class="fs-5 fw-bolder text-dark  me-1">Aabirah
                                                                    Aadab Aadil</a>
                                                                <p class="text-dark fs-7 mb-1">48 hours ago</p>
                                                            </div>
                                                            <!--end::Details-->
                                                        </div>
                                                        <!--end::User-->
                                                        <!--begin::Text-->
                                                        <div class="rounded bg-light-info text-dark fw-bold mw-lg-400px text-start p-2"
                                                            data-kt-element="message-text">Sic de isto et tutius
                                                            perducit ad
                                                            actum ipsum, ut si dico "Ego autem vadam lavari, ut mens
                                                        </div>
                                                        <!--end::Text-->
                                                    </div>
                                                    <!--end::Wrapper-->
                                                </div>
                                                <!--end::Message(in)-->
                                                <!--begin::Message(in)-->
                                                <div class="d-flex justify-content-start ">
                                                    <!--begin::Wrapper-->
                                                    <div class="d-flex flex-column align-items-start">
                                                        <!--begin::User-->
                                                        <div class="d-flex align-items-center mb-2">
                                                            <!--begin::Details-->
                                                            <div>
                                                                <a href="#"
                                                                    class="fs-5 fw-bolder text-dark me-1">Aabirah
                                                                    Aadab Aadil</a>
                                                                <p class="text-dark fs-7 mb-1">48 hours ago</p>
                                                            </div>
                                                            <!--end::Details-->
                                                        </div>
                                                        <!--end::User-->
                                                        <!--begin::Text-->
                                                        <div class="rounded bg-light-info text-dark fw-bold mw-lg-400px text-start p-2 mb-20"
                                                            data-kt-element="message-text">Sic de isto et tutius
                                                            perducit ad
                                                            actum ipsum, ut si dico "Ego autem vadam lavari, ut mens
                                                        </div>
                                                        <!--end::Text-->
                                                    </div>
                                                    <!--end::Wrapper-->
                                                </div>
                                                <!--end::Message(in)-->
                                            </div>
                                            <!--end::Messages-->
                                        </div>
                                        <!--end::Card body-->
                                        <!--begin::Card footer-->
                                        <div>
                                            

                                            <!--end::Card footer-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="position-relative">
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" class="container-xxl mt-5">
                        <div class="card mx-5 mt-5 mb-20">
                            <div class="card-body">
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Stactic content start</span>
                                </div>
                                <h2 class="lh-base fw-700 mb-15">Rental Agreement For Al-John<br>Tower Office
                                    with Tothiq</h2>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Starting Date : 01-01-2023
                                        12:00:00</span>
                                </div>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Period : 1 Year</span>
                                </div>
                                <div>
                                    <span class="fs-6 fw-bold">Contract Value : KWD 12000.000</span>
                                </div>
                                <div class="card_table my-15">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr class="fw-bold fs-6 text-gray-800">
                                                    <th class="border border-1 border-secondary p-2">Parties
                                                        Information</th>
                                                    <th class="border border-1 border-secondary p-2">Name Of
                                                        Person</th>
                                                </tr>
                                            </thead>
                                            <tbody class="border border-1 border-secondary p-2">
                                                <tr>
                                                    <td class="border border-1 border-secondary p-2">First Party
                                                    </td>
                                                    <td class="border border-1 border-secondary p-2">Aabirah
                                                        Aadab Aadil</td>
                                                </tr>
                                                <tr>
                                                    <td class="border border-1 border-secondary p-2">Second
                                                        Party</td>
                                                    <td class="border border-1 border-secondary p-2">Aadil
                                                        Aabirah Aadab</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Stactic content end</span>
                                </div>
                                <div class="card_body_highlited_text mb-10">
                                    <span class="bg-warning text-dark">Upload Document will come here</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer py-4 d-flex  flex-lg-column app-footer position-fixed bottom-0 end-0 w-100" id="kt_footer">
                <!--begin::Container-->
                <div
                    class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                            Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalToggle5" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel5" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="modal-body">
                        <div>
                            <div class="modal-header d-flex justify-content-between">
                                <h3 class="text-capitalize">request for cancel contract</h3>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="mt-10 px-7">
                                <h5>Cancellation Reason</h5>
                                <div class="form-group">
                                    <textarea class="form-control rounded-0" id="exampleFormControlTextarea1"
                                        rows="7" placeholder="Please Enter Cancellation Reason"></textarea>
                                </div>
                                <div class="d-flex align-items-center mt-10 justify-content-between">
                                    <p class="text-danger">As soon as you send in a request to cancel at of the other parties involved in this contract are obligated to acknowledge and consent to the cancellation. After they have acknowledged and accapted this cancellation contract, it will be terminated immediately, and we will notify them of this via email</p>
                                    <a type="button" class="btn btn-primary btn-sm w-50 ms-5"
                                        href="premium_cont_cancelled_view.php" type="button">Submit Cancellation
                                        Request</a>
                                </div>
                            </div>
                        </div>
                        <div class="pt-3 text-end">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>