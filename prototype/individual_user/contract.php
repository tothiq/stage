<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/iu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true"
                data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="free_dashboard.php">
                        <img alt="Logo" src="../assets/images/logo.png" class="w-60px" />
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                    <!--end::Logo-->
                    <!--begin::Aside toggler-->
                    <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
                        data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
                        data-kt-toggle-name="aside-minimize">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                        <span class="svg-icon svg-icon-1 rotate-180">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.5"
                                    d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                                    fill="black" />
                                <path
                                    d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Aside toggler-->
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
                        <!--begin::Menu-->
                        <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                            id="#kt_aside_menu" data-kt-menu="true">
                            <div class="menu-item">
                                <!-- <div class="menu-content pb-2">
                    <span class="menu-section text-muted text-uppercase fs-8 ls-1">Dashboard</span>
                </div> -->
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_dashboard.php">
                                    <span class="menu-icon">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-th-large fa-lg"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_contract.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-file-contract fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contract</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_contact.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="far fa-address-book fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contact</span>
                                </a>
                            </div>
                        </div>
                        <!--end::Menu-->
                    </div>
                    <div class="aside-footer flex-column-auto pt-5 pb-7 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                        id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link" href="basic_notification.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <i class="fas fa-bell fa-lg"></i>
                                    </span>
                   
                                </span>
                                <span class=" menu-title">Notification</span>
                            </a>
                        </div>
                        <div class="d-flex align-items-center" id="kt_header_user_menu_toggle">
                            <!--begin::Menu wrapper-->
                            <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click"
                                data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                <div class="menu-item">
                                    <a class="menu-link">
                                        <span class="menu-icon">
                                            <span class="svg-icon svg-icon-2 position-relative">
                                                <i class="fas fa-user-circle fa-lg"><div class="position-absolute top-0 start-100 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                                </div></i>
                                                
                                            </span>
                                        </span>
                                        <span class=" menu-title">User Profile</span>
                                    </a>
                                </div>
                            </div>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                                data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <div class="menu-content d-flex align-items-center px-3">
                                        <!--begin::Username-->
                                        <div class="d-flex flex-column">
                                            <div class="fw-bolder d-flex align-items-center fs-5">Tothiq User
                                            </div>
                                            <p class="fw-bold text-muted text-hover-primary fs-7">Free Membership</p>
                                        </div>
                                        <!--end::Username-->
                                    </div>
                                </div>
                                <div class="separator my-2"></div>
                                <div class="menu-item px-5">
                                    <a href="" class="menu-link px-5">My Profile</a>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="basic_login_screen.php"
                                        class="menu-link px-5">Sign Out</a>
                                </div>
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div id="kt_header" class="header align-items-center px-7">
                <h1 class="d-flex align-items-center text-dark fw-bolder fs-2 my-1">Contracts</h1>
                <div>
                    <a href="basic_create_cont_step1.php" class="btn btn-sm btn-primary">Create Contracts</a>
                </div>
            </div>
            <div class="content d-flex flex-column p-0" id="kt_content">
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container">
                        <div class="d-flex flex-column flex-xl-row">
                            <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-5">
                                <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                                    id="#kt_aside_menu" data-kt-menu="true">
                                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion hover show">
                                        <span class="menu-link">
                                            <span class="menu-title fs-5 fw-bold">My Folders</span>
                                            <span class="menu-arrow"></span>
                                        </span>
                                        <div class="menu-sub menu-sub-accordion menu-active-bg show"
                                            kt-hidden-height="312">
                                            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                                                <span class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title fs-5 fw-bold">General Folders</span>
                                                    <span class="menu-arrow"></span>
                                                </span>
                                            </div>
                                            <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                                                <span class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title fs-5 fw-bold">Rent Agreement</span>
                                                    <span class="menu-arrow"></span>
                                                </span>
                                                <div class="menu-sub menu-sub-accordion menu-active-bg">
                                                    <div class="menu-item">
                                                        <a class="menu-link"
                                                            href="#.">
                                                            <span class="menu-bullet">
                                                                <span class="bullet bullet-dot"></span>
                                                            </span>
                                                            <span class="menu-title fs-5 fw-bold">Home Rent Agreement</span>
                                                        </a>
                                                    </div>
                                                    <div class="menu-item">
                                                        <a class="menu-link"
                                                            href="#.">
                                                            <span class="menu-bullet">
                                                                <span class="bullet bullet-dot"></span>
                                                            </span>
                                                            <span class="menu-title fs-5 fw-bold">Office Rent Agreement</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="left_new_folder px-4 py-4">
                                            <a href="#." class="text-decoration-underline text-primary fs-5 fw-bold" data-bs-toggle="modal"
                                            data-bs-target="#create_contract">Add New
                                                Folder</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Sidebar-->
                    <!--begin::Content-->
                    <div class="flex-lg-row-fluid ms-lg border border-3">
                        <!--begin:::Tabs-->
                        <ul
                            class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8 ms-2">
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 ms-2 text-active-primary pb-4 active" data-bs-toggle="tab"
                                    href="#all_contrcats">All Contracts</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#drafts">Drafts</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-kt-countup-tabs="true"
                                    data-bs-toggle="tab" href="#under-review">Under-Review</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#ready">Ready</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#signed">Signed</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#rejected">Rejected</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#deleted">Deleted</a>
                            </li>
                            <!--end:::Tab item-->
                            <!--begin:::Tab item-->
                            <li class="nav-item">
                                <a class="nav-link me-5 text-active-primary pb-4" data-bs-toggle="tab"
                                    href="#cancelled">Cancelled</a>
                            </li>
                            <!--end:::Tab item-->
                        </ul>
                        <!--end:::Tabs-->
                        <div class="d-flex align-items-center py-1 justify-content-end">
                            <div class="contract_filter">
                                <select class="form-select form-select-solid select2-hidden-accessible"
                                    data-control="select2" data-placeholder="Select an option"
                                    data-select2-id="select2-data-1-ox89" tabindex="-1" aria-hidden="true">
                                    <option value="1" data-select2-id="select2-data-3-21xq">2022-10-30 to 2022-11-28
                                    </option>
                                    <option value="2">2022-7-30 to 2022-8-28</option>
                                </select>
                            </div>
                        </div>
                        <!--begin:::Tab content-->
                        <div class="tab-content ms-2 mb-20" id="myTabContent">
                            <!--begin:::Tab pane-->
                            <div class="tab-pane fade show active" id="all_contrcats" role="tabpanel">
                                <!--begin::Card-->
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg text-success"></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center bg-light-secondary">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg text-success"></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg text-success"></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg text-success"></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Draft</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card-->
                            </div>
                            <!--end:::Tab pane-->
                            <!--begin:::Tab pane-->
                            <div class="tab-pane fade" id="drafts" role="tabpanel">
                                <!--begin::Card-->
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Draft</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card-->
                            </div>
                            <!--end:::Tab pane-->
                            <!--begin:::Tab pane-->
                            <div class="tab-pane fade" id="under-review" role="tabpanel">
                                <!--begin::Earnings-->

                                <!--end::Statements-->
                            </div>
                            <div class="tab-pane fade" id="ready" role="tabpanel">
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="bottom-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="bottom-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mb-4 me-4 border-secondary border border-2">
                                    <div class="row g-0">
                                        <div
                                            class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <i class="fas fa-user-circle fa-lg"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-6 cont_block_2">
                                            <div class="card-body">
                                                <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                                                    Tothiq</h5>
                                                <p class="card-text">Create Date: 12-11-2022 14:35:29</p>
                                                <div class="d-flex">
                                            <p class="card-text"><small class="text-muted">Status: Ready</small></p>
                                            <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                                            <a href="#." class="card-text"><small class="text-muted">Clone</small></a>
                                        </div>
                                            </div>
                                        </div>
                                        <div
                                            class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                                            <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                                <a href="#.">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown"
                                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                                        data-kt-menu-placement="top-end">
                                                        <i class="fas fa-user-circle fa-lg "></i>
                                                    </div>
                                                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg"
                                                        data-kt-menu="true"
                                                        style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);"
                                                        data-popper-placement="top-end">
                                                        <!--begin::Menu item-->
                                                        <div class="card ">
                                                            <div class="card-body text-dark">
                                                                <div>
                                                                    <div class="card_user_info d-flex justify-content-between align-items-center">
                                                                        <i class="fas fa-user-circle fa-lg"></i>
                                                                        <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                                                    </div>
                                                                    <div class="pt-5">
                                                                        <h3>Aabirah Aadab Aadil</h3>
                                                                        <p>Tothiq</p>
                                                                    </div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p class="mb-0">Email:</p>
                                                                            <p>aabirah.aadil@domain.com</p>
                                                                            <p class="mb-0">Mobile Number:</p>
                                                                            <p>+965-123456789</p>
                                                                            <p class="mb-0">Hawati Verification:</p>
                                                                            <p>Verified</p>
                                                                        </span></div>
                                                                    <div class="separator separator-dashed border-3 my-5"></div>
                                                                    <div><span>
                                                                            <p>Not viewed</p>
                                                                        </span></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end::Menu item-->
                                                    </div>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="signed" role="tabpanel">
                                <!--begin::Earnings-->

                                <!--end::Statements-->
                            </div>
                            <div class="tab-pane fade" id="rejected" role="tabpanel">
                                <!--begin::Earnings-->

                                <!--end::Statements-->
                            </div>
                            <div class="tab-pane fade" id="deleted" role="tabpanel">
                                <!--begin::Earnings-->

                                <!--end::Statements-->
                            </div>
                            <div class="tab-pane fade" id="cancelled" role="tabpanel">
                                <!--begin::Earnings-->

                                <!--end::Statements-->
                            </div>
                            <!--end:::Tab pane-->
                        </div>
                        <!--end:::Tab content-->
                    </div>
                    <!--end::Content-->
                </div>
            </div>
            <div class="footer py-4 d-flex  flex-lg-column app-footer position-fixed bottom-0 end-0 w-100" id="kt_footer">
                <!--begin::Container-->
                <div
                    class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                            Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
        </div>
    </div>

    </div>
    </div>
    <div class="modal fade" id="create_contract" tabindex="-1" aria-modal="true" role="dialog">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-900px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header">
                    <!--begin::Modal title-->
                    <h2>Create Folder</h2>
                    <!--end::Modal title-->
                    <!--begin::Close-->
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="black"></rect>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--end::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body py-lg-10 px-lg-10">
                    <div class="popup_select_parent_folder mt-5 mb-10">
                        <h3>Select Parent Folder</h3>
                        <select class="form-select" aria-label="Select example">
                            <option>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="popup_select_folder_name my-10">
                        <h3>Folder Name</h3>
                        <input type="text" class="form-control" placeholder="name@example.com" />
                    </div>
                    <div class="popup_create_folder_btn d-flex justify-content-end">
                        <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal"
                            data-bs-target="#create_contract">Create Folder</a>
                    </div>
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
    <!--end::Root-->

    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>