<?php include("header_basic.php") ?>
			<div id="kt_app_toolbar" class="app-toolbar py-8">
				<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
					<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
						<h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Create Contract</h1>
						
					</div>
				</div>
			</div>
			<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
				<div class="post d-flex flex-column-fluid" id="kt_post">
					<div id="kt_content_container" class="container-xxl">
						<div class="kt_content_containe_inr d-flex justify-content-between my-5">
							<div class="create_contract_step">
								<ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
									<li class="nav-item">
										<a class="nav-link active border-bottom-2 border-primary">Step-1</a>
									</li>
									<li class="nav-item">
										<a class="nav-link">Step-2</a>
									</li>
									<li class="nav-item">
										<a class="nav-link">Step-3</a>
									</li>
								</ul>
							</div>
							<div class="create_contract_btn">
								<div class="create_cont_btn_inr d-flex align-items-end">
									<div class="create_contract_btn1 me-5">
										<a href="basic_create_cont_step2.php" class="btn btn-sm btn-primary" >Create Blank Contract</a>
									</div>
									<div class="create_contract_btn2">
										<h3 class="fs-6 fw-normal bg-warning bg-opacity-50 text-dark">For premium members</h3>
										<a href="#." class="btn btn-sm btn-primary disabled" data-bs-toggle="modal"
											data-bs-target="#uploaddocument">Upload
											Document</a>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-content" id="myTabContent">
							<!--begin:::Tab pane-->
							<div class="tab-pane fade active show" id="all_contrcats" role="tabpanel">
								<div class="row gy-5 g-xl-8 mx-0">
									<!--begin::Col-->
									<div class="col-xl-3 ps-0">
										<!--begin::List Widget 3-->
										<div class="card card-xl-stretch mb-xl-8">
											<!--begin::Header-->
											<div class="card-header p-0 min-h-0 m-5">
												<form data-kt-search-element="form"
													class="d-none d-lg-block w-100 mb-5 mb-lg-0 position-relative"
													autocomplete="off">
													<span>
														<i class="fas fa-search fa-lg position-absolute" style="top: 29%;left: 5%;"></i>
													</span>
													<input type="text"
														class="form-control form-control-solid h-40px bg-body ps-13 fs-7"
														name="search" value=""
														placeholder="Type content which you would like to search"
														data-kt-search-element="input">
												</form>
											</div>
											<!--end::Header-->
											<!--begin::Body-->
											<div class="card-body p-0">
												<!--begin::Item-->
												<ul class="contract_tab nav flex-row flex-md-column mb-3 mb-md-0">
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link active" data-bs-toggle="tab"
															href="#tab1_all">Categories</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab2_at">Accounting &amp; Tax</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab3_aggrement">Agreements</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab4_ba">Buisness &amp; Plans <span
																class="text-dark bg-warning bg-opacity-50">Premium</span></a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab5_construction">Construction</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab6_consulting">Consulting</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab7_contracts">Contracts</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab8_employment">Employment</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab9_financial">Financial <span
																class="text-dark bg-warning bg-opacity-50">Premium</span></a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab10_healthcare">Healthcare</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab11_manufacturing">Manufacturing</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab12_marketing">Marketing</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab13_re">Real Estate <span
																class="text-dark bg-warning bg-opacity-50">Premium</span></a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab14_sales">Sales</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab15_software">Software</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab16_tft">Tax form templates</a>
													</li>
													<li class="nav-item  me-0">
														<a class="fs-6 fw-bold nav-link" data-bs-toggle="tab"
															href="#tab17_technology">Technology</a>
													</li>
												</ul>
												<!--end:Item-->
											</div>
											<!--end::Body-->
										</div>
										<!--end:List Widget 3-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col-xl-9">
										<!--begin::Tables Widget 9-->
										<div class="tab-content ms-2" id="myTabContent">
											<div class="card-header p-0 min-h-0">
												<form data-kt-search-element="form"
													class="d-none d-lg-block w-100 mb-5 mb-lg-0 position-relative"
													autocomplete="off">
													<span>
														<i class="fas fa-search fa-lg position-absolute" style="top: 50%;left: 2%;"></i>
													</span>
													<input type="text"
														class="form-control form-control-solid h-40px bg-body ps-13 fs-7"
														name="search" value=""
														placeholder="Type content which you would like to search"
														data-kt-search-element="input">
												</form>
											</div>
											<!--begin:::Tab pane-->
											<div class="tab-pane fade mt-3 active show" id="tab1_all" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Contract Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab2_at" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab3_aggrement" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab4_ba" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab5_construction" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab6_consulting" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab7_contracts" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab8_employment" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab9_financial" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab10_healthcare" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab11_manufacturing" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab12_marketing" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab13_re" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab14_sales" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab15_software" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab16_tft" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Bookkeeping
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<div class="tab-pane fade mt-3" id="tab17_technology" role="tabpanel">
												<div class="templates_boxsec d-flex flex-wrap">
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">

															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Accountin
																		Service
																		Template</span></a>
															</div>
														</div>
													</a>
													<a href="basic_create_cont_step2.php">
														<div class="grid_layout_slide_right_doc col-3 p-5">
															<div class="template_img pb-3 position-relative">
																<img src="../assets/images/template.png" alt=""
																	class="w-100">
																<span
																	class="text-dark position-absolute top-0 end-0 m-1 bg-warning bg-opacity-50">Premium</span>
															</div>
															<div class="template_lable_container">
																<a href="#."><span
																		class="template_lable fs-6 fw-bold">Audit
																		Contract
																		Template</span></a>
															</div>
														</div>
													</a>
												</div>
											</div>
											<!--end:::Tab pane-->
										</div>
										<!--end::Tables Widget 9-->
									</div>
									<!--end::Col-->
								</div>
							</div>
							<!--end:::Tab pane-->
						</div>
					</div>
				</div>
			</div>
	<!--end::Root-->


	<!--end::Modals-->
	<!--begin::Scrolltop-->
	<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
		<span class="svg-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
				<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
					fill="black" />
				<path
					d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
					fill="black" />
			</svg>
		</span>
	</div>
	
	<script>
		ClassicEditor
			.create(document.querySelector('#ckeditor_1'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
		ClassicEditor
			.create(document.querySelector('#ckeditor_2'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
		ClassicEditor
			.create(document.querySelector('#ckeditor_3'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
	</script>
	<script>

		$("#business-tab").click(function () {
			$("div#myTabContent1").hide();
			$("div#myTabContent2").show();
		});
		$("#individual-tab").click(function () {
			$("div#myTabContent1").show();
			$("div#myTabContent2").hide();
		});
		$(document).ready(function () {
			$('#addresstype').on('change', function () {
				var demovalue = $(this).val();
				$("div.myDiv").hide();
				$("#show" + demovalue).show();
			});
			$('#addresstype1').on('change', function () {
				var demovalue1 = $(this).val();
				$("div.myDiv1").hide();
				$("#show" + demovalue1).show();
			});
		});
		// var profileborder = "border-danger";
		$(".userprofile").addClass("border-danger");
		// function changeuserborder() {
		//     $(".userprofile").removeClass(profileborder);
		//     var profileborder = "border-success";
		// }
		$("#userheaderchange").click(function () {
			$(".userprofile").removeClass("border-danger");
			$(".userprofile").addClass("border-success");
			$("#headererror").addClass("d-none");

		});
	</script>
	<script>
		$.fn.equalHeights = function () {
			var max_height = 0;
			$(this).each(function () {
				max_height = Math.max($(this).height(), max_height);
			});
			$(this).each(function () {
				$(this).height(max_height);
			});
		};

		$(document).ready(function () {
			$('.userdasboardbox ul li a .card').equalHeights();
		});
	</script>

<?php include("footer_basic.php") ?>