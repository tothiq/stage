<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/iu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true"
                data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="basic_dashboard.php">
                        <img alt="Logo" src="images/logo.png" class="w-60px" />
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                    <!--end::Logo-->
                    <!--begin::Aside toggler-->
                    <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
                        data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
                        data-kt-toggle-name="aside-minimize">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                        <span class="svg-icon svg-icon-1 rotate-180">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.5"
                                    d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                                    fill="black" />
                                <path
                                    d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Aside toggler-->
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
                        <!--begin::Menu-->
                        <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                            id="#kt_aside_menu" data-kt-menu="true">
                            <div class="menu-item">
                                <a class="menu-link" href="basic_dashboard.php">
                                    <span class="menu-icon">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-th-large fa-lg"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="contract.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-file-contract fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contract</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_contact.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="far fa-address-book fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contact</span>
                                </a>
                            </div>
                        </div>
                        <!--end::Menu-->
                    </div>
                    <div class="aside-footer flex-column-auto pt-5 pb-7 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link" href="notification.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <i class="fas fa-bell fa-lg""></i>
                                    </span>
                   
                                </span>
                                <span class="menu-title">Notification</span>
                            </a>
                        </div>
                        <div class="d-flex align-items-center" id="kt_header_user_menu_toggle">
                            <!--begin::Menu wrapper-->
                            <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click"
                                data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                <div class="menu-item">
                                    <a class="menu-link">
                                        <span class="menu-icon">
                                            <span class="svg-icon svg-icon-2 position-relative">
                                                <i class="fas fa-user-circle fa-lg"><div class="position-absolute top-0 start-100 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                                </div></i>
                                                
                                            </span>
                                        </span>
                                        <span class=" menu-title">User Profile</span>
                                    </a>
                                </div>
                            </div>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                                data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <div class="menu-content d-flex align-items-center px-3">
                                        <!--begin::Username-->
                                        <div class="d-flex flex-column">
                                            <div class="fw-bolder d-flex align-items-center fs-5">Tothiq User
                                            </div>
                                            <p class="fw-bold text-muted text-hover-primary fs-7">Free Membership</p>
                                        </div>
                                        <!--end::Username-->
                                    </div>
                                </div>
                                <div class="separator my-2"></div>
                                <div class="menu-item px-5">
                                    <a href="" class="menu-link px-5">My Profile</a>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="free_login_screen.php"
                                        class="menu-link px-5">Sign Out</a>
                                </div>
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div id="kt_header" class="header align-items-stretch">
                <div class="container-fluid d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center d-lg-none ms-n3 me-1" title="Show aside menu">
                        <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
                            <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                            <span class="svg-icon svg-icon-2x mt-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black"></path>
                                    <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black"></path>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="content d-flex flex-column p-0" id="kt_content">
                <div class="toolbar" id="kt_toolbar">
                    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                        <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                            class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                            <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1"></h1>
                        </div>
                    </div>
                </div>
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" class="container-xxl">
                        <div class="kt_content_container_inr">
                            <div class="kt_content_container_heading">
                                <div class="row mt-10 align-items-center">
                                    <div class="col-7">
                                        <h3>Notification</h3>
                                    </div>
                                    <div class="col-5 ps-0">
                                        <form data-kt-search-element="form"
                                        class="d-none d-lg-block w-100 mb-5 mb-lg-0 position-relative"
                                        autocomplete="off">
                                        <input type="hidden">
                                        <span
                                            class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                                                </rect>
                                                <path
                                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </span>
                                        <input type="text"
                                            class="form-control form-control-solid h-40px bg-body ps-13 fs-7"
                                            name="search" value="" placeholder="Type content which you would like to search"
                                            data-kt-search-element="input">
                                    </form>
                                    </div>
                                </div>
                                <!-- <h3>Notification</h3> -->
                                <!-- <div>
                                    <form data-kt-search-element="form"
                                        class="d-none d-lg-block w-100 mb-5 mb-lg-0 position-relative"
                                        autocomplete="off">
                                        <input type="hidden">
                                        <span
                                            class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                                                </rect>
                                                <path
                                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </span>
                                        <input type="text"
                                            class="form-control form-control-solid h-40px bg-body ps-13 fs-7"
                                            name="search" value="" placeholder="Type content which you would like to search"
                                            data-kt-search-element="input">
                                    </form>
                                </div> -->
                            </div>
                            <div class="notice d-flex border-secondary border border-2 p-6 my-10">
                                <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                    <i class="fas fa-user-circle fa-6x"></i>
                                </span>
                                <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                    <div class="mb-3 mb-md-0 fw-bold">
                                        <div class="notification_block_heading d-flex justify-content-between">
                                            <h4 class="text-gray-900 fw-bolder">Amaira has accepted rent contract. 1 Minute Ago</h4>
                                            <div class="notification_block_date d-flex">
                                                <p>3rd Dec 2022 11:5221</p>
                                                <a href="#."><i class="fas fa-trash-alt fa-lg ms-5"></i></a>
                                            </div>
                                        </div>
                                        <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest libido frustra miseri qui incurrit odium sul obiecti</div>
                                    </div>
                                </div>
                            </div>
                            <div class="notice d-flex border-secondary border border-2 p-6 my-10">
                                <span class="svg-icon svg-icon-2 d-flex align-items-center">
                                    <i class="fas fa-user-circle fa-6x"></i>
                                </span>
                                <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                    <div class="mb-3 mb-md-0 fw-bold">
                                        <div class="notification_block_heading d-flex justify-content-between">
                                            <h4 class="text-gray-900 fw-bolder">Amaira has accepted rent contract. 1 Minute Ago</h4>
                                            <div class="notification_block_date d-flex">
                                                <p>3rd Dec 2022 11:5221</p>
                                                <a href="#."><i class="fas fa-trash-alt fa-lg ms-5"></i></a>
                                            </div>
                                        </div>
                                        <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest libido frustra miseri qui incurrit odium sul obiecti</div>
                                    </div>
                                </div>
                            </div>
                            <div class="notice d-flex border-secondary border border-2 p-6 my-10">
                                <img src="./images/logo.png" class="w-70px" />
                                <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                    <div class="mb-3 mb-md-0 fw-bold">
                                        <div class="notification_block_heading d-flex justify-content-between">
                                            <h4 class="text-gray-900 fw-bolder">Special Renewal Offer -25% Off</h4>
                                            <div class="notification_block_date d-flex">
                                                <p>3rd Dec 2022 11:5221</p>
                                                <a href="#."><i class="fas fa-trash-alt fa-lg ms-5"></i></a>
                                            </div>
                                        </div>
                                        <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest libido frustra miseri qui incurrit odium sul obiecti</div>
                                    </div>
                                </div>
                            </div>
                            <div class="notice d-flex border-secondary border border-2 p-6 my-10">
                                <img src="./images/logo.png" class="w-70px" />
                                <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                    <div class="mb-3 mb-md-0 fw-bold">
                                        <div class="notification_block_heading d-flex justify-content-between">
                                            <h4 class="text-gray-900 fw-bolder">Special Renewal Offer -25% Off</h4>
                                            <div class="notification_block_date d-flex">
                                                <p>3rd Dec 2022 11:5221</p>
                                                <a href="#."><i class="fas fa-trash-alt fa-lg ms-5"></i></a>
                                            </div>
                                        </div>
                                        <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest libido frustra miseri qui incurrit odium sul obiecti</div>
                                    </div>
                                </div>
                            </div>
                            <div class="notice d-flex border-secondary border border-2 p-6 my-10">
                                <img alt="Logo" src="./images/logo.png" class="w-70px" />
                                <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                    <div class="mb-3 mb-md-0 fw-bold">
                                        <div class="notification_block_heading d-flex justify-content-between">
                                            <h4 class="text-gray-900 fw-bolder">Special Renewal Offer -25% Off</h4>
                                            <div class="notification_block_date d-flex">
                                                <p>3rd Dec 2022 11:5221</p>
                                                <a href="#."><i class="fas fa-trash-alt fa-lg ms-5"></i></a>
                                            </div>
                                        </div>
                                        <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest libido frustra miseri qui incurrit odium sul obiecti</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer py-4 d-flex  flex-lg-column app-footer" id="kt_footer">
                <!--begin::Container-->
                <div class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>

        </div>
    </div>
    <!--end::Root-->

    <!-- Modal Invite Contact  -->
    <div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2"
        tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="form w-100">
                        <div class=" d-flex flex-wrap">
                            <div class="fv-row p-5 col-12">
                                <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                                <input class="form-control form-control-lg form-control-solid" type="email"
                                    placeholder="Email Address" autocomplete="off" />
                            </div>
                        </div>
                        <div class="text-center btncolorblue pt-10">
                            <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Invite Contact  -->

    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="assets/plugins/global/plugins.bundle.js"></script>
    <script src="assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="assets/js/custom/widgets.js"></script>
    <script src="assets/js/custom/apps/chat/chat.js"></script>
    <script src="assets/js/custom/modals/create-app.js"></script>
    <script src="assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>