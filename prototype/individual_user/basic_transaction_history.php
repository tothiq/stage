<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/iu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true"
                data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="basic_dashboard.php">
                        <img alt="Logo" src="../assets/images/logo.png" class="w-60px" />
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                    <!--end::Logo-->
                    <!--begin::Aside toggler-->
                    <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
                        data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
                        data-kt-toggle-name="aside-minimize">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                        <span class="svg-icon svg-icon-1 rotate-180">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.5"
                                    d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                                    fill="black" />
                                <path
                                    d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Aside toggler-->
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
                        <!--begin::Menu-->
                        <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                            id="#kt_aside_menu" data-kt-menu="true">
                            <div class="menu-item">
                                <!-- <div class="menu-content pb-2">
                    <span class="menu-section text-muted text-uppercase fs-8 ls-1">Dashboard</span>
                </div> -->
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_dashboard.php">
                                    <span class="menu-icon">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-th-large fa-lg"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="contract.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-file-contract fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contract</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="basic_contact.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="fa-regular fa-address-book"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contact</span>
                                </a>
                            </div>
                        </div>
                        <!--end::Menu-->
                    </div>
                    <div class="aside-footer flex-column-auto pt-5 pb-7 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                        id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link" href="notification.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <i class="fas fa-bell fa-lg""></i>
                                    </span>
                   
                                </span>
                                <span class=" menu-title">Notification</span>
                            </a>
                        </div>
                        <div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
                            <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown text-hover-primary position-relative"
                                data-kt-menu-trigger="hover" data-kt-menu-attach="parent"
                                data-kt-menu-placement="right-end">
                                <i class="fas fa-user-circle fa-lg ms-4 me-4"></i> User Profile
                                <span
                                    class="position-absolute top-0 start-25 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                </span>
                            </div>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px "
                                data-kt-menu="true" data-popper-placement="right-end"
                                style="z-index: 105; position: fixed; inset: 0px 0px auto auto; margin: 0px; transform: translate3d(-30px, 65px, 0px);">
                                <div class="menu-item  bg-light-danger p-5 text-dark">
                                    <h2>Tothiq User</h2>
                                    <p>Individual profile</p>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="basic_profile_screen_active.php" class="menu-link px-5">
                                        <div class="ps-5 pt-5 d-flex"><i
                                                class="bi bi-person-circle me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>My profile</p>
                                            </span></div>
                                    </a>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="basic_transaction_history.php" class="menu-link px-5">
                                        <div class="ps-5 d-flex"><i
                                                class="bi bi-calculator-fill me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>Transaction History</p>
                                            </span></div>
                                    </a>
                                </div>
                                <div class="separator border-3 my-5"></div>
                                <div class="menu-item px-5">
                                    <a href="basic_login_screen.php" class="menu-link px-5">
                                        <div class="ps-5 d-flex"><i
                                                class="bi bi-box-arrow-right me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>Log Out</p>
                                            </span></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div id="kt_header" class="header align-items-stretch d-flex justify-content-end">
                <div class="d-flex">
                    <div class="mt-2">
                        <a class="menu-link" href="basic_notification.php">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <i class="fa-regular fa-bell"></i>
                                </span>
               
                            </span>
                            <!-- <span class=" menu-title">Notification</span> -->
                        </a>
                    </div>
                    <div class="d-flex align-items-center" id="kt_header_user_menu_toggle">
                        <!--begin::Menu wrapper-->
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click"
                            data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                            <div class="menu-item">
                                <a class="menu-link">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2 position-relative">
                                            <i class="fa-regular fa-user-group">
                                                <div
                                                    class="position-absolute top-0 start-100 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                                </div>
                                            </i>

                                        </span>
                                    </span>
                                    <!-- <span class=" menu-title">User Profile</span> -->
                                </a>
                            </div>
                        </div>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                            data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <div class="menu-content d-flex align-items-center px-3">
                                    <!--begin::Username-->
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder d-flex align-items-center fs-5">Tothiq User
                                        </div>
                                        <p class="fw-bold text-muted text-hover-primary fs-7">Basic Membership</p>
                                    </div>
                                    <!--end::Username-->
                                </div>
                            </div>
                            <div class="separator my-2"></div>
                            <div class="menu-item px-5">
                                <a href="basic_profile_screen_active.php" class="menu-link px-5">My Profile</a>
                            </div>
                            <div class="menu-item px-5">
                                <a href="basic_login_screen.php" class="menu-link px-5">Sign Out</a>
                            </div>
                        </div>
                        <!--end::Menu-->
                        <!--end::Menu wrapper-->
                    </div>
                </div>
            </div>
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container px-5 d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Transaction History</h1>
                        <!-- <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="basic_dashboard.php" class="text-muted text-hover-primary">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Transaction History</li>
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="content d-flex flex-column p-0" id="kt_content">
                

                <div class="post" id="kt_post">
                    <div id="kt_content_container">
                        <div class="d-flex flex-column  ">
                            <div class="row gy-5 g-xl-8 mx-0 mb-5 mt-2">
                                <div class="col-xl-3">
                                    <div class="card card-xl-stretch mb-xl-8">
                                        <div class="card-body p-0 mt-3">
                                            <h6 class="text-info m-4">Transaction type</h6>
                                            <ul class="contract_tab nav flex-row flex-md-column mb-3 mb-md-0">
                                                <li class="nav-item  me-0 w-100">
                                                    <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary border-start border-2 text-dark"
                                                        data-bs-toggle="tab" href="#tab1">All Transaction</a>
                                                </li>
                                                <li class="nav-item  me-0">
                                                    <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark"
                                                        data-bs-toggle="tab" href="#tab2">Membership</a>
                                                </li>
                                                <li class="nav-item  me-0">
                                                    <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark"
                                                        data-bs-toggle="tab" href="#tab3">Contracts</a>
                                                </li>


                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-9">
                                    <div class="tab-content ms-2" id="myTabContent_main">
                                        <div class="tab-pane fade active show" id="tab1" role="tabpanel">
                                            <div class="card p-5 ">
                                                <div class="d-flex ">
                                                    <div class="mt-9 me-20">
                                                        <h3 class="ms-7 me-10">All Transaction</h3>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="fv-row p-5 col-6 ms-10">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Transaction Type">
                                                                <option></option>
                                                                <option value="1">All Transaction</option>
                                                                <option value="2">Membership</option>
                                                                <option value="3">Contract</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select me-4" data-control="select2"
                                                                data-placeholder="Month">
                                                                <option></option>
                                                                <option value="1">Jan</option>
                                                                <option value="2">Feb</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Year">
                                                                <option></option>
                                                                <option value="1">2022</option>
                                                                <option value="2">2023 </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row row-cols-1 row-cols-md-12 g-4 mx-0">
                                                    <div class="col px-5">
                                                        <div class="card border border-2">
                                                            <div class="card-body p-0">
                                                                <div class="row mx-0 ">
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_1">
                                                                            <h4 class="mb-5">Membership Renewal</h4>
                                                                            <p>Purchase Date-01-12-2022</p>
                                                                            <p>Next Renewal: 30-11-2023</p>
                                                                            <p>Duration: Yearly</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_2 mt-11">
                                                                            <h6 class="mb-0">Payment Mode</h6>
                                                                            <p>Knet</p>
                                                                            <h6 class="mb-0">Transaction ID</h6>
                                                                            <p>#2305812dhw49320</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10 bg-light-primary text-center">
                                                                        <div class="card_body_block_3 mt-11">
                                                                            <p class="mb-0">Membership Amount</p>
                                                                            <h5>KWD 49.000</h5>
                                                                            <h6 class="text-decoration-line-through text-gray-400">KWD
                                                                                69.000</h6>
                                                                            <p class="text-gray-400">Discount-KWD 20.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col px-5">
                                                        <div class="card border border-2">
                                                            <div class="card-body p-0">
                                                                <div class="row mx-0">
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_1">
                                                                            <h4 class="mb-5">Additional Contract Purchase</h4>
                                                                            <p>Purchase Date-01-12-2022</p>
                                                                            <p>Number of Contract Purchase - 30</p>
                                                                            <p>Cost of Per Contract - KWD 02.000</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_2 mt-11">
                                                                            <h6 class="mb-0">Payment Mode</h6>
                                                                            <p>Knet</p>
                                                                            <h6 class="mb-0">Transaction ID</h6>
                                                                            <p>#2305812dhw49320</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10 bg-light-primary text-center">
                                                                        <div class="card_body_block_3 mt-11">
                                                                            <p class="mb-0">Paid Amount</p>
                                                                            <h5>KWD 49.000</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col px-5 mb-5">
                                                        <div class="card border border-2">
                                                            <div class="card-body p-0">
                                                                <div class="row mx-0">
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_1">
                                                                            <h4 class="mb-5">Individual Premium Membership Purchase</h4>
                                                                            <p>Purchase Date-01-12-2022</p>
                                                                            <p>Next Renewal: 30-11-2023</p>
                                                                            <p>Duration: Yearly</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_2 mt-11">
                                                                            <h6 class="mb-0">Payment Mode</h6>
                                                                            <p>Knet</p>
                                                                            <h6 class="mb-0">Transaction ID</h6>
                                                                            <p>#2305812dhw49320</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10 bg-light-primary text-center">
                                                                        <div class="card_body_block_3 mt-11">
                                                                            <h5 class="mb-0">Membership Amount</h5>
                                                                            <h5>KWD 49.000</h5>
                                                                            <h6 class="text-decoration-line-through text-gray-400">KWD
                                                                                69.000</h6>
                                                                            <p class="text-gray-400">Discount-KWD 20.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="border border-2  d-flex m-5">
                                                    <div class="me-10 pt-10 ps-10">
                                                        <h4 class="mb-5">Membership Renewal</h4>
                                                        <p>Purchase Date-01-12-2022</p>
                                                        <p>Next Renewal: 30-11-2023</p>
                                                        <p>Duration: Yearly</p>
                                                    </div>
                                                    <div class="ms-20 me-10 mt-11 pt-10">
                                                        <h6>Payment Mode</h6>
                                                        <p>Knet</p>
                                                        <h6>Transaction ID</h6>
                                                        <p>#2305812dhw49320</p>
                                                    </div>
                                                    <div class="ms-20 me-13 py-10"><i class="fas fa-download text-dark"></i></div>
                                                    <div class=" pt-10 ps-15 pe-20 bg-light-primary">
                                                        <p>Membership Amount</p>
                                                        <h5>KWD 49.000</h5>
                                                        <h6 class="text-decoration-line-through text-gray-400">KWD 69.000</h6>
                                                        <p class="text-gray-400">Discount-KWD 20.000</p>
                                                    </div>
                                                </div>
                                                <div class="border border-2  d-flex m-5">
                                                    <div class=" pt-10 ps-10">
                                                        <h4 class="mb-5">Additional Contract Purchase</h4>
                                                        <p>Purchase Date-01-06-2022</p>
                                                        <p>Number of Contract Purchase - 30</p>
                                                        <p>Cost of Per Contract - KWD 02.000</p>
                                                    </div>
                                                    <div class="ms-11 me-10 mt-11 pt-10">
                                                        <h6>Payment Mode</h6>
                                                        <p>Knet</p>
                                                        <h6>Transaction ID</h6>
                                                        <p>#2305812dhw49320</p>
                                                    </div>
                                                    <div class="ms-20 me-13 py-10"><i class="fas fa-download text-dark"></i></div>
                                                    <div class=" pt-10 ps-17 pe-20 bg-light-primary ">
                                                        <p class="px-6 pt-10 ">Paid Amount</p>
                                                        <h5 class="ps-7">KWD 60.000</h5>
                                                    </div>
                                                </div>
                                                <div class="border border-2  d-flex m-5">
                                                    <div class=" pt-10 ps-10">
                                                        <h4 class="mb-5">Individual Premium Membership Purchase</h4>
                                                        <p>Purchase Date-01-12-2022</p>
                                                        <p>Next Renewal: 30-11-2023</p>
                                                        <p>Duration: Yearly</p>
                                                    </div>
                                                    <div class="mt-11 pt-10">
                                                        <h6>Payment Mode</h6>
                                                        <p>Knet</p>
                                                        <h6>Transaction ID</h6>
                                                        <p>#2305812dhw49320</p>
                                                    </div>
                                                    <div class="ms-7 me-13 py-10"><i class="fas fa-download text-dark"></i></div>
                                                    <div class=" pt-10 ps-15 pe-20 bg-light-primary">
                                                        <p>Membership Amount</p>
                                                        <h5>KWD 49.000</h5>
                                                        <h6 class="text-decoration-line-through text-gray-400">KWD 69.000</h6>
                                                        <p class="text-gray-400">Discount-KWD 20.000</p>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="tab2" role="tabpanel">
                                            <div class="card p-5 ">
                                                <div class="d-flex ">
                                                    <div class="mt-9 me-20">
                                                        <h3 class=" ms-7 me-10">All Transaction</h3>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="fv-row p-5 col-6 ms-10">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Transaction Type">
                                                                <option></option>
                                                                <option value="1">All Transaction</option>
                                                                <option value="2">Membership</option>
                                                                <option value="3">Contract</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select me-4" data-control="select2"
                                                                data-placeholder="Month">
                                                                <option></option>
                                                                <option value="1">Jan</option>
                                                                <option value="2">Feb</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Year">
                                                                <option></option>
                                                                <option value="1">2022</option>
                                                                <option value="2">2023 </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row row-cols-1 row-cols-md-12 g-4 mx-0">
                                                    <div class="col px-5">
                                                        <div class="card border border-2">
                                                            <div class="card-body p-0">
                                                                <div class="row mx-0 ">
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_1">
                                                                            <h4 class="mb-5">Membership Renewal</h4>
                                                                            <p>Purchase Date-01-12-2022</p>
                                                                            <p>Next Renewal: 30-11-2023</p>
                                                                            <p>Duration: Yearly</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_2 mt-11">
                                                                            <h6 class="mb-0">Payment Mode</h6>
                                                                            <p>Knet</p>
                                                                            <h6 class="mb-0">Transaction ID</h6>
                                                                            <p>#2305812dhw49320</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10 bg-light-primary text-center">
                                                                        <div class="card_body_block_3 mt-11">
                                                                            <p class="mb-0">Membership Amount</p>
                                                                            <h5>KWD 49.000</h5>
                                                                            <h6 class="text-decoration-line-through text-gray-400">KWD
                                                                                69.000</h6>
                                                                            <p class="text-gray-400">Discount-KWD 20.000</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="tab3" role="tabpanel">
                                            <div class="card p-5 ">
                                                <div class="d-flex ">
                                                    <div class="mt-9 me-20">
                                                        <h3 class="me-20">All Transaction</h3>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="fv-row p-5 col-6 ms-10">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Transaction Type">
                                                                <option></option>
                                                                <option value="1">All Transaction</option>
                                                                <option value="2">Membership</option>
                                                                <option value="3">Contract</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select me-4" data-control="select2"
                                                                data-placeholder="Month">
                                                                <option></option>
                                                                <option value="1">Jan</option>
                                                                <option value="2">Feb</option>
                                                            </select>
                                                        </div>
                                                        <div class="fv-row p-5 col-4">
                                                            <select class="form-select " data-control="select2"
                                                                data-placeholder="Year">
                                                                <option></option>
                                                                <option value="1">2022</option>
                                                                <option value="2">2023 </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row row-cols-1 row-cols-md-12 g-4 mx-0">
                                                    
                                                    <div class="col px-5">
                                                        <div class="card border border-2">
                                                            <div class="card-body p-0">
                                                                <div class="row mx-0">
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_1">
                                                                            <h4 class="mb-5">Additional Contract Purchase</h4>
                                                                            <p>Purchase Date-01-12-2022</p>
                                                                            <p>Number of Contract Purchase - 30</p>
                                                                            <p>Cost of Per Contract - KWD 02.000</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10">
                                                                        <div class="card_body_block_2 mt-11">
                                                                            <h6 class="mb-0">Payment Mode</h6>
                                                                            <p>Knet</p>
                                                                            <h6 class="mb-0">Transaction ID</h6>
                                                                            <p>#2305812dhw49320</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4 p-10 bg-light-primary text-center">
                                                                        <div class="card_body_block_3 mt-11">
                                                                            <p class="mb-0">Paid Amount</p>
                                                                            <h5>KWD 49.000</h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer py-4 d-flex  flex-lg-column app-footer" id="kt_footer">
            <div
                class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                <!--begin::Copyright-->
                <div class="text-dark order-2 order-md-1">
                    <span class="text-muted fw-bold me-1">© 2023</span>
                    <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                        Reserved</a>
                </div>
            </div>
        </div>
    </div>
    </div>

    </div>
    </div>

    <div class="modal fade" id="create_contact" tabindex="-1" aria-modal="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered mw-900px">
            <div class="modal-content">
                <div class="modal-header">
                    <h2>Create Contact</h2>
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                    transform="rotate(-45 6 17.3137)" fill="black"></rect>
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                    fill="black"></rect>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-lg-10 px-lg-10">

                    <div class="popup_select_folder_name mb-10">
                        <h3>Enter Civil ID</h3>
                        <input type="text" class="form-control" placeholder="Civil ID" />
                    </div>
                    <div class="popup_create_folder_btn d-flex justify-content-end">
                        <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal"
                            data-bs-target="#create_contract">Invite Contactr</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>