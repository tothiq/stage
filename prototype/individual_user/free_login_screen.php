<html lang="en">

<head>
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/iu_style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap" rel="stylesheet">


</head>

<body id="kt_body" class="bg-body">
    <div class="index_menu index_menu--active">
        <div class="index_container index_menu__wrapper">
            <div class="index_header_row">
                <div class="index_menu__logo index_menu__item">
                    <a href="https://stage.tothiq.com/">
                        <img src="../assets/images/logo.png" alt="">
                        <!-- <h3>Digital Contract Platform</h3> -->
                    </a>
                </div>
                <div class="index_menu__item d-t-none">
                    <nav class="index_menu__center-nav">
                        <ul>
                            <li>
                                <div class="index_menu__dropdown">
                                    <a href="https://stage.tothiq.com/" class="link link--gray menu__dropdown-btn">Home</a>
                                    <!-- <span><i class="mdi mdi-chevron-down"></i></span> -->
                                    <!-- <div class="menu__dropdown-content menu__dropdown-content--home">
                  <a class="link link--gray" href="01_mobileapp.html">Mobile App</a>
                  <a class="link link--gray" href="02_messenger.html">Messenger</a>
                  <a class="link link--gray link--gray-active" href="#">Web App</a>
                  <a class="link link--gray" href="04_desktop.html">Desktop App</a>
                </div> -->
                                </div>
                            </li>
                            <li><a href="https://stage.tothiq.com/prototype/individual_user/" class="link link--gray">Pricing</a></li>
                            <li><a href="https://stage.tothiq.com/help-center.php" class="link link--gray">Support</a></li>
                            <!-- <li><a href="login_page.html" class="link link--gray">Create Contract</a></li> -->
                            <!-- <li><a href="template.php" class="link link--gray">Templates</a></li> -->
                            <!-- <li><a href="help-center.php" class="link link--gray">Help Center</a></li> -->
                            <li><a href="https://stage.tothiq.com/contact_us_form.php" class="link link--gray">Get In Touch</a></li>
                            <li>
                                <div class="index_menu__dropdown d-t-none">
                                    <a class="link link--gray index_menu__dropdown-btn" style="font-family: 'Aref Ruqaa'; font-size:16px">عربــي
                                        <!-- <span><i class="mdi mdi-chevron-down"></i></span> -->
                                    </a>
                                    <!-- <div class="menu__dropdown-content">
                    <a class="link link--gray link--gray-active" href="#">En</a>
                    <a class="link link--gray" href="#">عربي</a>
                  </div> -->
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="index_menu__item">
                    <nav class="index_menu__right-nav d-l-none">
                        <ul>
                            <a href="../individual_user/free_registration_screen.php" class="btn btn-primary text-white" style="margin-right:10px">Sign Up</a>
                            <a href="../individual_user/free_login_screen.php" class="btn btn-primary text-white">Login</a>
                        </ul>
                    </nav>
                    <div class="d-none d-t-block">
                        <button type="button" class="menu__mobile-button">
                            <span><i class="mdi mdi-menu" aria-hidden="true"></i></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed login_str">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="p-10 p-lg-15 mx-auto">
                    <form class="form w-100">
                        <img src="./assets/images/logo.png" alt="">
                        <h1 class="mb-10 text-dark text-center">Log In to your Tothiq Account</h1>
                        <div class="fv-row mb-10">
                            <label class="form-label fs-6 fw-bolder text-dark required">Civil ID</label>
                            <input class="form-control form-control-lg form-control-solid" type="text" name="ID" placeholder="123412341234" autocomplete="off" />
                        </div>

                        <div class="text-center">
                            <a class="btn btn-primary w-50  mb-10" data-bs-toggle="modal" type="button" href="#exampleModalToggle4">
                                Authenticate
                            </a>
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <span>Don't have a account?</span>
                            </div>
                            <a href="index.php" class="btn btn-outline btn-outline-primary btn-active-light-primary text-capitalize">register
                                now</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="index_footer-menu">
        <div class="index_footer_row">
            <div class="index_footer-left">
                <div class="text-block-left">© 2023 Tothiq Web Design Company.</div>
            </div>
            <div class="index_footer-middle">
                <div class="index_footer_social-wrapper">
                    <a href="#." target="_blank" class="index_footer_social-link is-first w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bc8a2f32922df62802b_Linkedin.svg" loading="lazy" alt="">
                    </a>
                    <a href="#." target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bd05ad8e3573d182785_Twitter.svg" loading="lazy" alt="">
                    </a>
                    <a href="#." target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bd6a2f329796a6280a1_Instagram.svg" loading="lazy" alt="">
                    </a>
                    <a href="#." target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45be3e711c4fe07eb31df_Facebook.svg" loading="lazy" alt="">
                    </a>
                </div>
                <ol role="list" class="w-list-unstyled">
                    <!-- <li><a href="https://stage.tothiq.com/about_us.php" class="link link--gray">About</a></li> -->
                    <!-- <li><a href="https://stage.tothiq.com/template.php" class="link link--gray">Templates</a></li> -->
                    <!-- <li><a href="#." class="link link--gray">Blog</a></li> -->
                    <li><a href="#." class="link link--gray">Privacy</a></li>
                    <li><a href="#." class="link link--gray">Terms &amp; Conditions</a></li>
                    <!-- <li><a href="#." class="link link--gray">Disclaimer</a></li> -->
                </ol>
            </div>
            <div class="index_footer-right">
                <h3>Application Download</h3>
                <div class="ftr-social-btns">
                    <a class="app_btn" href="http:apple.com">
                        <img src="../assets/images/ios.jpg" alt="">
                    </a>
                    <a class="and_btn" href="http:google.com">
                        <img src="../assets/images/android.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-l">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <img class="border border-2" src="../assets/images/process_1.png" alt="">
                    </div>
                    <div class="pt-3 text-end">
                        <a type="button" class="btn btn-primary" href="free_dashboard.php">Continue</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>