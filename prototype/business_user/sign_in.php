<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="shortcut icon" href="tothiq_admin/../assets/media/logos/logo.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/css/bu_style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />

</head>

<body class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <div class="logo text-center pb-10">
                        <a href="#.">
                            <img alt="Logo" src="../assets/media/logos/logo.png" class="w-100px">
                            <!-- <p class="pt-3 fs-4 fw-bolder text-dark">Digital Contracts Platform</p> -->
                        </a>
                    </div>
                    <form class="form w-100">
                        <div class="text-center mb-10">
                            <h1 class="text-dark mb-3">Login in to your Tothiq account</h1>
                            <!-- <div class="text-gray-400 fw-bold fs-4">Welcome to Admin Panel
                            </div> -->
                        </div>

                        <div class="fv-row mb-10">
                            <label class="form-label required fs-6 fw-bolder text-dark">Civil ID</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="1234-5678-9012-3456" autocomplete="off" />
                        </div>
                        <div class="text-center mb-10">
                            <button class="btn btn-primary w-50" data-bs-toggle="modal" type="button" href="#exampleModalToggle4">Verify</button>
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <span>Don't have a account?</span>
                            </div>
                            <a class="btn btn-outline btn-outline-primary btn-active-light-primary text-capitalize" href="index.php">register
                                now</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-l">
            <div class="modal-content">
                <div class="modal-body hawati-veri" data-bs-target="" data-bs-toggle="modal">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <a href="dashboard.php">
                            <img class="border border-2" src="../assets/media/logos/process_1.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
</body>

</html>