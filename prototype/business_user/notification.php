<?php include("header_free.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container d-flex justify-content-between" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Notifications</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <div class="col-5 ps-0">
            <form data-kt-search-element="form" class="d-none d-lg-block w-100 mb-5 mb-lg-0 position-relative" autocomplete="off">
                <input type="hidden">
                <span class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                        </rect>
                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                    </svg>
                </span>
                <input type="text" class="form-control form-control-solid h-40px bg-body ps-13 fs-7 border-secondary" name="search" value="" placeholder="Type content which you would like to search" data-kt-search-element="input">
            </form>
        </div>
    </div>
    <!--end::Toolbar container-->
</div>
<div class="content d-flex flex-column pb-20 pt-0" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div class="kt_content_container_inr">
                <div class="notice d-flex border-secondary border border-2 p-6 mb-10 bg-white">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                        <i class="fas fa-user-circle fa-lg"></i>
                    </span>
                    <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                        <div class="mb-3 mb-md-0 fw-bold">
                            <div class="notification_block_heading d-flex justify-content-between">
                                <h4 class="text-gray-900 fw-bolder">Amaira has accepted rent contract. 1 Minute
                                    Ago</h4>
                                <div class="notification_block_date d-flex">
                                    <p>3rd Dec 2022 11:5221</p>
                                    <a href="#."><i class="fas fa-trash-alt fa-lg ms-5 text-hover-danger"></i></a>
                                </div>
                            </div>
                            <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione
                                tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest
                                libido frustra miseri qui incurrit odium sul obiecti</div>
                        </div>
                    </div>
                </div>
                <div class="notice d-flex border-secondary border border-2 p-6 my-10 bg-white">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                        <i class="fas fa-user-circle fa-lg"></i>
                    </span>
                    <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                        <div class="mb-3 mb-md-0 fw-bold">
                            <div class="notification_block_heading d-flex justify-content-between">
                                <h4 class="text-gray-900 fw-bolder">Amaira has accepted rent contract. 1 Minute
                                    Ago</h4>
                                <div class="notification_block_date d-flex">
                                    <p>3rd Dec 2022 11:5221</p>
                                    <a href="#."><i class="fas fa-trash-alt fa-lg ms-5 text-hover-danger"></i></a>
                                </div>
                            </div>
                            <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione
                                tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest
                                libido frustra miseri qui incurrit odium sul obiecti</div>
                        </div>
                    </div>
                </div>
                <div class="notice d-flex border-secondary border border-2 p-6 my-10 bg-white">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                        <i class="fas fa-user-circle fa-lg"></i>
                    </span>
                    <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                        <div class="mb-3 mb-md-0 fw-bold">
                            <div class="notification_block_heading d-flex justify-content-between">
                                <h4 class="text-gray-900 fw-bolder">Special Renewal Offer -25% Off</h4>
                                <div class="notification_block_date d-flex">
                                    <p>3rd Dec 2022 11:5221</p>
                                    <a href="#."><i class="fas fa-trash-alt fa-lg ms-5 text-hover-danger"></i></a>
                                </div>
                            </div>
                            <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione
                                tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest
                                libido frustra miseri qui incurrit odium sul obiecti</div>
                        </div>
                    </div>
                </div>
                <div class="notice d-flex border-secondary border border-2 p-6 my-10 bg-white">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                        <i class="fas fa-user-circle fa-lg"></i>
                    </span>
                    <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                        <div class="mb-3 mb-md-0 fw-bold">
                            <div class="notification_block_heading d-flex justify-content-between">
                                <h4 class="text-gray-900 fw-bolder">Special Renewal Offer -25% Off</h4>
                                <div class="notification_block_date d-flex">
                                    <p>3rd Dec 2022 11:5221</p>
                                    <a href="#."><i class="fas fa-trash-alt fa-lg ms-5 text-hover-danger"></i></a>
                                </div>
                            </div>
                            <div class="fs-6 text-gray-700 pe-7">Pest vetum promissa memini cuius adeptione
                                tupis: quem pollicitus est aversione aversi et fuga. Qui autem de re dest
                                libido frustra miseri qui incurrit odium sul obiecti</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Invite Contact  -->
<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100">
                    <div class=" d-flex flex-wrap">
                        <div class="fv-row p-5 col-12">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Email Address" autocomplete="off" />
                        </div>
                    </div>
                    <div class="text-center btncolorblue pt-10">
                        <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Invite Contact  -->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_free.php") ?>