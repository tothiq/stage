<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bu_style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/bu_media.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<!--begin::Body-->

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">

    <div class="page d-flex flex-row">
        <!--begin::Aside-->
        <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
            <!--begin::Brand-->
            <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                <!--begin::Logo-->
                <a href="basic_dashboard.php" class="open_sidebar_logo"><img src="../assets/images/logo.png" alt=""></a>
                <a href="basic_dashboard.png" class="close_sidebar_logo"><img src="../assets/images/T_logo.png" alt=""></a>
                <!--end::Logo-->
                <!--begin::Aside toggler-->
                <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                    <span class="svg-icon svg-icon-1 rotate-180">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="black"></path>
                            <path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="black"></path>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Aside toggler-->
            </div>
            <!--end::Brand-->
            <!--begin::Aside menu-->
            <div class="aside-menu flex-column-fluid">
                <!--begin::Aside Menu-->
                <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0" style="height: 286px;">
                    <!--begin::Menu-->
                    <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
                        <div class="menu-item">
                            <a class="menu-link" href="basic_dashboard.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        fill: #183052;
                                                    }
                                                </style>
                                            </defs>
                                            <path class="cls-1" d="m65.28,241.34c-6.65,5.52-12.12,10.33-17.89,14.76-6.06,4.66-12.57,8.43-20.74,5.99-8.39-2.51-12.32-8.93-13.85-16.9-1.42-7.42,2.19-13.22,7.61-17.82,32.59-27.62,65.25-55.16,97.89-82.72,39.09-33.01,78.18-66.01,117.26-99.03,10.65-9,19.38-9.09,29.91-.17,71.35,60.5,142.68,121,214.02,181.51,12.67,10.74,9.87,29.51-5.23,34.97-7.77,2.81-13.94-.92-19.75-5.25-6.27-4.67-12.29-9.68-19.45-15.36v6.9c0,47.7.11,95.4-.06,143.09-.05,14.47-2.78,28.39-11.09,40.75-11.13,16.55-26.72,25.78-46.37,27.96-8.17.91-16.46,1.06-24.69,1.07-72.66.08-145.32.14-217.98-.03-17.45-.04-33.64-4.3-47.25-16.25-12.48-10.95-19.36-24.67-21.19-40.91-.92-8.17-1.07-16.45-1.09-24.69-.1-43.48-.05-86.96-.05-130.44v-7.46Zm105.71,180.06v-7.15c0-38.17-.02-76.34,0-114.5.01-22.87,13.76-36.54,36.75-36.55,28.26-.02,56.54.58,84.78-.19,21.71-.59,37.43,16.22,37.02,37.23-.74,38.15-.21,76.33-.21,114.5v7c12.45,0,24.32,0,36.2,0,20.44-.01,30.09-9.6,30.1-29.94,0-59.45-.04-118.91.09-178.36,0-4.23-1.22-7.01-4.49-9.77-34.18-28.91-68.21-58-102.29-87.03-12.82-10.92-25.66-21.83-38.63-32.86-1.16.86-2.05,1.45-2.86,2.14-46.51,39.61-93.05,79.2-139.43,118.96-1.86,1.59-3.19,4.89-3.2,7.4-.18,60.19-.14,120.37-.13,180.56,0,2.02,0,4.05.22,6.05,1.29,11.68,9.89,21.56,21.57,22.3,14.57.92,29.25.22,44.5.22Zm39.8-118.61v118.56h78.75v-118.56h-78.75Z"></path>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="menu-title fs-5 ms-2">Home</span>
                            </a>
                        </div>
                        <!-- <div class="menu-item">
                            <a class="menu-link" href="basic_profile&amp;activation.php">
                                <span class="menu-icon">
                                    
                                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                        <defs>
                                            <style>
                                                .cls-1 {
                                                    fill: #183052;
                                                }
                                            </style>
                                        </defs>
                                        <path d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"></path>
                                    </svg>
                                    
                                </span>
                                <span class="menu-title fs-5 ms-2 ">Profile</span>
                            </a>
                        </div> -->
                        <div class="menu-item">
                            <a class="menu-link" href="basic_contract.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        fill: #183052;
                                                    }
                                                </style>
                                            </defs>
                                            <path class="" d="m427.95,284.81c0,46.38.07,92.77-.04,139.15-.04,14.54-3.61,28.17-12.64,39.86-12.33,15.94-29.21,23.37-48.86,23.45-77.68.3-155.36.29-233.04.03-29.5-.1-51.84-17.9-59.27-46.46-1.43-5.51-1.97-11.39-1.98-17.1-.11-115.86-.1-231.71-.07-347.57,0-30.73,16.5-53.11,45.5-61.28,5.66-1.59,11.76-2.28,17.65-2.29,53.37-.17,106.73-.09,160.1-.11,19.24,0,35.57,6.35,49.2,20.4,20.66,21.3,41.67,42.28,62.93,62.98,14.16,13.78,20.61,30.26,20.56,49.78-.11,46.38-.03,92.77-.03,139.15ZM279.8,56.79h-6.31c-46.17,0-92.33-.01-138.5,0-13.11,0-18.7,5.67-18.7,18.91,0,116.3-.01,232.59,0,348.89,0,13.56,5.03,18.51,18.77,18.51,76.67.01,153.34.01,230.01,0,14.04,0,18.43-4.64,17.71-18.43-.09-1.65-.02-3.3-.02-4.95,0-83.51,0-167.02,0-250.53v-8.46c-2.93,0-5.15,0-7.37,0-21.02,0-42.05.38-63.06-.23-6.53-.19-13.6-1.89-19.35-4.94-10.62-5.63-13.11-16.27-13.15-27.47-.09-23.48-.03-46.96-.03-71.31Z"></path>
                                            <path class="" d="m249.92,220.39c23.91,0,47.82-.07,71.73.03,15.54.06,25.77,6.47,29.95,19.09,1.4,4.22,2,8.86,2.05,13.32.23,17.73.15,35.47.08,53.2-.08,21.46-11.42,32.83-32.88,32.86-47.2.06-94.4.06-141.6,0-21.51-.02-32.91-11.37-33.01-32.74-.08-17.73-.09-35.47,0-53.2.11-21.05,11.51-32.47,32.57-32.55,23.7-.09,47.41-.02,71.11-.02Z"></path>
                                            <path class="" d="m197.71,131.03c-11.72,0-23.45.16-35.16-.07-6.19-.12-11.95-1.89-14.64-8.27-2.31-5.46-2.22-11.11,2.08-15.49,2.27-2.32,5.8-4.75,8.79-4.8,26.07-.4,52.17-.89,78.22,0,10.42.35,15.03,10.04,11.41,19.94-2.65,7.25-9.04,8.47-15.53,8.62-11.72.27-23.44.08-35.16.07Z"></path>
                                            <path class="" d="m197.67,190.42c-11.72,0-23.45.17-35.16-.07-6.19-.12-11.94-1.92-14.61-8.31-2.29-5.46-2.4-11.42,2.18-15.41,3.28-2.86,8.17-5.3,12.39-5.39,23.64-.51,47.29-.45,70.94-.16,12.33.15,18.67,8.98,15.18,20.1-1.82,5.8-7.26,9.12-15.75,9.21-11.72.13-23.44.03-35.16.02Z"></path>
                                            <path class="" d="m301.75,398.3c-11.72,0-23.45.21-35.16-.06-12.15-.28-18.78-8.96-15.32-19.5,1.97-5.99,7.01-9.72,14.88-9.78,23.85-.2,47.71-.32,71.56.03,11.99.18,18.12,8.96,14.82,19.88-1.79,5.92-7.12,9.29-15.6,9.4-11.72.14-23.44.04-35.16.03Z"></path>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="menu-title fs-5 ms-2">Contracts</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link" href="basic_contact.php">
                                <span class="menu-icon">

                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                            <path d="M272 288h-64C163.8 288 128 323.8 128 368C128 376.8 135.2 384 144 384h192c8.836 0 16-7.164 16-16C352 323.8 316.2 288 272 288zM240 256c35.35 0 64-28.65 64-64s-28.65-64-64-64c-35.34 0-64 28.65-64 64S204.7 256 240 256zM496 320H480v96h16c8.836 0 16-7.164 16-16v-64C512 327.2 504.8 320 496 320zM496 64H480v96h16C504.8 160 512 152.8 512 144v-64C512 71.16 504.8 64 496 64zM496 192H480v96h16C504.8 288 512 280.8 512 272v-64C512 199.2 504.8 192 496 192zM384 0H96C60.65 0 32 28.65 32 64v384c0 35.35 28.65 64 64 64h288c35.35 0 64-28.65 64-64V64C448 28.65 419.3 0 384 0zM400 448c0 8.836-7.164 16-16 16H96c-8.836 0-16-7.164-16-16V64c0-8.838 7.164-16 16-16h288c8.836 0 16 7.162 16 16V448z"></path>
                                        </svg>
                                    </span>

                                </span>
                                <span class="menu-title fs-5 ms-2">Address Book</span>
                            </a>
                        </div>
                        <div data-kt-menu-trigger="click" class="menu-item here menu-accordion"><!--begin:Menu link-->

                            <a class="menu-link fs-5">
                                <span class="menu-icon">

                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                            <path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352c53 0 96-43 96-96s-43-96-96-96s-96 43-96 96s43 96 96 96z" />
                                        </svg>
                                    </span>

                                </span>
                                <span class="menu-title ms-2">Switch Account</span>
                                <span class="menu-arrow">
                                </span>
                            </a><!--end:Menu link--><!--begin:Menu sub-->
                            <div class="menu-sub menu-sub-accordion" style="display: none; overflow: hidden;"><!--begin:Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="../individual_user/basic_dashboard.php" class="menu-link px-5 fs-5 ps-18">
                                        Individual User
                                    </a>
                                </div>
                                <!--end::Menu item-->

                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="../business_user/basic_dashboard.php" class="menu-link px-5 fs-5 ps-18">
                                        Business User
                                    </a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link" href="#.">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                            <path d="M506.1 127.1c-17.97-20.17-61.46-61.65-122.7-71.1c-22.5-3.354-45.39 3.606-63.41 18.21C302 60.47 279.1 53.42 256.5 56.86C176.8 69.17 126.7 136.2 124.6 139.1c-7.844 10.69-5.531 25.72 5.125 33.57c4.281 3.157 9.281 4.657 14.19 4.657c7.406 0 14.69-3.375 19.38-9.782c.4062-.5626 40.19-53.91 100.5-63.23c7.457-.9611 14.98 .67 21.56 4.483L227.2 168.2C214.8 180.5 207.1 196.1 207.1 214.5c0 17.5 6.812 33.94 19.16 46.29C239.5 273.2 255.9 279.1 273.4 279.1s33.94-6.813 46.31-19.19l11.35-11.35l124.2 100.9c2.312 1.875 2.656 5.251 .5 7.97l-27.69 35.75c-1.844 2.25-5.25 2.594-7.156 1.063l-22.22-18.69l-26.19 27.75c-2.344 2.875-5.344 3.563-6.906 3.719c-1.656 .1562-4.562 .125-6.812-1.719l-32.41-27.66L310.7 392.3l-2.812 2.938c-5.844 7.157-14.09 11.66-23.28 12.6c-9.469 .8126-18.25-1.75-24.5-6.782L170.3 319.8H96V128.3L0 128.3v255.6l64 .0404c11.74 0 21.57-6.706 27.14-16.14h60.64l77.06 69.66C243.7 449.6 261.9 456 280.8 456c2.875 0 5.781-.125 8.656-.4376c13.62-1.406 26.41-6.063 37.47-13.5l.9062 .8126c12.03 9.876 27.28 14.41 42.69 12.78c13.19-1.375 25.28-7.032 33.91-15.35c21.09 8.188 46.09 2.344 61.25-16.47l27.69-35.75c18.47-22.82 14.97-56.48-7.844-75.01l-120.3-97.76l8.381-8.382c9.375-9.376 9.375-24.57 0-33.94c-9.375-9.376-24.56-9.376-33.94 0L285.8 226.8C279.2 233.5 267.7 233.5 261.1 226.8c-3.312-3.282-5.125-7.657-5.125-12.31c0-4.688 1.812-9.064 5.281-12.53l85.91-87.64c7.812-7.845 18.53-11.75 28.94-10.03c59.75 9.22 100.2 62.73 100.6 63.29c3.088 4.155 7.264 6.946 11.84 8.376H544v175.1c0 17.67 14.33 32.05 31.1 32.05L640 384V128.1L506.1 127.1zM48 352c-8.75 0-16-7.245-16-15.99c0-8.876 7.25-15.99 16-15.99S64 327.2 64 336.1C64 344.8 56.75 352 48 352zM592 352c-8.75 0-16-7.245-16-15.99c0-8.876 7.25-15.99 16-15.99s16 7.117 16 15.99C608 344.8 600.8 352 592 352z" />
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="menu-title fs-5 ms-2">Support</span>
                            </a>
                        </div>
                        <div class="menu-item">
                            <a class="menu-link" href="basic_sign_in.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                            <path d="M534.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-128-128c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L434.7 224 224 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l210.7 0-73.4 73.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l128-128zM192 96c17.7 0 32-14.3 32-32s-14.3-32-32-32l-64 0c-53 0-96 43-96 96l0 256c0 53 43 96 96 96l64 0c17.7 0 32-14.3 32-32s-14.3-32-32-32l-64 0c-17.7 0-32-14.3-32-32l0-256c0-17.7 14.3-32 32-32l64 0z" />
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                </span>
                                <span class="menu-title fs-5 ms-2">Log Out</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="separator  text-dark"></div>
                <div class="aside-footer flex-column-auto pt-4 pb-4 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="kt_aside_footer">
                    <div class="menu-item">
                        <a class="menu-link pe-none">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="../assets/images/logo(1).png" alt="" class="h-40x w-40px rounded-circle">
                                </span>
                            </span>
                            <span class="menu-title fs-5 fw-bolder cursor-default">Business Account</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- End :: Menu Area -->
    <!-- Start :: Content Area-->
    <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
        <!--begin::Header-->
        <div id="kt_header" class="header align-items-center px-9 d-flex justify-content-end">
            <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                <a href="#." class="d-lg-none">
                    <img alt="Logo" src="../assets/images/logo.png" class="h-30px">
                </a>
            </div>
            <!-- <div style="margin-top: 0px; margin-right: 20px;">
                <a href="basic_create_cont_step1.php" class="btn btn-sm btn-primary d-lg-none">Create Contracts</a>
            </div> -->
            <div style="margin-top: 0px; margin-right: 10px;">
                <a href="basic_create_cont_step1.php" class="btn btn-sm btn-primary d-none d-lg-block">Create
                    Contract</a>
            </div>
            <div class="d-flex">
                <div class="">
                    <a class="menu-link" href="basic_notification.php">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                    <defs>
                                        <style>
                                            .cls-1 {
                                                fill: #183052;
                                            }
                                        </style>
                                    </defs>
                                    <path class="cls-1" d="m219.83,57.41c.42-6.52.45-14.16,1.51-21.66,1.95-13.74,13.96-23.25,28.49-23.25,14.53,0,26.54,9.52,28.49,23.25,1.06,7.5,1.09,15.14,1.6,22.99,7.39,2.08,15.03,3.74,22.32,6.37,51.87,18.71,83.3,55.33,93.24,109.49,2.73,14.85,2.55,30.35,2.6,45.56.09,30.07,4.87,59.04,17.56,86.47,9.08,19.62,21.89,36.76,35.33,53.51,9.84,12.26,9.22,25.33-2.62,33.25-4.92,3.29-11.9,4.89-17.95,4.91-102.06.29-204.12.21-306.18.21-19.18,0-38.35.13-57.52-.05-14.75-.14-25.85-10.36-24.43-23.46.58-5.32,3.64-10.92,7-15.29,13.71-17.83,27.3-35.59,36.36-56.47,10.66-24.58,15.28-50.2,15.95-76.87.48-19.1.36-38.51,3.8-57.18,7.92-42.97,32.95-74.55,71.11-95.05,13.26-7.12,28.25-11.01,43.33-16.71Zm171.25,296.44c-2.49-4.18-4.38-7.34-6.26-10.52-17.99-30.54-28.18-63.48-29.94-98.9-.97-19.55-.3-39.25-2.41-58.66-3.82-35.06-22.42-60.68-54.51-75.35-25.47-11.64-52.54-12.14-79.27-5.63-36.78,8.96-59.93,32.61-69.63,69.37-4.39,16.62-3.26,33.56-3.29,50.42-.05,37.67-7.29,73.68-24.57,107.36-3.72,7.25-8.01,14.22-12.37,21.91h282.24Z" />
                                    <path class="cls-1" d="m308.81,428.92c-.55,33.65-28.35,58.34-58.65,58.58-29.66.23-58.64-23.89-59.3-58.58h117.95Z" />
                                </svg>
                            </span>

                        </span>
                        <!-- <span class=" menu-title">Notification</span> -->
                    </a>
                </div>
            </div>
        </div>
        <!-- <div id="kt_app_toolbar" class="app-toolbar pt-3 pt-lg-6">
            <div id="kt_app_toolbar_container" class="app-container px-7 d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Tothiq - Dashboard</h1>
                </div>
            </div>
        </div> -->
        <!--end::Header-->