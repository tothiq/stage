<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/bu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <div class="d-flex flex-column flex-root">
        <div class="page d-flex flex-row">
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="new_draft kt_header" class="header align-items-stretch">
                    <!-- <div class="row m-0 align-items-center border border-3"> -->
                    <!-- <div class="col-1 d-flex align-items-center p-5">
                            <a href="#."><i class="fas fa-bars fa-lg"></i></a>
                        </div> -->
                    <div class="col-1 d-flex align-items-center p-5">
                        <a href="pre_dashboard.php"><i class="fas fa-home fs-1"></i></a>
                        <a href="pre_contract.php" class="ms-5"><i class="fas fa-angle-double-left fs-1"></i></a>
                    </div>
                    <div class="col-5 d-flex justify-content-center align-items-center">
                        <h4 class="mb-0 ms-5">Rental Aggrement For Al-John Tower Office With Tothiq</h4>
                    </div>
                    <div class="col-2 d-flex justify-content-center align-items-center border border-top-0 border-bottom-0 border-3">
                        <i class="fas fa-users fa-lg text-warning"></i>
                        <h4 class="mb-0 mx-5">4 Parties</h4>
                    </div>
                    <div class="col-4 d-flex justify-content-end align-items-center pe-9">
                        <div class="d-flex justify-content-center align-items-center pe-3">
                            <span class="fs-6 fw-bold text-danger text-decoration-underline">Status : Deleted</span>
                        </div>
                        <div class="create_contract_btn d-flex align-items-center">
                            <!-- <i class="fas fa-file-alt fa-lg"></i> -->
                            <div style="margin-left: 5px;">
                                <a href="pre_dashboard.php">
                                    <img src="../assets/images/bin.png" alt="" class="bin_icon">
                                </a>
                            </div>
                            <div style="margin-left: 10px;margin-right:10px">
                                <a href="pre_dashboard.php" class="menu-link">
                                    <span class="menu-icon">
                                        <img src="../assets/images/download.png" alt="" class="download_icon">

                                    </span>
                                </a>
                            </div>
                            <div class="">
                                <a class="menu-link" href="pre_notification.php">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                                <defs>
                                                    <style>
                                                        .cls-1 {
                                                            fill: #183052;
                                                        }
                                                    </style>
                                                </defs>
                                                <path class="cls-1" d="m219.83,57.41c.42-6.52.45-14.16,1.51-21.66,1.95-13.74,13.96-23.25,28.49-23.25,14.53,0,26.54,9.52,28.49,23.25,1.06,7.5,1.09,15.14,1.6,22.99,7.39,2.08,15.03,3.74,22.32,6.37,51.87,18.71,83.3,55.33,93.24,109.49,2.73,14.85,2.55,30.35,2.6,45.56.09,30.07,4.87,59.04,17.56,86.47,9.08,19.62,21.89,36.76,35.33,53.51,9.84,12.26,9.22,25.33-2.62,33.25-4.92,3.29-11.9,4.89-17.95,4.91-102.06.29-204.12.21-306.18.21-19.18,0-38.35.13-57.52-.05-14.75-.14-25.85-10.36-24.43-23.46.58-5.32,3.64-10.92,7-15.29,13.71-17.83,27.3-35.59,36.36-56.47,10.66-24.58,15.28-50.2,15.95-76.87.48-19.1.36-38.51,3.8-57.18,7.92-42.97,32.95-74.55,71.11-95.05,13.26-7.12,28.25-11.01,43.33-16.71Zm171.25,296.44c-2.49-4.18-4.38-7.34-6.26-10.52-17.99-30.54-28.18-63.48-29.94-98.9-.97-19.55-.3-39.25-2.41-58.66-3.82-35.06-22.42-60.68-54.51-75.35-25.47-11.64-52.54-12.14-79.27-5.63-36.78,8.96-59.93,32.61-69.63,69.37-4.39,16.62-3.26,33.56-3.29,50.42-.05,37.67-7.29,73.68-24.57,107.36-3.72,7.25-8.01,14.22-12.37,21.91h282.24Z">
                                                </path>
                                                <path class="cls-1" d="m308.81,428.92c-.55,33.65-28.35,58.34-58.65,58.58-29.66.23-58.64-23.89-59.3-58.58h117.95Z">
                                                </path>
                                            </svg>
                                        </span>

                                    </span>
                                    <!-- <span class=" menu-title">Notification</span> -->
                                </a>
                            </div>
                        </div>

                    </div>
                    <!-- </div> -->
                    <div id="new_draft kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                        <div class="aside-menu flex-column-fluid">
                            <div class="aside-menu flex-column-fluid">
                                <div class="contract_left_sec border border-top-0 p-0 bg-white">
                                    <ul class="nav border-0 flex-row flex-md-column mb-3 mb-md-0 fs-6">
                                        <li class="nav-item  me-0 border-bottom border-3 p-4 d-flex justify-content-between">
                                            <a class="fs-5 fw-bold">Comments</a>
                                            <i class="fas fa-expand-alt fa-lg align-items-center d-flex"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card" id="kt_chat_messenger">
                                    <!--begin::Card body-->
                                    <div class="card-body p-5" id="kt_chat_messenger_body">
                                        <!--begin::Messages-->
                                        <div class="scroll-y me-n5 pe-5 h-500px h-lg-auto" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="-2px" style="max-height: 195px;">
                                            <!--begin::Message(in)-->
                                            <div class="d-flex justify-content-start mb-10">
                                                <!--begin::Wrapper-->
                                                <div class="d-flex flex-column align-items-start">
                                                    <!--begin::User-->
                                                    <div class="d-flex align-items-center mb-2">
                                                        <!--begin::Details-->
                                                        <div>
                                                            <a href="#" class="fs-6 fw-bolder text-gray-900 text-hover-primary me-1">Aabirah
                                                                Aadab Aadil</a>
                                                            <p class="text-muted fs-7 mb-1">48 hours ago</p>
                                                        </div>
                                                        <!--end::Details-->
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::Text-->
                                                    <div class="rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">Sic de isto et tutius perducit ad
                                                        actum ipsum, ut si dico "Ego autem vadam lavari, ut mens</div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Message(in)-->
                                            <!--begin::Message(in)-->
                                            <div class="d-flex justify-content-start mb-10">
                                                <!--begin::Wrapper-->
                                                <div class="d-flex flex-column align-items-start">
                                                    <!--begin::User-->
                                                    <div class="d-flex align-items-center mb-2">
                                                        <!--begin::Details-->
                                                        <div>
                                                            <a href="#" class="fs-6 fw-bolder text-gray-900 text-hover-primary me-1">Aabirah
                                                                Aadab Aadil</a>
                                                            <p class="text-muted fs-7 mb-1">48 hours ago</p>
                                                        </div>
                                                        <!--end::Details-->
                                                    </div>
                                                    <!--end::User-->
                                                    <!--begin::Text-->
                                                    <div class="rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">Sic de isto et tutius perducit ad
                                                        actum ipsum, ut si dico "Ego autem vadam lavari, ut mens</div>
                                                    <!--end::Text-->
                                                </div>
                                                <!--end::Wrapper-->
                                            </div>
                                            <!--end::Message(in)-->
                                        </div>
                                        <!--end::Messages-->
                                    </div>
                                    <!--end::Card body-->
                                    <!--begin::Card footer-->
                                    <div class="under_review card-footer p-5 d-flex justify-content-between text-light position-fixed bottom-0 start-0" style="width: 263px;z-index: 9;">
                                        <!--begin::Input-->
                                        <div>
                                            <textarea class="form-control p-2" placeholder="Type your comment" rows="1" id="floatingTextarea"></textarea>
                                        </div>
                                        <!--end::Input-->
                                        <!--begin:Toolbar-->
                                        <div class="d-flex flex-stack align-items-center justify-content-center">
                                            <!--begin::Actions-->
                                            <button class="btn btn-primary btn-sm btn-icon btn-active-dark p-1" type="button" data-bs-toggle="tooltip" title="" data-bs-original-title="Send">
                                                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 566.93 566.93">
                                                    <defs>
                                                        <style>
                                                            .cls-2 {
                                                                fill: #fff;
                                                            }
                                                        </style>
                                                    </defs>
                                                    <path class="cls-2" d="M98.71,470.86c-20.89.15-33.83-18.64-27.37-36.13,3.09-8.36,7.59-16.21,11.59-24.22,19.2-38.48,38.13-77.1,57.9-115.29,4.69-9.06,4.35-16-.22-24.85q-33.44-65.16-65.46-131c-3.34-6.88-5.75-15.62-4.72-23,2.46-17.33,19.33-24.91,37.3-17.45,30.57,12.69,60.89,26,91.31,39q137.49,59,275,117.84c12.82,5.46,23.34,12.17,23.19,27.7s-10.72,22.31-23.45,27.75Q294.25,388,114.79,465C108.67,467.61,102.28,469.57,98.71,470.86Zm287.7-206.69.71-3.57L125.65,148.48c16.19,38.54,34,74.65,52.6,110.32,1.7,3.26,8.88,5.09,13.54,5.14,39,.4,78,.23,117,.23ZM124,414.63l2,2.75L388.62,304.65c-2.77-1.86-3.42-2.68-4.07-2.68-65.44-.1-130.89-.23-196.33.07-3.6,0-9,2.78-10.52,5.8C159.47,343.27,141.8,379,124,414.63Z" />
                                                </svg>
                                            </button>
                                            <!--end::Actions-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </div>
                                    <!--end::Card footer-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-xxl my-5">
                    <div class="card mt-5" style="height: 530px;">
                        <div class="card-body">
                            <!--begin::Scroll-->
                            <div>
                                <p>Quando ambulabat agendis admonere to qualis actio. Si ad corpus, quae plerumque
                                    Imaginare tecum in balineo quidam aquam fundes aliquod discrimen vituperiis usum
                                    alii furantur.</p>

                                <p>Sic de isto et tutius perducit ad actum ipsum, ut si dico "Ego autem vadam
                                    lavari, ut mens mea in statu naturae conformior." Et similiter circa alias res.
                                    Et sic, si contingit ex per se lavantem, et erit hoc paratus ut diceret, "Hoc
                                    non solum lavari ut desideravit, sed ut animus in statu naturae convenienter
                                    naturae, et non ut si ego quæ ventura offendit."</p>

                                <p>Sollicitant homines non sunt nisi quam formae rerum principiis opiniones. Mors
                                    enim est terribilis ut Socrati aliud esse apparet. Sed timor mortis est notio
                                    terribile. Cum igitur impediantur turbentur aut dolere videamus aliis non
                                    tribuunt nisi nobis hoc est, in principiis nostra.</p>

                                <p>Ponam in culpa idiota aliis pravitatis. Principium ponere culpam in se justum
                                    praeceptum. Neque improperes et aliis qui non perfecte ipse docuit.</p>

                                <p>Quod Enchiridion Epictetus stoici scripsit. Rodrigo Abela et Technologiae apud
                                    Massachusetts instituta Opera collectio. Ex anglicus latine translata sunt.
                                </p>

                                <p>Quidam alii sunt, et non est in nostra potestate. Quae omnia in nostra sententia,
                                    pursuit, cupiditatem, aversatio, ex quibus in Verbo, quicquid non suis actibus
                                    nostris. Non sunt in nostra potestate corpore bona fama imperii, denique quod
                                    non sunt actus.</p>

                                <p>Et omnia in potate nostra esse natura liber, libera, libere valeant, sed illis
                                    non est in nostra potestate sunt infirmi, servilis, licet, lex pertinet</p>
                            </div>
                            <!--end::Scroll-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer py-4 d-flex  flex-lg-column app-footer position-fixed end-0 bottom-0 w-100" id="kt_footer">
            <!--begin::Container-->
            <div class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                <!--begin::Copyright-->
                <div class="text-dark order-2 order-md-1">
                    <span class="text-muted fw-bold me-1">© 2023</span>
                    <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                        Reserved</a>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>
    <!-- Modal Invite Contact  -->
    <div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xs">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="form w-100">
                        <div class=" d-flex flex-wrap">
                            <div class="fv-row p-5 col-12">
                                <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                                <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Email Address" autocomplete="off" />
                            </div>
                        </div>
                        <div class="text-center btncolorblue pt-10">
                            <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Invite Contact  -->

    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
                <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function() {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function() {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function() {
            $('#addresstype').on('change', function() {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function() {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function() {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function() {
            var max_height = 0;
            $(this).each(function() {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function() {
                $(this).height(max_height);
            });
        };

        $(document).ready(function() {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>