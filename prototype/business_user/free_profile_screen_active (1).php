<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bu_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/bu_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true"
                data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="free_dashboard.php">
                        <img alt="Logo" src="./images/logo.png" class="w-60px" />
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                    <!--end::Logo-->
                    <!--begin::Aside toggler-->
                    <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
                        data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
                        data-kt-toggle-name="aside-minimize">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                        <span class="svg-icon svg-icon-1 rotate-180">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <path opacity="0.5"
                                    d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                                    fill="black" />
                                <path
                                    d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                                    fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Aside toggler-->
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
                        <!--begin::Menu-->
                        <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                            id="#kt_aside_menu" data-kt-menu="true">
                            <div class="menu-item">
                                <!-- <div class="menu-content pb-2">
                    <span class="menu-section text-muted text-uppercase fs-8 ls-1">Dashboard</span>
                </div> -->
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="free_dashboard.php">
                                    <span class="menu-icon">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-th-large fa-lg"></i>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="draft.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="fas fa-file-contract fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contract</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="contacts.php">
                                    <span class="menu-icon">

                                        <span class="svg-icon svg-icon-2">
                                            <i class="far fa-address-book fa-lg"></i>
                                        </span>

                                    </span>
                                    <span class="menu-title">Contact</span>
                                </a>
                            </div>
                        </div>
                        <!--end::Menu-->
                    </div>
                    <div class="aside-footer flex-column-auto pt-5 pb-7 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                        id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link" href="notification.php">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <i class="fas fa-bell fa-lg""></i>
                                    </span>
                   
                                </span>
                                <span class=" menu-title">Notification</span>
                            </a>
                        </div>
                        <div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
                            <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown text-hover-primary position-relative"
                                data-kt-menu-trigger="hover" data-kt-menu-attach="parent"
                                data-kt-menu-placement="right-end">
                                <i class="fas fa-user-circle fa-lg ms-4 me-4"></i> User Profile
                                <span
                                    class="position-absolute top-0 start-25 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                </span>
                            </div>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px "
                                data-kt-menu="true" data-popper-placement="right-end"
                                style="z-index: 105; position: fixed; inset: 0px 0px auto auto; margin: 0px; transform: translate3d(-30px, 65px, 0px);">
                                <div class="menu-item  bg-light-danger p-5 text-dark">
                                    <h2>Tothiq User</h2>
                                    <p>Individual profile</p>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="free_profile_screen_active.php" class="menu-link px-5">
                                        <div class="ps-5 pt-5 d-flex"><i
                                                class="bi bi-person-circle me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>My profile</p>
                                            </span></div>
                                    </a>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="#." class="menu-link px-5">
                                        <div class="ps-5 d-flex"><i
                                                class="bi bi-calculator-fill me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>Transaction History</p>
                                            </span></div>
                                    </a>
                                </div>
                                <div class="separator border-3 my-5"></div>
                                <div class="menu-item px-5">
                                    <a href="free_login_screen.php" class="menu-link px-5">
                                        <div class="ps-5 d-flex"><i
                                                class="bi bi-box-arrow-right me-2 mt-1 text-dark fa-1x"></i><span>
                                                <p>Log Out</p>
                                            </span></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="user_profile_modal" >
        <div class="modal fade " id="profilepop1" aria-hidden="true" aria-labelledby="profilepop1" tabindex="-1"
            data-bs-dismiss="modal">
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div>
                            <div class="bg-light-danger p-5">
                                <h2>Alen Leo</h2>
                                <p>Individual profile</p>
                            </div>
                            <div class="ps-5 pt-5 d-flex"><i class="bi bi-person-circle me-2 mt-1 text-dark fa-1x"></i><span><p>My profile</p></span></div>
                            <div class="ps-5 d-flex"><i class="bi bi-calculator-fill me-2 mt-1 text-dark fa-1x"></i><span><p>Transaction History</p></span></div>
                            <div class="separator border-3 my-5"></div>
                            <div class="ps-5 d-flex"><i class="bi bi-box-arrow-right me-2 mt-1 text-dark fa-1x"></i><span><p>Log Out</p></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

        <div>
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="kt_header" class="header align-items-stretch">
                    <div class="container-fluid d-flex align-items-center justify-content-between">

                    </div>
                </div>
                <div class="content d-flex flex-column p-0" id="kt_content">
                    <div class="toolbar" id="kt_toolbar">
                        <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                            <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                                class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                                <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Profile</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" class="container-xxl">
                        <div class="d-flex flex-column flex-xl-row">
                            <div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10 ">
                                <div class="card mb-5 mb-xl-8">
                                    <!--begin::Card body-->
                                    <div class="card-body pt-15 h-100">
                                        <!--begin::Summary-->
                                        <div class="d-flex flex-center flex-column mb-5">
                                            <!--begin::Avatar-->
                                            <div class="symbol symbol-100px symbol-circle mb-7 ">
                                                <i class="bi bi-person-fill fs-5tx"></i>
                                            </div>
                                            <a href="#"
                                                class="fs-3 text-gray-800 text-hover-primary fw-bolder mb-1">Tothiq User</a>
                                            <div class="fs-5 fw-bold text-muted mb-6">ID: 975078</div>
                                            <div class="text-success text-decoration-underline fs-4">
                                                <p> Verified</p>
                                            </div>
                                        </div>
                                        <div class="separator separator-dashed my-3"></div>
                                        <div id="kt_customer_view_details" class="collapse show">
                                            <div class="py-5 fs-6">
                                                <div class="">
                                                    <h5>Free Membership</h5>
                                                </div>
                                                <div class="fw-bolder mt-5">Creation date</div>
                                                <div class="text-gray-600">14-11-2022 13:45:30</div>

                                                <div class="fw-bolder mt-5">Verification date</div>
                                                <div class="text-gray-600"><span
                                                        class="text-success bg-light-warning fw-bolder">Verified</span>
                                                </div>

                                                <div class="fw-bolder mt-5">Status:</div>
                                                <div class="text-gray-600">Active</div>

                                                <div class="fw-bolder mt-5">Account type</div>
                                                <div class="text-gray-600">Individual</div>

                                                <div class="fw-bolder mt-5">Account Role</div>
                                                <div class="text-gray-600">Individual</div>

                                                <div class="fw-bolder mt-5">Last Activity</div>
                                                <div class="text-gray-600">24-11-2022 14:45:38</div>
                                                <!--begin::Details item-->
                                            </div>
                                        </div>
                                        <!--end::Details content-->
                                    </div>
                                    <!--end::Card body-->
                                </div>
                            </div>
                            <div class="col ms-5">
                                <div class="card">
                                    <div class="card-body pb-0 pt-3 userdasboardbox">
                                        <ul
                                            class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">
                                            <li class="nav-item ">
                                                <a class="nav-link text-active-primary pb-4 active border-active-primary border-bottom border-2"
                                                    data-bs-toggle="tab" href="#kt_tab_pane_1">Profile</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2"
                                                    data-bs-toggle="tab" href="#kt_tab_pane_2">Notification</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2"
                                                    data-kt-countup-tabs="true" data-bs-toggle="tab"
                                                    href="#kt_tab_pane_3">Membership & Contracts</a>
                                            </li>
                                        </ul>

                                        <div class=" py-3">
                                            <div class="tab-content">
                                                <div class="tab-pane fade active show" id="kt_tab_pane_1">
                                                    <div class="table-responsive">
                                                        <div class="mb-4">
                                                            <a type="reset" href="free_profile&activation.php"
                                                                class="btn btn-warning text-dark">Edit
                                                                Profile</a>
                                                        </div>
                                                        <div class="separator border-3 mb-5"></div>
                                                        <div class="mt-8">
                                                            <h4>Personal Details</h4>
                                                            <form action="">
                                                                <label class="form-label fs-6 fw-bolder text-dark">Full
                                                                    Name</label>
                                                                <input type="text"
                                                                    class="form-control form-control-solid"
                                                                    value="Tothiq User" readonly>
                                                                <div class="d-flex flex-wrap">
                                                                    <div class="fv-row p-5 col-4">
                                                                        <label
                                                                            class="form-label fs-6 fw-bolder text-dark">Phone
                                                                            number</label>
                                                                        <input
                                                                            class="form-control form-control-lg form-control-solid"
                                                                            type="email" value="+965 6845 2875"
                                                                            autocomplete="off" readonly>
                                                                    </div>
                                                                    <div class="fv-row p-5 col-5">
                                                                        <label
                                                                            class="form-label fs-6 fw-bolder text-dark">Civil
                                                                            ID Number</label>
                                                                        <input
                                                                            class="form-control form-control-lg form-control-solid"
                                                                            type="email" value="123412341234"
                                                                            autocomplete="off" readonly>
                                                                    </div>
                                                                    <div class="fv-row p-5 col-3">
                                                                        <label
                                                                            class="form-label fs-6 fw-bolder text-dark">Language</label>
                                                                        <select
                                                                            class="form-select form-select-lg form-select-solid"
                                                                            data-control="select2"
                                                                            data-placeholder="English">
                                                                            <option></option>
                                                                            <option value="1" selected>English</option>
                                                                            <option value="2">Quwati</option>
                                                                            <option value="3">France</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="fv-row p-5 col-3">
                                                                        <label
                                                                            class="form-label fs-6 fw-bolder text-dark">Address
                                                                            Type</label>
                                                                        <select
                                                                            class="form-select form-select-lg form-select-solid"
                                                                            data-control="select2"
                                                                            data-placeholder="Appartment">
                                                                            <option></option>
                                                                            <option value="1" selected>Appartment
                                                                            </option>
                                                                            <option value="2">House</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="separator border-3 my-10"></div>
                                                            <div>
                                                                <h4>User Authentication</h4>
                                                                <div id="kt_customer_details_invoices_table_1_wrapper"
                                                                    class="dataTables_wrapper dt-bootstrap4 no-footer">
                                                                    <div class="table-responsive">
                                                                        <table id="kt_customer_details_invoices_table_1"
                                                                            class="table align-start table-row-dashed fs-6 fw-bolder gy-5 dataTable no-footer">
                                                                            <tbody class="fs-6 fw-bold text-gray-600">
                                                                                <tr class="odd">
                                                                                    <td data-order="Invalid date">
                                                                                        <a href="#"
                                                                                            class="text-gray-600">Email
                                                                                            Address:</a>
                                                                                    </td>
                                                                                    <td class="text-dark fw-bolder">
                                                                                        nexaned896@turuma.com</td>
                                                                                </tr>
                                                                                <tr class="even">
                                                                                    <td data-order="Invalid date">
                                                                                        <a href="#"
                                                                                            class="text-gray-600">Password:</a>
                                                                                    </td>
                                                                                    <td class="btn btn-primary">Change
                                                                                        Password</td>
                                                                                </tr>
                                                                            </tbody>
                                                                            <!--end::Tbody-->
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="kt_tab_pane_2">
                                                    <div class="table-responsive">
                                                        <h4 class="my-5">Notifications</h4>
                                                        <p class="mb-5">Set your notification preference for when you
                                                            are in
                                                            or away from the dashboard.<br>You need to configure your
                                                            settings to allow notification from hbhhdbc.com.</p>
                                                        <label class="form-check form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="checkbox" value="" />
                                                            <span class="form-check-label">
                                                                Show notification for new messages in open chats.
                                                            </span>
                                                        </label>
                                                        <label
                                                            class="form-check form-check-custom form-check-solid my-3">
                                                            <input class="form-check-input" type="checkbox" value="" />
                                                            <span class="form-check-label">
                                                                Show notification for status changes.
                                                            </span>
                                                        </label>
                                                        <label class="form-check form-check-custom form-check-solid">
                                                            <input class="form-check-input" type="checkbox" value="" />
                                                            <span class="form-check-label">
                                                                Show notification for membership expriration.
                                                            </span>
                                                        </label>
                                                        <h5 class="mt-8 mb-5">When you receive a chat message</h5>
                                                        <div
                                                            class="form-check form-switch form-check-custom form-check-solid">
                                                            <input class="form-check-input " type="checkbox" value=""
                                                                id="flexSwitchChecked" checked="checked" />
                                                            <label class="form-check-label" for="flexSwitchChecked">
                                                                Play a sound
                                                            </label>
                                                        </div>
                                                        <div
                                                            class="form-check form-switch form-check-custom form-check-solid my-3">
                                                            <input class="form-check-input " type="checkbox" value=""
                                                                id="flexSwitchChecked" />
                                                            <label class="form-check-label" for="flexSwitchChecked">
                                                                Highlight the bell icon in the system tray
                                                            </label>
                                                        </div>
                                                        <h5 class="mt-8 mb-5">Play notification sounds through</h5>
                                                        <div class="d-flex "><i class="bi bi-volume-off-fill fa-2x"></i>
                                                            <p class="">Quodsi haberent magnalia inter <a href="#."
                                                                    class="text-primary fs-4"> Change A/V Settings</a>
                                                            </p>
                                                        </div>
                                                        <div class="d-flex "><i
                                                                class="bi bi-volume-off-fill fa-2x "></i>
                                                            <p class="">Quodsi haberent magnalia inter <a href="#."
                                                                    class="text-primary fs-4">Alert</a></p>
                                                        </div>
                                                        <a href="#." class="btn btn-primary my-3">play notification
                                                            sound</a>
                                                        <h4 class="my-3">Rmainders</h4>
                                                        <div class="d-flex ">
                                                            <div class="w-50">
                                                                <p>Quidam alii sunt, et non est in nostra
                                                                    potestate.<br>Quae omnia in nostra</p>
                                                            </div>
                                                            <div class="mb-10">
                                                                <div
                                                                    class="form-check form-switch form-check-custom form-check-solid ">
                                                                    <input class="form-check-input " type="checkbox"
                                                                        value="" id="flexSwitchChecked"
                                                                        checked="checked" />
                                                                    <label class="form-check-label"
                                                                        for="flexSwitchChecked">
                                                                        Push
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    class="form-check form-switch form-check-custom form-check-solid my-2">
                                                                    <input class="form-check-input " type="checkbox"
                                                                        value="" id="flexSwitchChecked" />
                                                                    <label class="form-check-label"
                                                                        for="flexSwitchChecked">
                                                                        Email
                                                                    </label>
                                                                </div>
                                                                <div
                                                                    class="form-check form-switch form-check-custom form-check-solid">
                                                                    <input class="form-check-input " type="checkbox"
                                                                        value="" id="flexSwitchChecked" />
                                                                    <label class="form-check-label"
                                                                        for="flexSwitchChecked">
                                                                        SMS
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="kt_tab_pane_3">
                                                    <div class="table-responsive">
                                                        <div class="d-flex justify-content-between my-7">
                                                            <div>
                                                                <p>Current Membership</p>
                                                                <h4>Free Membership</h4>
                                                            </div>
                                                            <div>
                                                                <button class="btn btn-primary btn-sm"
                                                                    data-bs-toggle="modal" type="button"
                                                                    href="#exampleModalToggle10">Upgrade
                                                                    Membership</button>
                                                            </div>
                                                        </div>
                                                        <div class="separator border-3 my-10"></div>
                                                        <div>
                                                            <h4>Contracts</h4>
                                                            <p>Contract Available - 01</p>
                                                            <p>Contract Used - 00</p>
                                                            <h4 class="my-5">Buy Additional Contract</h4>
                                                            <table
                                                                class="table table-row-dashed table-row-gray-300 gy-7">
                                                                <tr>
                                                                    <td>Number of contract</td>
                                                                    <td><input type="number"
                                                                            class="form-control form-control-solid form-control-sm" />
                                                                    </td>
                                                                    <td>X</td>
                                                                    <td>1.000 KWD</td>
                                                                    <td>=</td>
                                                                    <td>20.000 KWD</td>
                                                                </tr>
                                                            </table>
                                                            <div class="d-flex justify-content-end mb-10">
                                                                <a 
                                                                class="btn btn-sm btn-warning text-end text-dark"
                                                                data-bs-toggle="modal" type="button"
                                                                href="#exampleModalToggle7">Pay
                                                                Now</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Modal - New Card-->
                                    <!--end::Modals-->
                                </div>
                                <!--end::Container-->
                            </div>
                            <!--end::Post-->
                        </div>
                        <div class="modal fade" id="exampleModalToggle10" aria-hidden="true"
                            aria-labelledby="exampleModalToggle10" tabindex="-1" data-bs-dismiss="modal">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class=" p-20">
                                            <form action="">
                                                <tr>
                                                    <h3 class="text-primary">Upgrading Yearly Membership</h3>
                                                </tr>
                                                <div class="row mt-4 mb-7">
                                                    <div class="col-9">
                                                        <p>Individual Primum Membership</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 49.000
                                                    </div>
                                                </div>
                                                <div class="fs-6 text-danger mb-2"><a
                                                        style="background-color: rgba(255, 184, 70, 0.612);"
                                                        class="p-2 text-danger rounded-pill"> Enter Coupon Code</a>
                                                </div>
                                                <div class="row text-danger">
                                                    <div class="col-9">
                                                        <p>Ramadon Special -20%OFF</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 09.800
                                                    </div>
                                                </div>
                                                <div class="row mt-4 mb-2 border-bottom border-dark">
                                                    <div class="col-9">
                                                        <p>My Fatoorah Knet Charges</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 00.100
                                                    </div>
                                                </div>
                                                <div class="row my-5" style="border-bottom-style: dashed;">
                                                    <div class="col-9">
                                                        <p>Total</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 39.300
                                                    </div>
                                                </div>

                                                <a class="btn btn-primary mt-15 mb-5 w-100 fw-bold" type="button"
                                                    href="free_payment_my_fatoorah_upgrading.php">Submit</a>
                                            </form>



                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="exampleModalToggle7" aria-hidden="true"
                            aria-labelledby="exampleModalToggle7" tabindex="-1" data-bs-dismiss="modal">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class=" p-20">
                                            <form action="">
                                                <tr>
                                                    <h3 class="text-primary">Additional contracts</h3>
                                                </tr>
                                                <div class="row mt-4 mb-7">
                                                    <div class="col-9">
                                                        <p>1 Additional Contract</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 2.000
                                                    </div>
                                                </div>
                                                <div class="fs-6 text-danger mb-2"><a
                                                        style="background-color: rgba(255, 184, 70, 0.612);"
                                                        class="p-2 text-danger rounded-pill"> Enter Coupon Code</a>
                                                </div>
                                                <div class="row text-danger">
                                                    <div class="col-9">
                                                        <p>No Code</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 00.000
                                                    </div>
                                                </div>
                                                <div class="row mt-4 mb-2 border-bottom border-dark">
                                                    <div class="col-9">
                                                        <p>My Fatoorah Knet Charges</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 00.100
                                                    </div>
                                                </div>
                                                <div class="row my-5" style="border-bottom-style: dashed;">
                                                    <div class="col-9">
                                                        <p>Total</p>
                                                    </div>
                                                    <div class="col-3">
                                                        KWD 02.100
                                                    </div>
                                                </div>

                                                <a class="btn btn-primary mt-15 mb-5 w-100 fw-bold" type="button"
                                                    href="payment_my_fatoorah_screen2.php">Submit</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <style>
                            .nav-line-tabs .nav-item .nav-link.active,
                            .nav-line-tabs .nav-item .nav-link:hover:not(.disabled),
                            .nav-line-tabs .nav-item.show .nav-link {
                                border-bottom: unset;
                            }

                            .nav-line-tabs.nav-line-tabs-2x .nav-item .nav-link.active,
                            .nav-line-tabs.nav-line-tabs-2x .nav-item .nav-link:hover:not(.disabled),
                            .nav-line-tabs.nav-line-tabs-2x .nav-item.show .nav-link {
                                border-bottom-width: unset;
                            }

                            .btncolorblue a,
                            button.btn.btncolorblue {
                                background-color: #0a3857;
                                color: #fff;
                            }

                            .btncolorblue a:hover {
                                background-color: #ccc;
                                color: #0a3857;
                            }

                            .form-select.form-select-solid {
                                color: #a1a5b7;
                            }

                            .template_img.pb-3 img {
                                width: 100%;
                            }
                        </style>


                    </div>
                </div>
            </div>
            <div class="footer py-4 d-flex  flex-lg-column app-footer" id="kt_footer">
                <!--begin::Container-->
                <div
                    class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                            Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
            <!--end::Footer-->
        </div>

    </div>
    <!--end::Root-->



    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>


    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>