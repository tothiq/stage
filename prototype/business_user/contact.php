<?php include("header_free.php") ?>

<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Address Book</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
    </div>
    <!--end::Toolbar container-->
</div>
<div class="container-xxl pb-20" id="kt_content">
    <div class="card pe-0">
        <div class="d-flex row m-0 py-3 flex-wrap align-items-center">
            <div class="d-flex align-items-center col-7">
                <a href="#." class="mx-4 text-dark text-hover-primary">A</a>
                <a href="#." class="text-dark text-hover-primary">B</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">C</a>
                <a href="#." class="text-dark text-hover-primary">D</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">E</a>
                <a href="#." class="text-dark text-hover-primary">F</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">G</a>
                <a href="#." class="text-dark text-hover-primary">H</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">I</a>
                <a href="#." class="text-dark text-hover-primary">J</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">K</a>
                <a href="#." class="text-dark text-hover-primary">L</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">M</a>
                <a href="#." class="text-dark text-hover-primary">N</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">O</a>
                <a href="#." class="text-dark text-hover-primary">P</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">Q</a>
                <a href="#." class="text-dark text-hover-primary">R</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">S</a>
                <a href="#." class="text-dark text-hover-primary">T</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">U</a>
                <a href="#." class="text-dark text-hover-primary">V</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">W</a>
                <a href="#." class="text-dark text-hover-primary">X</a>
                <a href="#." class="mx-4 text-dark text-hover-primary">Y</a>
                <a href="#." class="text-dark text-hover-primary">Z</a>
            </div>
            <div class="col-2">
                <form data-kt-search-element="form" class="d-none d-lg-block mb-5 mb-lg-0 position-relative" autocomplete="off">
                    <span class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4 ">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="17.0365" y="13.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                            </rect>
                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor"></path>
                        </svg>
                    </span>
                    <input type="text" class="form-control form-control-solid h-40px bg-body ps-13 fs-7 w-100" name="search" value="" placeholder="Search Contact" data-kt-search-element="input">
                </form>
            </div>
            <div class="col-3 d-flex justify-content-end">
                <div>
                    <a href="#." class="btn btn-sm btn-primary d-none d-lg-block" style="margin-right: 10px;">Search</a>
                </div>
                <div>
                    <a class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_contact">Create
                        Contact</a>
                </div>
            </div>
            <!-- <i class="col-1 fas fa-filter fa-2x text-dark d-flex justify-content-center align-items-center"></i> -->
        </div>
    </div>
    <div class="post" id="kt_post">
        <div id="kt_content_container">
            <div class="d-flex flex-column  ">
                <div class="row gy-5 g-xl-8 mx-0 my-5" id="kt_content">
                    <div class="col-xl-3 mt-0 ps-0">
                        <div class="card card-xl-stretch mb-xl-8 h-100">
                            <div class="card-body p-0 mt-3">
                                <ul class="contract_tab nav flex-row flex-md-column mb-3 mb-md-0">
                                    <li class="nav-item  me-0 w-100">
                                        <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab1_all"><i class="fas fa-user-circle fa-1x me-3 "></i>Aabet
                                            Baasigaat
                                            Baazighah</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab3"><i class="fas fa-user-circle fa-1x me-3 "></i>Aabidoon
                                            Aabinus
                                            Pabel</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab3_aggrement"><i class="fas fa-user-circle fa-1x me-3 "></i>Aabdar
                                            Kaal
                                            Pabel</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab4_ba"><i class="fas fa-user-circle fa-1x me-3 "></i>Cabir
                                            Baazighah
                                            Kaal</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab5_construction"><i class="fas fa-user-circle fa-1x me-3 "></i>Cassem
                                            Aabidoon
                                            Baasigaat</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab6_consulting"><i class="fas fa-user-circle fa-1x me-3 "></i>Gadee
                                            Naadi
                                            Aabdar</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab7_contracts"><i class="fas fa-user-circle fa-1x me-3 "></i>Maaarib
                                            Maadil
                                            Aabdar</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab8_employment"><i class="fas fa-user-circle fa-1x me-3 "></i>Ma'awiya
                                            Aabinus
                                            Aabel</a>
                                    </li>
                                    <li class="nav-item  me-0">
                                        <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark" data-bs-toggle="tab" href="#tab9_financial"><i class="fas fa-user-circle fa-1x me-3 "></i>Naadi
                                            Kaal Pabel
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 px-0 mt-0">
                        <div class="tab-content" id="myTabContent_main">
                            <div class="tab-pane fade active show" id="tab1_all" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Aabel Baasiqaat Baazighah</h3>
                                            <p class="fs-6 fw-normal"><i class="fas fa-envelope-open-text me-5 text-dark"></i>aabel.baasi@gmail.com
                                            </p>
                                            <p class="fs-6 fw-normal"><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_one">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab3" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Aabidoon Aabinus Pabel</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Aabidoon.Aabinus@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_two">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab3_aggrement" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Aabdar Kaal Pabel</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Aabdar.Pabel@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_three">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab4_ba" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Cabir Baazighah Kaal</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Cabir.Kaal@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_four">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab5_construction" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Cassem Aabidoon Baasigaat</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Cassem.Baasigaat@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_five">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab6_consulting" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Gadee Naadi Aabdar</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Gadee.Aabdar@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_six">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab7_contracts" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Maaarib Maadil Aabdar</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Maaarib.Aabdar@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_seven">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="tab8_employment" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Ma'awiya Aabinus Aabel</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Ma'awiya.Aabel@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_eight">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab9_financial" role="tabpanel">
                                <div class="templates_boxsec d-flex flex-wrap card">
                                    <div class="row mx-0 mb-3 align-items-center">
                                        <div class="col-4">
                                            <h2 class="text-primary">Contact Details</h2>
                                        </div>
                                        <div class="col-8 d-flex justify-content-end">
                                            <a href="#." class="btn btn-sm btn-primary d-none d-lg-block">Report a user</a>
                                        </div>
                                    </div>
                                    <div class="d-flex pb-5">
                                        <div class="mx-9">
                                            <div class=""><i class="fas fa-user-circle p-10 fa-2x  border border-2 border-secondary"></i>
                                            </div>
                                        </div>
                                        <div class="">
                                            <h3 class="mb-5">Naadi Kaal Pabel</h3>
                                            <p><i class="fas fa-envelope-open-text me-5 text-dark"></i>Naadi.Pabel@gmail.com
                                            </p>
                                            <p><i class="fas fa-mobile-alt me-5 text-dark"></i>+965-1234567
                                            </p>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6 mx-10">
                                        <li class="nav-item  me-0 ">
                                            <a class="fs-6 fw-bold nav-link active text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#General">General</a>
                                        </li>
                                        <li class="nav-item  me-0">
                                            <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary  border-2 text-dark" data-bs-toggle="tab" href="#Contract">Contract</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content" id="myTabContent_nine">
                                    <div class="tab-pane fade active show" id="General" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card">
                                            <div class="mx-15 my-10">
                                                <h5 class="pb-0">Civil Id Number</h5>
                                                <p class="fs-6 fw-normal pb-7">*******
                                                </p>
                                                <h5 class="pb-0">Nationality</h5>
                                                <p class="fs-6 fw-normal pb-7">Indian</p>
                                                <h5 class="pb-0">Gender</h5>
                                                <p class="fs-6 fw-normal pb-7">Male</p>
                                                <h5 class="pb-0">Date of Birth</h5>
                                                <p class="fs-6 fw-normal">*******</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Contract" role="tabpanel">
                                        <div class="templates_boxsec d-flex flex-wrap card ">
                                            <div class="mx-15 my-10">
                                                Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Officia earum quasi neque, commodi modi eum
                                                dolorem iste sapiente optio provident pariatur rem
                                                non! Tempore nobis a eligendi soluta unde
                                                repudiandae.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="create_contact" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered mw-900px">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Create Contact</h2>
                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body py-lg-10 px-lg-10">

                <div class="popup_select_folder_name mb-10">
                    <h3>Enter Civil ID</h3>
                    <input type="text" class="form-control" placeholder="Civil ID" />
                </div>
                <div class="popup_create_folder_btn d-flex justify-content-end">
                    <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_contract">Invite Contact</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Root-->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_free.php") ?>