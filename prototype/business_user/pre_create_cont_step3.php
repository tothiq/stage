<?php include("header_premium.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Create Contract</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
    </div>
    <!--end::Toolbar container-->
</div>
<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 align-items-center">
                <div class="create_contract_step">
                    <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
                        <li class="nav-item">
                            <a class="nav-link">Step-1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ">Step-2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active border-bottom-2 border-primary">Step-3</a>
                        </li>
                    </ul>
                </div>
                <div class="create_contract_btn d-flex align-items-center mb-5">
                    <div class="form-check form-check-solid form-switch form-check-custom fv-row" style="margin-right: 10px;">
                        <label class="form-check-label fs-6" for="allowmarketing" style="color: #3f4254;font-weight: 500;">Edit : </label>
                        <i class="fa-solid fa-check fa-xl" style="color:#183052;margin-left:5px;"></i>
                    </div>
                    <a href="pre_new&draft.php" class="btn btn-sm btn-primary" style="margin-right: 10px;">Continue</a>
                    <a href="pre_create_cont_step2.php" class="btn btn-sm btn-outline btn-outline-primary btn-active-light-primary">Cancel</a>
                </div>
            </div>
            <div class="tab-content" id="myTabContent">
                <!--begin:::Tab pane-->
                <div class="tab-pane fade active show" id="all_contrcats" role="tabpanel">
                    <form id="kt_modal_new_target_form" class="form mb-10" action="#">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-13">
                                    <h1 class="mb-3">First Parties</h1>
                                </div>
                                <div class="first_party_heading d-flex mb-5">
                                    <h3 class="fs-6 me-20">Company Name</h3>
                                    <h3 class="fs-6">Company Address</h3>
                                </div>
                                <div class="firstparty_personname mt-5 d-flex">
                                    <div class="fv-row col-11">
                                        <label class="form-label fs-6 fw-bolder text-dark">Authorized Person To Sign</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">Aaabirah Aadab Aadil</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="fv-row p-0 col-1 d-flex align-items-end justify-content-center">
                                        <a href="#." class="btn btn-sm btn-primary text-white m-2"><i class="fas fa-plus fa-lg p-0"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mt-5">
                            <div class="card-body">
                                <div class="mb-13">
                                    <h1 class="mb-3">Add Additional First Parties</h1>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Select Party Type</label>
                                        <select name="form-select" id="select-party-type" class="form-select">
                                            <option value="">Select a Option</option>
                                            <option value="option_company">Company</option>
                                            <option value="option_individual">Individual</option>
                                        </select>
                                        <div class="opt_inr">
                                            <div class="fv-row col-6" data-attribute="option_company">
                                                <label class="form-label fs-6 fw-bolder text-dark">Company Name</label>
                                                <select class="form-select" aria-label="Select example" placeholder="">
                                                    <option value="1">Mic</option>
                                                    <option value="2">Alsalam Hospital</option>
                                                    <option value="3">Invite New Party</option>
                                                </select>
                                            </div>
                                            <div class="fv-row col-6" data-attribute="option_company">
                                                <label class="form-label fs-6 fw-bolder text-dark">Email Address</label>
                                                <select class="form-select" aria-label="Select example" placeholder="">
                                                    <option value="1">username@mail.com</option>
                                                    <option value="2">Tothiq User</option>
                                                    <option value="3">Invite New Party</option>
                                                </select>
                                            </div>
                                            <div class="fv-row p-5 col-3 ps-0" data-attribute="option_individual">
                                                <label class="form-label fs-6 fw-bolder text-dark">Company Name</label>
                                                <select class="form-select" aria-label="Select example" placeholder="">
                                                    <option value="1">Mic</option>
                                                    <option value="2">Alsalam Hospital</option>
                                                    <option value="3">Invite New Party</option>
                                                </select>
                                            </div>
                                            <div class="fv-row p-5 col-3" data-attribute="option_individual">
                                                <label class="form-label fs-6 fw-bolder text-dark">Email Address</label>
                                                <select class="form-select" aria-label="Select example" placeholder="">
                                                    <option value="1">username@mail.com</option>
                                                    <option value="2">Tothiq User</option>
                                                    <option value="3">Invite New Party</option>
                                                </select>
                                            </div>
                                            <div class="fv-row p-5 col-3" data-attribute="option_individual">
                                                <label class="form-label fs-6 fw-bolder text-dark">Email Address</label>
                                                <select class="form-select" aria-label="Select example" placeholder="">
                                                    <option value="1">username@mail.com</option>
                                                    <option value="2">Tothiq User</option>
                                                    <option value="3">Invite New Party</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div> -->
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 col-4 ps-0">
                                        <label class="form-label fs-6 fw-bolder text-dark">Select Party Type</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">Company</option>
                                            <option value="2">Individual</option>
                                        </select>
                                    </div>
                                    <div class="fv-row p-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Company Name</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">Mic</option>
                                            <option value="2">Alsalam Hospital</option>
                                            <option value="3">Invite New Party</option>
                                        </select>
                                    </div>
                                    <div class="fv-row p-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Company Email</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">username@mail.com</option>
                                            <option value="2">Tothiq User</option>
                                            <option value="3">Invite New Party</option>
                                        </select>
                                    </div>
                                    <!-- <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Full Name
                                        </label>
                                        <input type="text" class="form-control form-control-white" placeholder="Tothiq User">
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="card mt-5">
                            <div class="card-body">
                                <div class="mb-13">
                                    <h1 class="mb-3">Second Parties</h1>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 col-4 ps-0">
                                        <label class="form-label fs-6 fw-bolder text-dark">Select Party Type</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">Company</option>
                                            <option value="2">Individual</option>
                                        </select>
                                    </div>
                                    <div class="fv-row p-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Company Name</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">Mic</option>
                                            <option value="2">Alsalam Hospital</option>
                                            <option value="3">Invite New Party</option>
                                        </select>
                                    </div>
                                    <div class="fv-row p-5 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Company Email</label>
                                        <select class="form-select" aria-label="Select example" placeholder="">
                                            <option value="1">username@mail.com</option>
                                            <option value="2">Tothiq User</option>
                                            <option value="3">Invite New Party</option>
                                        </select>
                                    </div>
                                    <!-- <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Full Name
                                        </label>
                                        <input type="text" class="form-control form-control-white" placeholder="Tothiq User">
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="regis_lower_box border border-primary my-20 p-10">
                        <h2 style="color: rgb(255, 115, 0);">To add more parties in the contract, please contact company administrator</h2>
                        <div class="d-flex pt-3 justify-content-between">
                            <p class="">Quod Enchiridion Epictetus stoici scripsit. Rodrigo Abela of Technologiae apud Massachusetts instituta Opera collectio. <br>Ex anglicus latine translata sunt.</p>
                        </div>
                    </div>
                </div>
                <!--end:::Tab pane-->
            </div>
        </div>
    </div>
</div>


<!-- Modal Invite Contact  -->
<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100">
                    <div class=" d-flex flex-wrap mb-20">
                        <div class="fv-row p-5 col-12">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Email Address" autocomplete="off" />
                        </div>
                    </div>
                    <div class="text-center btncolorblue pt-10">
                        <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Invite Contact  -->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_premium.php") ?>