<?php include("header_premium.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Home</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
    </div>
    <!--end::Toolbar container-->
</div>
<!--begin::Content-->
<div class="content d-flex flex-column flex-column-fluid pb-20 pt-0" id="kt_content">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Row First Section-->
            <div class="row g-5 mx-0 g-xxl-8">
                <!--begin::Col-->
                <div class="col-xl-6 ps-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading">
                                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Contracts Summary</h1>
                            </div>
                        </div>
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">8140</div>
                                        <div class="fw-bold text-dark-100">Used</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">140</div>
                                        <div class="fw-bold text-dark-100">Draft</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0 ">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">1340</div>
                                        <div class="fw-bold text-dark-100">Under-Review</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">2233</div>
                                        <div class="fw-bold text-dark-100">Waiting for Sign</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">4312</div>
                                        <div class="fw-bold text-dark-100">Ready</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">108</div>
                                        <div class="fw-bold text-dark-100">Delete</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">05</div>
                                        <div class="fw-bold text-dark-100">Canceled</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-3">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-0 ">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-2 mb-2">02</div>
                                        <div class="fw-bold text-dark-100">Rejected</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <!-- <div class="block-1_heading">
                                            <h3 class="fs-5">Contract's Frequency</h3>
                                        </div> 
                                        <div id="apexchartsmf4sqg4y"
                                            class="apexcharts-canvas apexchartsmf4sqg4y apexcharts-theme-light"
                                            style="height: 150px;"><svg id="SvgjsSvg1533" height="150"
                                                xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                xmlns:svgjs="http://svgjs.dev" class="apexcharts-svg"
                                                xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                                style="background: transparent;">
                                                <g id="SvgjsG1535" class="apexcharts-inner apexcharts-graphical"
                                                    transform="translate(0, 0)">
                                                    <defs id="SvgjsDefs1534">
                                                        <clipPath id="gridRectMaskmf4sqg4y">
                                                            <rect id="SvgjsRect1538" width="410" height="153" x="-3.5"
                                                                y="-1.5" rx="0" ry="0" opacity="1" stroke-width="0"
                                                                stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                                        </clipPath>
                                                        <clipPath id="forecastMaskmf4sqg4y"></clipPath>
                                                        <clipPath id="nonForecastMaskmf4sqg4y"></clipPath>
                                                        <clipPath id="gridRectMarkerMaskmf4sqg4y">
                                                            <rect id="SvgjsRect1539" width="407" height="154" x="-2"
                                                                y="-2" rx="0" ry="0" opacity="1" stroke-width="0"
                                                                stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                                        </clipPath>
                                                    </defs>
                                                    <g id="SvgjsG1546" class="apexcharts-xaxis"
                                                        transform="translate(0, 0)">
                                                        <g id="SvgjsG1547" class="apexcharts-xaxis-texts-g"
                                                            transform="translate(0, -4)"></g>
                                                    </g>
                                                    <g id="SvgjsG1556" class="apexcharts-grid">
                                                        <g id="SvgjsG1557" class="apexcharts-gridlines-horizontal"
                                                            style="display: none;">
                                                            <line id="SvgjsLine1559" x1="0" y1="0" x2="403" y2="0"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1560" x1="0" y1="15" x2="403" y2="15"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1561" x1="0" y1="30" x2="403" y2="30"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1562" x1="0" y1="45" x2="403" y2="45"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1563" x1="0" y1="60" x2="403" y2="60"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1564" x1="0" y1="75" x2="403" y2="75"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1565" x1="0" y1="90" x2="403" y2="90"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1566" x1="0" y1="105" x2="403" y2="105"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1567" x1="0" y1="120" x2="403" y2="120"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1568" x1="0" y1="135" x2="403" y2="135"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                            <line id="SvgjsLine1569" x1="0" y1="150" x2="403" y2="150"
                                                                stroke="#e0e0e0" stroke-dasharray="0"
                                                                stroke-linecap="butt" class="apexcharts-gridline">
                                                            </line>
                                                        </g>
                                                        <g id="SvgjsG1558" class="apexcharts-gridlines-vertical"
                                                            style="display: none;"></g>
                                                        <line id="SvgjsLine1571" x1="0" y1="150" x2="403" y2="150"
                                                            stroke="transparent" stroke-dasharray="0"
                                                            stroke-linecap="butt"></line>
                                                        <line id="SvgjsLine1570" x1="0" y1="1" x2="0" y2="150"
                                                            stroke="transparent" stroke-dasharray="0"
                                                            stroke-linecap="butt"></line>
                                                    </g>
                                                    <g id="SvgjsG1540"
                                                        class="apexcharts-area-series apexcharts-plot-series">
                                                        <g id="SvgjsG1541" class="apexcharts-series"
                                                            seriesname="NetxProfit" data:longestseries="true" rel="1"
                                                            data:realindex="0">
                                                            <path id="SvgjsPath1544"
                                                                d="M 0 150L 0 50C 23.508333333333333 50 43.65833333333334 50 67.16666666666667 50C 90.67500000000001 50 110.82500000000002 75 134.33333333333334 75C 157.84166666666667 75 177.99166666666667 75 201.5 75C 225.00833333333333 75 245.15833333333336 62.5 268.6666666666667 62.5C 292.175 62.5 312.325 62.5 335.8333333333333 62.5C 359.34166666666664 62.5 379.4916666666667 25 403 25C 403 25 403 25 403 150M 403 25z"
                                                                fill="rgba(0,158,247,0.3)" fill-opacity="1"
                                                                stroke-opacity="1" stroke-linecap="butt"
                                                                stroke-width="0" stroke-dasharray="0"
                                                                class="apexcharts-area" index="0"
                                                                clip-path="url(#gridRectMaskmf4sqg4y)"
                                                                pathto="M 0 150L 0 50C 23.508333333333333 50 43.65833333333334 50 67.16666666666667 50C 90.67500000000001 50 110.82500000000002 75 134.33333333333334 75C 157.84166666666667 75 177.99166666666667 75 201.5 75C 225.00833333333333 75 245.15833333333336 62.5 268.6666666666667 62.5C 292.175 62.5 312.325 62.5 335.8333333333333 62.5C 359.34166666666664 62.5 379.4916666666667 25 403 25C 403 25 403 25 403 150M 403 25z"
                                                                pathfrom="M -1 150L -1 150L 67.16666666666667 150L 134.33333333333334 150L 201.5 150L 268.6666666666667 150L 335.8333333333333 150L 403 150">
                                                            </path>
                                                            <path id="SvgjsPath1545"
                                                                d="M 0 50C 23.508333333333333 50 43.65833333333334 50 67.16666666666667 50C 90.67500000000001 50 110.82500000000002 75 134.33333333333334 75C 157.84166666666667 75 177.99166666666667 75 201.5 75C 225.00833333333333 75 245.15833333333336 62.5 268.6666666666667 62.5C 292.175 62.5 312.325 62.5 335.8333333333333 62.5C 359.34166666666664 62.5 379.4916666666667 25 403 25"
                                                                fill="none" fill-opacity="1" stroke="#009ef7"
                                                                stroke-opacity="1" stroke-linecap="butt"
                                                                stroke-width="3" stroke-dasharray="0"
                                                                class="apexcharts-area" index="0"
                                                                clip-path="url(#gridRectMaskmf4sqg4y)"
                                                                pathto="M 0 50C 23.508333333333333 50 43.65833333333334 50 67.16666666666667 50C 90.67500000000001 50 110.82500000000002 75 134.33333333333334 75C 157.84166666666667 75 177.99166666666667 75 201.5 75C 225.00833333333333 75 245.15833333333336 62.5 268.6666666666667 62.5C 292.175 62.5 312.325 62.5 335.8333333333333 62.5C 359.34166666666664 62.5 379.4916666666667 25 403 25"
                                                                pathfrom="M -1 150L -1 150L 67.16666666666667 150L 134.33333333333334 150L 201.5 150L 268.6666666666667 150L 335.8333333333333 150L 403 150">
                                                            </path>
                                                            <g id="SvgjsG1542" class="apexcharts-series-markers-wrap"
                                                                data:realindex="0">
                                                                <g class="apexcharts-series-markers">
                                                                    <circle id="SvgjsCircle1577" r="0" cx="0" cy="50"
                                                                        class="apexcharts-marker wox7v1zitk no-pointer-events"
                                                                        stroke="#f1faff" fill="#009ef7" fill-opacity="1"
                                                                        stroke-width="3" stroke-opacity="0.9"
                                                                        default-marker-size="0"></circle>
                                                                </g>
                                                            </g>
                                                        </g>
                                                        <g id="SvgjsG1543" class="apexcharts-datalabels"
                                                            data:realindex="0"></g>
                                                    </g>
                                                    <line id="SvgjsLine1572" x1="0" y1="0" x2="403" y2="0"
                                                        stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                        stroke-linecap="butt" class="apexcharts-ycrosshairs"></line>
                                                    <line id="SvgjsLine1573" x1="0" y1="0" x2="403" y2="0"
                                                        stroke-dasharray="0" stroke-width="0" stroke-linecap="butt"
                                                        class="apexcharts-ycrosshairs-hidden"></line>
                                                    <g id="SvgjsG1574" class="apexcharts-yaxis-annotations"></g>
                                                    <g id="SvgjsG1575" class="apexcharts-xaxis-annotations"></g>
                                                    <g id="SvgjsG1576" class="apexcharts-point-annotations"></g>
                                                </g>
                                                <g id="SvgjsG1555" class="apexcharts-yaxis" rel="0"
                                                    transform="translate(-18, 0)"></g>
                                                <g id="SvgjsG1536" class="apexcharts-annotations"></g>
                                            </svg>
                                            <div class="apexcharts-legend" style="max-height: 75px;"></div>
                                            <div class="apexcharts-tooltip apexcharts-theme-light"
                                                style="left: 11px; top: 53px;">
                                                <div class="apexcharts-tooltip-title"
                                                    style="font-family: inherit; font-size: 12px;">Feb</div>
                                                <div class="apexcharts-tooltip-series-group apexcharts-active"
                                                    style="order: 1; display: flex;"><span
                                                        class="apexcharts-tooltip-marker"
                                                        style="background-color: rgb(0, 158, 247);"></span>
                                                    <div class="apexcharts-tooltip-text"
                                                        style="font-family: inherit; font-size: 12px;">
                                                        <div class="apexcharts-tooltip-y-group"><span
                                                                class="apexcharts-tooltip-text-y-label">Net Profit:
                                                            </span><span class="apexcharts-tooltip-text-y-value">$40
                                                                thousands</span></div>
                                                        <div class="apexcharts-tooltip-goals-group"><span
                                                                class="apexcharts-tooltip-text-goals-label"></span><span
                                                                class="apexcharts-tooltip-text-goals-value"></span>
                                                        </div>
                                                        <div class="apexcharts-tooltip-z-group"><span
                                                                class="apexcharts-tooltip-text-z-label"></span><span
                                                                class="apexcharts-tooltip-text-z-value"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-light"
                                                style="left: -21.9688px; top: 152px;">
                                                <div class="apexcharts-xaxistooltip-text"
                                                    style="font-family: inherit; font-size: 12px; min-width: 23.9453px;">
                                                    Feb</div>
                                            </div>
                                            <div
                                                class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light">
                                                <div class="apexcharts-yaxistooltip-text"></div>
                                            </div>
                                        </div>->
                                    
                                    <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Charts Widget 1-->
                </div>
                <!--end::Col-->


                <!--begin::Col-->
                <div class="col-xl-6 pe-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading">
                                <h3 class="fs-3 fw-bold">Contracts Status</h3>
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2">11000
                                        </div>
                                        <div class="fw-bold text-dark-100">Total</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2">8140
                                        </div>
                                        <div class="fw-bold text-dark-100">Used</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-4">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_contract.php" class="card bg-secondary bg-opacity-25 mb-xl-8">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2">2860
                                        </div>
                                        <div class="fw-bold text-dark-100">Available
                                        </div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="d-flex flex-column w-100 mb-7">
                                <div class="d-flex flex-stack mb-2 justify-content-between">
                                    <h6>Usage Percentage</h6>
                                    <span class="text-muted fs-7 fw-bold" style="font-size: 1.5rem !important; color: #3f4254 !important;">50%</span>
                                </div>
                                <div class="progress h-6px w-100">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--<div id="kt_mixed_widget_14_chart" class="mt-0"
                                                    style="height: 200px; min-height: 178.7px;">
                                                    <div id="apexchartsewz89b5j"
                                                        class="apexcharts-canvas apexchartsewz89b5j apexcharts-theme-light d-flex justify-content-center">
                                                        <svg id="SvgjsSvg1350" width="254" height="178.7"
                                                            xmlns="http://www.w3.org/2000/svg" version="1.1"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                                            xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                                            xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                                            style="background: transparent;">
                                                            <g id="SvgjsG1352"
                                                                class="apexcharts-inner apexcharts-graphical"
                                                                transform="translate(40, 0)">
                                                                <defs id="SvgjsDefs1351">
                                                                    <clipPath id="gridRectMaskewz89b5j">
                                                                        <rect id="SvgjsRect1354" width="182"
                                                                            height="200" x="-3" y="-1" rx="0" ry="0"
                                                                            opacity="1" stroke-width="0" stroke="none"
                                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                    <clipPath id="gridRectMarkerMaskewz89b5j">
                                                                        <rect id="SvgjsRect1355" width="180"
                                                                            height="202" x="-2" y="-2" rx="0" ry="0"
                                                                            opacity="1" stroke-width="0" stroke="none"
                                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                                    </clipPath>
                                                                </defs>
                                                                <g id="SvgjsG1356" class="apexcharts-radialbar">
                                                                    <g id="SvgjsG1357">
                                                                        <g id="SvgjsG1358" class="apexcharts-tracks">
                                                                            <g id="SvgjsG1359"
                                                                                class="apexcharts-radialbar-track apexcharts-track"
                                                                                rel="1">
                                                                                <path id="apexcharts-radialbarTrack-0"
                                                                                    d="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 87.98928506193984 26.607927764323023"
                                                                                    fill="none" fill-opacity="1"
                                                                                    stroke="rgba(201,247,245,0.85)"
                                                                                    stroke-opacity="1"
                                                                                    stroke-linecap="round"
                                                                                    stroke-width="8.97439024390244"
                                                                                    stroke-dasharray="0"
                                                                                    class="apexcharts-radialbar-area"
                                                                                    data:pathorig="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 87.98928506193984 26.607927764323023">
                                                                                </path>
                                                                            </g>
                                                                        </g>
                                                                        <g id="SvgjsG1361">
                                                                            <g id="SvgjsG1365"
                                                                                class="apexcharts-series apexcharts-radial-series"
                                                                                seriesname="Progress" rel="1"
                                                                                data:realindex="0">
                                                                                <path id="SvgjsPath1366"
                                                                                    d="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 26.757474833957374 92.28249454023158"
                                                                                    fill="none" fill-opacity="0.85"
                                                                                    stroke="rgba(27,197,189,0.85)"
                                                                                    stroke-opacity="1"
                                                                                    stroke-linecap="round"
                                                                                    stroke-width="8.97439024390244"
                                                                                    stroke-dasharray="0"
                                                                                    class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                                    data:angle="266" data:value="74"
                                                                                    index="0" j="0"
                                                                                    data:pathorig="M 88 26.60792682926828 A 61.39207317073172 61.39207317073172 0 1 1 26.757474833957374 92.28249454023158"
                                                                                    selected="false"></path>
                                                                            </g>
                                                                            <circle id="SvgjsCircle1362"
                                                                                r="56.9048780487805" cx="88" cy="88"
                                                                                class="apexcharts-radialbar-hollow"
                                                                                fill="transparent"></circle>
                                                                            <g id="SvgjsG1363"
                                                                                class="apexcharts-datalabels-group"
                                                                                transform="translate(0, 0) scale(1)"
                                                                                style="opacity: 1;"><text
                                                                                    id="SvgjsText1364"
                                                                                    font-family="Helvetica, Arial, sans-serif"
                                                                                    x="88" y="100" text-anchor="middle"
                                                                                    dominant-baseline="auto"
                                                                                    font-size="30px" font-weight="700"
                                                                                    fill="#5e6278"
                                                                                    class="apexcharts-text apexcharts-datalabel-value"
                                                                                    style="font-family: Helvetica, Arial, sans-serif;">74%</text>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                                <line id="SvgjsLine1367" x1="0" y1="0" x2="176" y2="0"
                                                                    stroke="#b6b6b6" stroke-dasharray="0"
                                                                    stroke-width="1" class="apexcharts-ycrosshairs">
                                                                </line>
                                                                <line id="SvgjsLine1368" x1="0" y1="0" x2="176" y2="0"
                                                                    stroke-dasharray="0" stroke-width="0"
                                                                    class="apexcharts-ycrosshairs-hidden"></line>
                                                            </g>
                                                            <g id="SvgjsG1353" class="apexcharts-annotations"></g>
                                                        </svg>
                                                        <div class="apexcharts-legend"></div>
                                                    </div>
                                                </div>-->
                        </div>
                    </div>
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row First Section-->

            <!--begin::Row Second Section-->
            <div class="row g-5 mx-0 g-xxl-8" style="margin-top: 0px;">
                <!--begin::Col Categorywise-->
                <div class="col-xl-5 ps-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="card-header align-items-center border-0 payment_sum mt-0 px-4">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Payment
                                        Summary</span>
                                </h3>
                                <div class="d-flex align-items-center">
                                    <input type="date" class="form-control form-control-sm form-control-solid me-3">
                                    <a href="#.">
                                        <i class="fas fa-filter fs-4 text-hover-muted" style="color:#0b3856;"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- <div class="col-xl-12" style="margin-top: 0px; margin-bottom: -20px !important;">
                                                    <a href="premium_pre_contract.php"
                                                        class="card bg-secondary bg-opacity-25">
                                                        <div class="card-body p-3">
                                                            <div class="text-dark-100 fw-bolder fs-2 mb-2">Total Users</div>
                                                            <div class="fw-bold text-dark-100">KWD 8140.000</div>
                                                        </div>
                                                    </a>
                                                </div> -->
                            <div class="col-xl-12 mb-2 mt-0">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Total</div>
                                        <div class="fs-5">KWD 15680.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Membership - Basic</div>
                                        <div class="fs-5">KWD 4680.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>
                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0 mb-2">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Contracts</div>
                                        <div class="fs-5">KWD 2860.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>
                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12 mt-0">
                                <!--begin::Statistics Widget 5-->
                                <a href="#." class="card bg-secondary bg-opacity-25">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-5 mb-2">Users</div>
                                        <div class="fs-5">KWD 8140.000</div>
                                    </div>
                                    <!--end::Body-->
                                </a>
                                <!--end::Statistics Widget 5-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Col Categorywise-->


                <!--begin::Col Notifications-->
                <div class="col-xl-7 pe-0">
                    <div class="card mb-5 mb-xxl-8 h-100">
                        <div class="row g-5 g-xxl-8 m-0">
                            <div class="block-1_heading d-flex justify-content-between">
                                <h3 class="fs-3 fw-bold" style="margin-bottom: 20px;">Notifications</h3>
                                <a href="pre_notification.php">View All</a>
                            </div>
                            <div class="col-xl-12" style="margin-top: 0px; margin-bottom: -20px !important;">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_notification.php" class="card bg-secondary bg-opacity-25 mb-xl-8" style="background-color: #f1f9ff !important;">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2" style="font-size: 16px !important;"><i class="far fa-bell fa-3x text-primary" style="font-size: 16px;margin-top: 10px;margin-left: 10px;color: #2e9ef7 !important;margin-right: 10px;"></i>Your
                                            membership plan expired in 2 days.</div>
                                        <div class="fw-bold text-dark-100" style="font-size: 12px !important;margin-left: 35px;">
                                            Review now and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12" style="margin-top: 0px; margin-bottom: -20px !important;">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_notification.php" class="card bg-secondary bg-opacity-25 mb-xl-8" style="background-color: #f1f9ff !important;">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2" style="font-size: 16px !important;"><i class="far fa-bell fa-3x text-primary" style="font-size: 16px;margin-top: 10px;margin-left: 10px;color: #2e9ef7 !important;margin-right: 10px;"></i>Your
                                            membership plan expired in 2 days.</div>
                                        <div class="fw-bold text-dark-100" style="font-size: 12px !important;margin-left: 35px;">
                                            Review now and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                            <div class="col-xl-12" style="margin-top: 0px; margin-bottom: -20px !important;">
                                <!--begin::Statistics Widget 5-->
                                <a href="pre_notification.php" class="card bg-secondary bg-opacity-25 mb-xl-8" style="background-color: #f1f9ff !important;">
                                    <!--begin::Body-->
                                    <div class="card-body p-3">
                                        <div class="text-dark-100 fw-bolder fs-3 mb-2" style="font-size: 16px !important;"><i class="far fa-bell fa-3x text-primary" style="font-size: 16px;margin-top: 10px;margin-left: 10px;color: #2e9ef7 !important;margin-right: 10px;"></i>Your
                                            membership plan expired in 2 days.</div>
                                        <div class="fw-bold text-dark-100" style="font-size: 12px !important;margin-left: 35px;">
                                            Review now and get 15% off on membership.</div>
                                    </div>
                                    <!--end::Body-->
                                </a>

                                <!--end::Statistics Widget 5-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Col Notifications-->
            </div>
            <!--end::Row Second Section-->









        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
<!--end::Content-->



<!-- Start :: script Area-->
<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");

    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_premium.php") ?>