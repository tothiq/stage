<html lang="en">

<head>
    <title>Payment_screen</title>
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/logo(1).png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bu_style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-700px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <form class="form w-650" novalidate="novalidate" id="kt_sign_in_form" action="#">
                        <div class="p-5" style="background-color:#ffffff;  max-width: 600px;">
                            <div class="form-check bg-secondary py-3 px-12">
                                <input class="form-check-input" type="radio" value="" id="flexRadioChecked"
                                    checked="checked" name="radiobutton" />
                                <label class="form-check-label" for="flexRadioDefault1">
                                    My Fatoorah
                                </label>
                            </div>
                            <div class="m-7 mb-15px ">
                                <h6>Checkout with MyFatoorah payment gateway</h6>
                                <div class="innerbox p-15 my-10 shadow-sm">
                                    <h4>How would you like to pay?</h4>
                                    <h3 class="text-center my-8 text-gray-600">PAY WITH</h3>
                                    <a href="#.">
                                        <div
                                            class="row border-2 justify-content-between border align-middle h-40px pt-2">
                                            <div class="col-4 d-flex ">
                                                <span><i class="far fa-credit-card fa-2x"></i></span>
                                                <p class="ps-2  pt-1">KNET</p>
                                            </div>
                                            <div class="col-3 text-end  pt-1">
                                                24.4 KWD
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#.">
                                        <div
                                            class="row border-2 justify-content-between border align-middle h-40px pt-2 my-5">
                                            <div class="col-4 d-flex">
                                                <span><i class="fab fa-cc-apple-pay fa-2x"></i></span>
                                                <p class="ps-2 pt-1">Apple Pay</p>
                                            </div>
                                            <div class="col-3 text-end  pt-1">
                                                24.4 KWD
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#.">
                                        <div
                                            class="row border-2 justify-content-between border align-middle h-40px pt-2">
                                            <div class="col-4 d-flex">
                                                <span><i class="far fa-credit-card fa-2x"></i></span>
                                                <p class="ps-2  pt-1">Benifit</p>
                                            </div>
                                            <div class="col-4 text-end  pt-1">
                                                55.71 BHD
                                            </div>
                                        </div>
                                    </a>
                                    <h3 class="text-center my-8 text-gray-600">Or Insert Card Details</h3>
                                    <input type="text" placeholder="Name on Card" class="form-control w-100 p-3">
                                    <div class="input-group d-flex"><input type="text" placeholder="Number"
                                            class="form-control w-90 h-100 p-3">
                                        <span class="input-group-text"><i class="fab fa-cc-visa fa-lg me-2"></i><i
                                                class="fab fa-cc-mastercard fa-lg"></i></span>
                                    </div>
                                    <div class="d-flex">
                                        <input type="text" placeholder="MM/YY" class="form-control w-50 h-100 p-3">
                                        <input type="text" placeholder="CVV" class=" form-control w-50 h-100 p-3">
                                    </div>
                                    <a class="btn btn-primary mt-8 w-100 fw-bold" data-bs-toggle="modal"
                                        data-bs-target="#exampleModalToggle4">Pay Now</a>
                                </div>
                            </div>
                            <div class="form-check bg-secondary py-3 px-12">
                                <input class="form-check-input" type="radio" value="" id="flexRadioChecked"
                                    name="radiobutton">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    Cash on Delivery
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
        tabindex="-1" >
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body" data-bs-target="#exampleModalToggle5" data-bs-toggle="modal">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <img class="border border-2" src="./images/verifypayment.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalToggle5" aria-hidden="true" aria-labelledby="exampleModalToggleLabel5"
        tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body" data-bs-target="#exampleModalToggle6" data-bs-toggle="modal">
                    <div class="align-center w-100 text-center p-3">
                        <h3>Thank you for upgrading your account.</h3>
                        <p class="my-3">Check your email for a confirmation of the payment, please Please click the link below to take advantage of your individual premium membership benefits.</p>
                        <a href="basic_profile_screen_active_buy_contract.php" class="btn btn-primary align-center">Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>var hostUrl = "../assets/";</script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>