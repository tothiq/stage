<?php include("header_premium.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                Profile</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
    </div>
    <!--end::Toolbar container-->
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="d-flex flex-column flex-xl-row">
            <div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10 ">
                <div class="card mb-5 mb-xl-8">
                    <!--begin::Card body-->
                    <div class="card-body pt-15 h-100">
                        <!--begin::Summary-->
                        <div class="d-flex flex-center flex-column mb-5">
                            <!--begin::Avatar-->
                            <div class="symbol symbol-100px symbol-circle mb-7 ">
                                <i class="bi bi-person-fill fs-5tx"></i>
                            </div>
                            <a href="#" class="fs-3 text-gray-800 text-hover-primary fw-bolder mb-1">Tothiq User</a>
                            <div class="fs-5 fw-bold text-muted mb-1">ID: 975078</div>
                            <div class="">
                                <button class="btn btn-primary btn-sm">Premium Membership</button>
                            </div>
                            <!-- <div class="text-success text-decoration-underline fs-4">
                                        <p> Verified</p>
                                    </div> -->
                        </div>
                        <div class="separator separator-dashed my-3"></div>
                        <div id="kt_customer_view_details" class="collapse show">
                            <div class="pb-5 fs-6">
                                <div class="fw-bolder mt-5">Creation date</div>
                                <div class="text-gray-600">14-11-2022 13:45:30 - <span class="text-danger">Not Verified</span></div>

                                <div class="fw-bolder mt-5">Status:</div>
                                <div class="text-gray-600">Not Active</div>

                                <div class="fw-bolder mt-5">Account type</div>
                                <div class="text-gray-600">Business</div>

                                <div class="fw-bolder mt-5">Account Role</div>
                                <div class="text-gray-600">Business</div>

                                <div class="fw-bolder mt-5">Last Activity</div>
                                <div class="text-gray-600">24-11-2022 14:45:38</div>
                                <!--begin::Details item-->
                            </div>
                        </div>
                        <!--end::Details content-->
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col ms-5">
                <div class="card" style="height: 93%;">
                    <div class="card-body pt-3 userdasboardbox">
                        <div class="profile_tab_items d-flex justify-content-between align-baseline position-relative">
                            <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">
                                <li class="nav-item ">
                                    <a class="nav-link text-active-primary pb-4 active border-active-primary border-bottom border-2 ms-0" data-bs-toggle="tab" href="#kt_tab_pane_1">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2" data-bs-toggle="tab" href="#kt_tab_pane_2">Notification</a>
                                </li>
                            </ul>

                        </div>
                        <div class=" py-3">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="kt_tab_pane_1">
                                    <div class="table-responsive">
                                        <div class="position-absolute" style="top: 10px;right: 30px;">
                                            <a href="pre_profile&activation.php" class="btn btn-warning text-dark">Edit Profile</a>
                                        </div>
                                        <div class="">
                                            <h4>Personal Details</h4>
                                            <form action="">
                                                <div class="row mx-0">
                                                    <div class="col-6 ps-0 pt-5">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Full
                                                            Name</label>
                                                        <input type="text" class="form-control form-control-solid" value="Ahmed Salem" readonly="">
                                                    </div>
                                                    <div class="col-6 pt-5">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                                                        <input type="email" class="form-control form-control-solid" value="username@mail.com" readonly="">
                                                    </div>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <div class="fv-row py-5 col-4">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Phone
                                                            number</label>
                                                        <input class="form-control form-control-lg form-control-solid" type="email" value="+965 6845 2875" autocomplete="off" readonly="">
                                                    </div>
                                                    <div class="fv-row p-5 col-5">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Civil
                                                            ID Number</label>
                                                        <input class="form-control form-control-lg form-control-solid" type="email" value="123412341234" autocomplete="off" readonly="">
                                                    </div>
                                                    <div class="fv-row py-5 col-3">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Language</label>
                                                        <select class="form-select form-select-lg form-select-solid select2-hidden-accessible" data-control="select2" data-placeholder="English" data-select2-id="select2-data-1-h9tx" tabindex="-1" aria-hidden="true">
                                                            <option></option>
                                                            <option value="1" selected="" data-select2-id="select2-data-3-7nwd">
                                                                English</option>
                                                            <option value="2">Quwati</option>
                                                            <option value="3">France</option>
                                                        </select>
                                                    </div>

                                                    <div class="fv-row p-0 col-6">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Address
                                                            Type</label>
                                                        <select class="form-select form-select-lg form-select-solid select2-hidden-accessible" data-control="select2" data-placeholder="Select Address Type" data-select2-id="select2-data-4-1181" tabindex="-1" aria-hidden="true">
                                                            <option></option>
                                                            <option value="1" selected="" data-select2-id="select2-data-6-41oo">
                                                                Appartment
                                                            </option>
                                                            <option value="2">House</option>
                                                        </select>
                                                    </div>
                                                    <div class="fv-row pb-5 ps-5 col-6">
                                                        <label class="form-label fs-6 fw-bolder text-dark">Nationality</label>
                                                        <select class="form-select form-select-lg form-select-solid select2-hidden-accessible" data-control="select2" data-placeholder="Select You Nationality" data-select2-id="select2-data-7-48k5" tabindex="-1" aria-hidden="true">
                                                            <option></option>
                                                            <option value="1" selected="" data-select2-id="select2-data-9-er6c">Indian
                                                            </option>
                                                            <option value="2">Quwaitian</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="kt_tab_pane_2">
                                    <div class="table-responsive">
                                        <div class="position-absolute" style="top: 10px;right: 30px;">
                                            <a href="#." class="btn btn-primary">Print Report</a>
                                        </div>
                                        <h4 class="mb-5">Notifications</h4>
                                        <p class="mb-5">Set your notification preference for when you
                                            are in or away from the dashboard<br>You need to configure
                                            your settings to allow notification from Tothing.com:</p>
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification for new comments.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid my-3">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification for status changes.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification for membership expriration.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid my-3">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification for users add on.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get Notification on deleting contract.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid my-3">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification on reviewed contract.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification on signature.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid my-3">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification on cancted contract.
                                            </span>
                                        </label>
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="">
                                            <span class="form-check-label">
                                                Get notification on new or draft contract
                                            </span>
                                        </label>
                                        <div class="notification_tab_inr_section row mx-0">
                                            <div class="col-6 notification_tab_left">
                                                <h5 class="mt-8 mb-5">When you receive a chat message
                                                </h5>
                                                <div class="form-check form-switch form-check-custom form-check-solid ">
                                                    <input class="form-check-input " type="checkbox" value="" id="flexSwitchChecked" checked="checked">
                                                    <label class="form-check-label" for="flexSwitchChecked">
                                                        Play a sound
                                                    </label>
                                                </div>
                                                <div class="form-check form-switch form-check-custom form-check-solid my-3">
                                                    <input class="form-check-input " type="checkbox" value="" id="flexSwitchChecked">
                                                    <label class="form-check-label" for="flexSwitchChecked">
                                                        Highlight the bell icon in the system tray
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-6 notification_tab_right">
                                                <h4 class="mt-8 mb-5">Remainders</h4>
                                                <div class="d-flex justify-content-between">
                                                    <div class="w-50">
                                                        <p>Quidam alii sunt, et non est in nostra
                                                            potestate.<br>Quae omnia in nostra</p>
                                                    </div>
                                                    <div class="mb-10">
                                                        <div class="form-check form-switch form-check-custom form-check-solid ">
                                                            <input class="form-check-input " type="checkbox" value="" id="flexSwitchChecked" checked="checked">
                                                            <label class="form-check-label" for="flexSwitchChecked">
                                                                Push
                                                            </label>
                                                        </div>
                                                        <div class="form-check form-switch form-check-custom form-check-solid my-2">
                                                            <input class="form-check-input " type="checkbox" value="" id="flexSwitchChecked">
                                                            <label class="form-check-label" for="flexSwitchChecked">
                                                                Email
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="kt_tab_pane_3">
                                    <div class="table-responsive">
                                        <div class="position-absolute" style="top: 10px;right: 30px;">
                                            <a href="#." class="btn btn-primary">Print Report</a>
                                        </div>
                                        <div class="d-flex justify-content-between my-7">
                                            <div>
                                                <p>Current Membership</p>
                                                <h4>Basic Membership</h4>
                                                <p>Membership Expiry Date -13-11-2022 23:59:59</p>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary btn-sm" data-bs-toggle="modal" type="button" href="#exampleModalToggle10">Upgrade
                                                    Membership</button>
                                            </div>
                                        </div>
                                        <div class="separator border-3 mb-10"></div>
                                        <div>
                                            <h4>Contracts</h4>
                                            <p>Contract Available - 01</p>
                                            <p>Contract Used - 00</p>
                                            <h4 class="my-5">Buy Additional Contract</h4>
                                            <table class="table table-row-dashed table-row-gray-300 gy-7">
                                                <tbody>
                                                    <tr>
                                                        <td>Number of contract</td>
                                                        <td><input type="number" value="1" class="form-control form-control-solid form-control-sm">
                                                        </td>
                                                        <td>X</td>
                                                        <td>1.000 KWD</td>
                                                        <td>=</td>
                                                        <td>20.000 KWD</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="d-flex justify-content-end mb-10">
                                                <a class="btn btn-sm btn-warning text-end text-dark" data-bs-toggle="modal" type="button" href="#exampleModalToggle7">Pay
                                                    Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!--end::Modal - New Card-->
                    <!--end::Modals-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Post-->
        </div>





        <style>
            .nav-line-tabs .nav-item .nav-link.active,
            .nav-line-tabs .nav-item .nav-link:hover:not(.disabled),
            .nav-line-tabs .nav-item.show .nav-link {
                border-bottom: unset;
            }

            .nav-line-tabs.nav-line-tabs-2x .nav-item .nav-link.active,
            .nav-line-tabs.nav-line-tabs-2x .nav-item .nav-link:hover:not(.disabled),
            .nav-line-tabs.nav-line-tabs-2x .nav-item.show .nav-link {
                border-bottom-width: unset;
            }

            .btncolorblue a,
            button.btn.btncolorblue {
                background-color: #0a3857;
                color: #fff;
            }

            .btncolorblue a:hover {
                background-color: #ccc;
                color: #0a3857;
            }

            .form-select.form-select-solid {
                color: #a1a5b7;
            }

            .template_img.pb-3 img {
                width: 100%;
            }
        </style>


    </div>
</div>
<!--end::Root-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    $("#business-tab").click(function() {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function() {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function() {
        $('#addresstype').on('change', function() {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function() {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    $(".userprofile").addClass("border-danger");
    $("#userheaderchange").click(function() {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function() {
        var max_height = 0;
        $(this).each(function() {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function() {
            $(this).height(max_height);
        });
    };

    $(document).ready(function() {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer_premium.php") ?>