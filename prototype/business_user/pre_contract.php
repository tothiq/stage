<?php include("header_premium.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
  <!--begin::Toolbar container-->
  <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
    <!--begin::Page title-->
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
      <!--begin::Title-->
      <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
        Contracts</h1>
      <!--end::Title-->
    </div>
    <!--end::Page title-->

  </div>
  <!--end::Toolbar container-->
</div>
<div class="container-xxl pb-20" id="kt_content">

  <div id="kt_content_container">
    <div class="post d-flex flex-column-fluid" id="kt_post">
      <!-- <div class="post d-flex flex-column-fluid" id="kt_post"> -->
      <div id="kt_content_container">
        <div class="d-flex flex-column flex-xl-row">
          <div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-5">
            <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
              <div data-kt-menu-trigger="click" class="menu-item menu-accordion hover show card h-100">
                <div class="card-body ">
                  <div class="left_new_folder px-4 pb-4">
                    <a href="#." class="text-decoration-underline text-primary fs-5 fw-bold" data-bs-toggle="modal" data-bs-target="#create_contract">Add New
                      Folder</a>
                  </div>
                  <span class="menu-link">
                    <span class="menu-title fs-5 fw-bold">My Folders</span>
                    <span class="menu-arrow"></span>
                  </span>
                  <div class="menu-sub menu-sub-accordion menu-active-bg show" kt-hidden-height="312">
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                      <span class="menu-link">
                        <span class="menu-bullet">
                          <span class="bullet bullet-dot"></span>
                        </span>
                        <span class="menu-title fs-5 fw-bold">General Folders</span>
                        <!-- <span class="menu-arrow"></span> -->
                      </span>
                    </div>
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion">
                      <span class="menu-link">
                        <span class="menu-bullet">
                          <span class="bullet bullet-dot"></span>
                        </span>
                        <span class="menu-title fs-5 fw-bold">Rent Agreement</span>
                        <span class="menu-arrow"></span>
                      </span>
                      <div class="menu-sub menu-sub-accordion menu-active-bg">
                        <div class="menu-item">
                          <a class="menu-link" href="#.">
                            <span class="menu-bullet">
                              <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title fs-5 fw-bold">Home Rent Agreement</span>
                          </a>
                        </div>
                        <div class="menu-item">
                          <a class="menu-link" href="#.">
                            <span class="menu-bullet">
                              <span class="bullet bullet-dot"></span>
                            </span>
                            <span class="menu-title fs-5 fw-bold">Office Rent Agreement</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end::Sidebar-->
      <!--begin::Content-->
      <div class="flex-lg-row-fluid ms-lg card ms-4">
        <div class="card-body">

          <!--begin:::Tabs-->
          <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold row g-5 g-xxl-8 m-0">
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0 mt-0">
              <a class="nav-link text-active-primary active card bg-secondary bg-opacity-25 mb-xl-0 ms-0" data-bs-toggle="tab" href="#all_contrcats">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">8140</div>
                  <div class="fw-bold text-dark-100">Used</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0 mt-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0" data-bs-toggle="tab" href="#drafts">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">140</div>
                  <div class="fw-bold text-dark-100">Draft</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0 mt-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0" data-kt-countup-tabs="true" data-bs-toggle="tab" href="#under-review">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">1340</div>
                  <div class="fw-bold text-dark-100">Under-Review</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0 mt-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0 me-0" data-bs-toggle="tab" href="#ready">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">4312</div>
                  <div class="fw-bold text-dark-100">Ready</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0 ms-0" data-bs-toggle="tab" href="#signed">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">2233</div>
                  <div class="fw-bold text-dark-100">Signed</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0" data-bs-toggle="tab" href="#rejected">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">02</div>
                  <div class="fw-bold text-dark-100">Rejected</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0" data-bs-toggle="tab" href="#deleted">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">108</div>
                  <div class="fw-bold text-dark-100">Deleted</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
            <!--begin:::Tab item-->
            <li class="col-xl-3 nav-item px-0">
              <a class="nav-link text-active-primary card bg-secondary bg-opacity-25 mb-xl-0" data-bs-toggle="tab" href="#cancelled">
                <div class="card-body p-3">
                  <div class="text-dark-100 fw-bolder fs-2 mb-2">05</div>
                  <div class="fw-bold text-dark-100">Cancelled</div>
                </div>
              </a>
            </li>
            <!--end:::Tab item-->
          </ul>
          <!--end:::Tabs-->
          <div class="d-flex align-items-center py-10 justify-content-end">
            <div class="contract_filter">
              <select name="status" data-control="select2" data-hide-search="true" data-placeholder="Filter" class="form-select form-select-sm border-body bg-body w-150px select2-hidden-accessible" data-select2-id="select2-data-10-jwzz" tabindex="-1" aria-hidden="true" data-kt-initialized="1">
                <option value="1" data-select2-id="select2-data-12-qg2p">Recently
                  Updated
                </option>
                <option value="2" data-select2-id="select2-data-133-yd34">Last Month
                </option>
                <option value="3" data-select2-id="select2-data-134-l5qa">Last
                  Quarter</option>
                <option value="4" data-select2-id="select2-data-135-xx10">Last Year
                </option>
              </select>
            </div>
          </div>
          <!--begin:::Tab content-->
          <div class="tab-content ms-2" id="myTabContent">
            <!--begin:::Tab pane-->
            <div class="tab-pane fade show active" id="all_contrcats" role="tabpanel">
              <!--begin::Card-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg text-success"></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center bg-light-secondary">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg text-success"></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg text-success"></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg text-success"></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Draft</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Card-->
            </div>
            <!--end:::Tab pane-->
            <!--begin:::Tab pane-->
            <div class="tab-pane fade" id="drafts" role="tabpanel">
              <!--begin::Card-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Draft</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Card-->
            </div>
            <!--end:::Tab pane-->
            <!--begin:::Tab pane-->
            <div class="tab-pane fade" id="under-review" role="tabpanel">
              <!--begin::Earnings-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Under - Review</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Statements-->
            </div>
            <div class="tab-pane fade" id="ready" role="tabpanel">
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Ready</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="top-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="top-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="signed" role="tabpanel">
              <!--begin::Earnings-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Signed</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin: 0px 20px;">Add Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Statements-->
            </div>
            <div class="tab-pane fade" id="rejected" role="tabpanel">
              <!--begin::Earnings-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Rejected</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Statements-->
            </div>
            <div class="tab-pane fade" id="deleted" role="tabpanel">
              <!--begin::Earnings-->
              <div class="card mb-4 me-4 border-secondary border border-2">
                <div class="row g-0">
                  <div class="col-md-2 cont_block_1 d-flex justify-content-center align-items-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <i class="fas fa-user-circle fa-lg rounded"></i>
                    </span>
                  </div>
                  <div class="col-md-6 cont_block_2">
                    <div class="card-body p-5">
                      <h5 class="card-title">Rental Agreement For Al-John Tower Office with
                        Tothiq</h5>
                      <p class="card-text mb-0">Create Date: 12-11-2022 14:35:29</p>
                      <div class="d-flex">
                        <p class="card-text mb-0"><small class="text-muted">Status: Deleted</small></p>
                        <a href="#." class="card-text"><small class="text-muted" style="margin-right:20px;margin-left:20px;">Add Appendix</small></a>
                        <a href="#." class="card-text"><small class="text-muted">Duplicate</small></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 cont_block_3 d-flex align-items-center justify-content-center">
                    <span class="svg-icon svg-icon-2 d-flex align-items-center">
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                      <a href="#.">
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                          <i class="fas fa-user-circle fa-lg "></i>
                        </div>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px shadow-lg" data-kt-menu="true" style="z-index: 105; position: fixed; inset: auto 0px 0px auto; margin: 0px; transform: translate3d(-70px, -35px, 0px);" data-popper-placement="bottom-end">
                          <!--begin::Menu item-->
                          <div class="card ">
                            <div class="card-body text-dark">
                              <div>
                                <div class="card_user_info d-flex justify-content-between align-items-center">
                                  <i class="fas fa-user-circle fa-lg"></i>
                                  <p class="mb-0 bg-light p-3 rounded-pill">Signatory</p>
                                </div>
                                <div class="pt-5">
                                  <h3>Aabirah Aadab Aadil</h3>
                                  <p>Tothiq</p>
                                </div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p class="mb-0">Email:</p>
                                    <p>aabirah.aadil@domain.com</p>
                                    <p class="mb-0">Mobile Number:</p>
                                    <p>+965-123456789</p>
                                    <p class="mb-0">Hawati Verification:</p>
                                    <p>Verified</p>
                                  </span></div>
                                <div class="separator separator-dashed border-3 my-5"></div>
                                <div><span>
                                    <p>Not viewed</p>
                                  </span></div>
                              </div>
                            </div>
                          </div>

                          <!--end::Menu item-->
                        </div>
                      </a>
                    </span>
                  </div>
                </div>
              </div>
              <!--end::Statements-->
            </div>
            <div class="tab-pane fade" id="cancelled" role="tabpanel">
              <!--begin::Earnings-->

              <!--end::Statements-->
            </div>
            <!--end:::Tab pane-->
          </div>
          <!--end:::Tab content-->
        </div>
      </div>
      <!--end::Content-->
      <!-- </div> -->
    </div>
  </div>

</div>


<!--begin::Modals-->
<div class="modal fade" id="create_contract" tabindex="-1" aria-modal="true" role="dialog">
  <!--begin::Modal dialog-->
  <div class="modal-dialog modal-dialog-centered mw-900px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2>Create Folder</h2>
        <!--end::Modal title-->
        <!--begin::Close-->
        <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
          <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
          <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
              <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
              <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black">
              </rect>
            </svg>
          </span>
          <!--end::Svg Icon-->
        </div>
        <!--end::Close-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body py-lg-10 px-lg-10">
        <!--begin::Stepper-->
        <div class="popup_select_parent_folder mt-5 mb-10">
          <h3>Select Parent Folder</h3>
          <select class="form-select" aria-label="Select example">
            <option>Open this select menu</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
        </div>
        <div class="popup_select_folder_name my-10">
          <h3>Folder Name</h3>
          <input type="text" class="form-control" placeholder="name@example.com" />
        </div>
        <div class="popup_create_folder_btn d-flex justify-content-end">
          <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_contract">Create
            Folder</a>
        </div>

        <!--end::Stepper-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
<!--end::Modals-->


<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
  <span class="svg-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
      <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
      <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
    </svg>
  </span>
</div>


<script>
  ClassicEditor
    .create(document.querySelector('#ckeditor_1'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_2'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_3'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
</script>
<script>
  // $(document).ready(function () {
  //     $(".companyform").click(function () {
  //         var test = $(this).val();
  //         if(test =='yes'){
  //             $("div.desc2").hide();
  //             $("div.desc1").show();
  //         }else{
  //             $("div.desc1").hide();
  //             $("div.desc2").show();

  //         }
  //     });
  // });
  $("#business-tab").click(function() {
    $("div#myTabContent1").hide();
    $("div#myTabContent2").show();
  });
  $("#individual-tab").click(function() {
    $("div#myTabContent1").show();
    $("div#myTabContent2").hide();
  });
  $(document).ready(function() {
    $('#addresstype').on('change', function() {
      var demovalue = $(this).val();
      $("div.myDiv").hide();
      $("#show" + demovalue).show();
    });
    $('#addresstype1').on('change', function() {
      var demovalue1 = $(this).val();
      $("div.myDiv1").hide();
      $("#show" + demovalue1).show();
    });
  });
  // var profileborder = "border-danger";
  $(".userprofile").addClass("border-danger");
  // function changeuserborder() {
  //     $(".userprofile").removeClass(profileborder);
  //     var profileborder = "border-success";
  // }
  $("#userheaderchange").click(function() {
    $(".userprofile").removeClass("border-danger");
    $(".userprofile").addClass("border-success");
    $("#headererror").addClass("d-none");

  });
</script>
<script>
  $.fn.equalHeights = function() {
    var max_height = 0;
    $(this).each(function() {
      max_height = Math.max($(this).height(), max_height);
    });
    $(this).each(function() {
      $(this).height(max_height);
    });
  };

  $(document).ready(function() {
    $('.userdasboardbox ul li a .card').equalHeights();
  });
</script>

<?php include("footer_premium.php") ?>