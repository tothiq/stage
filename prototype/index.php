<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="assets\css\bu_style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets\plugins\global\plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="assets/images/Favicon.png" />


</head>

<body class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                    <div class="logo text-center">
                        <a href="#.">
                            <img alt="Logo" src="assets/images/logo.png">
                            <!-- <p class="pt-3 fs-4 fw-bolder text-dark">Digital Contracts Platform</p> -->
                        </a>
                    </div>
                    <form class="form w-100">
                        <div class="text-center mb-10">
                            <div class="row">
                                <div class="col-12 mt-10">
                                    <a href="../prototype/individual_user/index.php"
                                        class="btn btn-lg btn-primary w-50">Individual User</a>
                                </div>
                                <!-- <div class="col-12">
                                    <a href="https://stage.tothiq.com/prototype/business_user/"
                                        class="btn btn-lg btn-primary w-50">Business User</a>
                                </div> -->
                                <div class="col-12 my-10">
                                    <a href="../prototype/business_admin/index.php"
                                        class="btn btn-lg btn-primary w-50">Business Admin</a>
                                </div>
                                <!-- <div class="col-12">
                                    <a href="../prototype/super_admin/index.php"
                                        class="btn btn-lg btn-primary w-50">Super Admin</a>
                                </div> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        var hostUrl = "assets/";
    </script>
    <script src="\assets\plugins\global\plugins.bundle.js"></script>
    <script src="\assets\js\scripts.bundle.js"></script>
    <script src="\assets\js\custom\authentication\sign-in\general.js"></script>
</body>

</html>