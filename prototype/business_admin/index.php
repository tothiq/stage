<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!-- <link rel="shortcut icon" href="tothiq_admin/assets/media/logos/logo.png" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/css/ba_style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/ba_media.css" id="media_style">
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&display=swap" rel="stylesheet">
</head>

<body class="bg-body">
    <div class="index_menu index_menu--active">
        <div class="index_container index_menu__wrapper">
            <div class="index_header_row">
                <div class="index_menu__logo index_menu__item">
                    <a href="https://stage.tothiq.com/">
                        <img src="../assets/images/logo.png" alt="">
                        <!-- <h3>Digital Contract Platform</h3> -->
                    </a>
                </div>
                <div class="index_menu__item d-t-none">
                    <nav class="index_menu__center-nav">
                        <ul>
                            <li>
                                <div class="index_menu__dropdown">
                                    <a href="https://stage.tothiq.com/" class="link link--gray">Home</a>
                                    <!-- <span><i class="mdi mdi-chevron-down"></i></span> -->
                                    <!-- <div class="menu__dropdown-content menu__dropdown-content--home">
                  <a class="link link--gray" href="01_mobileapp.html">Mobile App</a>
                  <a class="link link--gray" href="02_messenger.html">Messenger</a>
                  <a class="link link--gray link--gray-active" href="#">Web App</a>
                  <a class="link link--gray" href="04_desktop.html">Desktop App</a>
                </div> -->
                                </div>
                            </li>
                            <li><a href="https://stage.tothiq.com/prototype/individual_user/" class="link link--gray">Pricing</a></li>
                            <li><a href="https://stage.tothiq.com/help-center.php" class="link link--gray">Support</a></li>
                            <!-- <li><a href="login_page.html" class="link link--gray">Create Contract</a></li> -->
                            <!-- <li><a href="template.php" class="link link--gray">Templates</a></li> -->
                            <!-- <li><a href="help-center.php" class="link link--gray">Help Center</a></li> -->
                            <li><a href="https://stage.tothiq.com/contact_us_form.php" class="link link--gray">Get In Touch</a></li>
                            <li>
                                <div class="index_menu__dropdown d-t-none">
                                    <a class="link link--gray index_menu__dropdown-btn" style="font-family: 'Aref Ruqaa'; font-size:16px">عربــي
                                        <!-- <span><i class="mdi mdi-chevron-down"></i></span> -->
                                    </a>
                                    <!-- <div class="menu__dropdown-content">
                    <a class="link link--gray link--gray-active" href="#">En</a>
                    <a class="link link--gray" href="#">عربي</a>
                  </div> -->
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="index_menu__item">
                    <nav class="index_menu__right-nav d-l-none">
                        <ul>
                            <a href="../business_admin/registration_screen.php" class="btn btn-primary text-white" style="margin-right:10px">Sign Up</a>
                            <a href="../business_admin/sign_in.php" class="btn btn-primary text-white">Login</a>
                        </ul>
                    </nav>
                    <div class="d-none d-t-block">
                        <button type="button" class="menu__mobile-button">
                            <span><i class="mdi mdi-menu" aria-hidden="true"></i></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed index_str">
            <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                <div class="p-10 p-lg-15 mx-auto">
                    <div class="pricing_heading pb-20 text-center">
                        <h3>Pricing</h3>
                    </div>
                    <div class="fv-row col-12 pb-5">
                        <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold text-center">
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1 disabled ms-0" data-bs-toggle="tab" id="individual-tab" href="#Individualform">Individual Memberships</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1 active ms-5" data-bs-toggle="tab" id="business-tab" href="#Businessform">Business Memberships</a>
                            </li>
                        </ul>
                    </div>
                    <!-- <div class="tab-content w-100 pt-10" id="myTabContent11" style="display:none;">
                        <div class="tab-pane fade show active" id="Individualform" role="tabpanel">
                            <div class="row m-0">
                                <div class="col bg-light pt-15 pb-10 px-5 position-relative border border-primary rounded ">
                                    <h2>Tothiq - Starter</h2>
                                    <h5>Free</h5>
                                    <ul>
                                        <li>Free sign up</li>
                                        <li>1 Free contract</li>
                                        <li>Free contract templates</li>
                                        <li>Add up to 2 parties per contract</li>
                                        <li>Save contact on address book</li>
                                        <li>Free contract storage</li>
                                    </ul>
                                    <a href="../individual_user/free_registration_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                                <div class="col bg-light pt-10 pb-10 position-relative px-5 mx-15 border border-primary rounded">
                                    <a href="#."><span class="position-absolute top-0 start-50 translate-middle h-30px  badge fs-5 fw-light text-dark pt-2 badge-light-danger border border-2 border-danger">Random
                                            Special - 20% OFF</span></a>
                                    <span class="badge badge-light-warning text-dark">Best value Individual</span>
                                    <h2>Tothiq - Basic</h2>
                                    <p class="fs-4">KD 9.000/Year</p>
                                    <ul>
                                        <li>Free sign up</li>
                                        <li>2 Free contracts</li>
                                        <li>Free and Premium contract templates (limited)</li>
                                        <li>Add up to 3 parties per contract</li>
                                        <li>Save contacts on address book</li>
                                        <li>Create Blank Contract</li>
                                        <li>Free contract storage</li>
                                    </ul>
                                    <a href="../individual_user/basic_checkout_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                                <div class="col bg-light pt-15 pb-10 position-relative px-5 border border-primary rounded">
                                    <h2>Tothiq - Premium</h2>
                                    <p class="fs-4">KD 19.000/Year</p>
                                    <ul>
                                        <li>Free sign up</li>
                                        <li>3 Free contracts</li>
                                        <li>Unlimited parties</li>
                                        <li>Chat between parties</li>
                                        <li>Unlimited contract templates</li>
                                        <li>Save contacts on address book</li>
                                        <li>Create Blank Contract</li>
                                        <li>Free contract storage</li>
                                        <li>Upload contract</li>
                                        <li>View log</li>
                                    </ul>
                                    <a href="../individual_user/premium_checkout_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="tab-content mt-5" id="myTabContent22">
                        <div class="tab-pane fade show active" id="Businessform" role="tabpanel">
                            <div class="row m-0">
                                <div class="col bg-light pt-10 pb-10 px-5 position-relative border border-primary rounded">
                                    <h2>Free</h2>
                                    <p class="fs-4">KD 0.000/Year</p>
                                    <ul>
                                        <li>Free sign up</li>
                                        <li>0 Free contract</li>
                                        <li>Free contract templates only</li>
                                        <li>Add up to 2 parties per contract</li>
                                        <li>Save contact on address book</li>
                                        <li>Free contract storage</li>
                                    </ul>
                                    <a href="../business_admin/registration_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                                <div class="col bg-light pt-10 pb-10 position-relative px-5 mx-15 border border-primary rounded">
                                    <a href="#."><span class="position-absolute top-0 start-50 translate-middle h-30px  badge fs-5 fw-light text-dark pt-2 badge-light-danger border border-2 border-danger">Special Offer - 20% OFF</span></a>
                                    <h2>Basic</h2>
                                    <p class="fs-4">KD 79.000/Year</p>
                                    <ul>
                                        <li>20 Free contracts</li>
                                        <li>Free and Premium contract templates (limited)</li>
                                        <li>Add up to 4 parties per contract</li>
                                        <li>Save contacts on address book</li>
                                        <li>Create Blank Contract</li>
                                        <li>Free contract storage</li>
                                    </ul>
                                    <a href="../business_admin/registration_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                                <div class="col bg-light pt-10 pb-10 position-relative px-5 border border-primary rounded">
                                    <h2>Premium</h2>
                                    <p class="fs-4">KD 129.000/Year</p>
                                    <ul>
                                        <li>50 Free contracts</li>
                                        <li>Unlimited parties</li>
                                        <li>Chat between parties</li>
                                        <li>Unlimited contract templates</li>
                                        <li>Save contacts on address book</li>
                                        <li>Create Blank Contract</li>
                                        <li>Free contract storage</li>
                                        <li>Upload contract</li>
                                        <li>View log</li>
                                    </ul>
                                    <a href="../business_admin/registration_screen.php"><button class="position-absolute top-100 start-50 translate-middle  btn btn-warning text-dark">Let's
                                            Start</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="regis_lower_box border border-primary my-20 p-10 rounded d-flex justify-content-between align-items-center">
                        <div class="high-vol_txt">
                            <h2>High-volume Contracts</h2>
                            <p>For High-volume Contracts kindly submit an Inquiry.</p>
                        </div>
                        <div class="">
                            <a href="#." class="btn btn-sm btn-primary rounded-1">Submit Inquiry</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="index_footer-menu">
        <div class="index_footer_row">
            <div class="index_footer-left">
                <div class="text-block-left">© 2023 Tothiq Web Design Company.</div>
            </div>
            <div class="index_footer-middle">
                <div class="index_footer_social-wrapper">
                    <a href="https://www.linkedin.com/company/tothiq" target="_blank" class="index_footer_social-link is-first w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bc8a2f32922df62802b_Linkedin.svg" loading="lazy" alt="">
                    </a>
                    <a href="https://twitter.com/TothiqPage" target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bd05ad8e3573d182785_Twitter.svg" loading="lazy" alt="">
                    </a>
                    <a href="https://www.instagram.com/tothiqpage/" target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45bd6a2f329796a6280a1_Instagram.svg" loading="lazy" alt="">
                    </a>
                    <a href="https://www.facebook.com/Tothiq" target="_blank" class="index_footer_social-link w-inline-block">
                        <img src="https://uploads-ssl.webflow.com/62c0651aabd7814a340cbc35/62c45be3e711c4fe07eb31df_Facebook.svg" loading="lazy" alt="">
                    </a>
                </div>
                <ol role="list" class="w-list-unstyled">
                    <!-- <li><a href="https://stage.tothiq.com/about_us.php" class="link link--gray">About</a></li> -->
                    <!-- <li><a href="https://stage.tothiq.com/template.php" class="link link--gray">Templates</a></li> -->
                    <!-- <li><a href="#." class="link link--gray">Blog</a></li> -->
                    <li><a href="#." class="link link--gray">Privacy</a></li>
                    <li><a href="#." class="link link--gray">Terms &amp; Conditions</a></li>
                    <!-- <li><a href="#." class="link link--gray">Disclaimer</a></li> -->
                </ol>
            </div>
            <div class="index_footer-right">
                <h3>Application Download</h3>
                <div class="ftr-social-btns">
                    <a class="app_btn" href="http:apple.com">
                        <img src="../assets/images/ios.jpg" alt="">
                    </a>
                    <a class="and_btn" href="http:google.com">
                        <img src="../assets/images/android.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <style>
        .formboxindividual {
            display: flex;
            flex-wrap: wrap;
        }

        .btncolorblue a,
        button.btn.btncolorblue {
            background-color: #0a3857;
            color: #fff;
        }

        .btncolorblue a:hover {
            background-color: #ccc;
            color: #0a3857;
        }
    </style>

    <script>
        $(document).ready(function() {
            $("#individual-tab").click(function() {
                $("div#myTabContent11").show();
                $("div#myTabContent22").hide();
            });
            $("#business-tab").click(function() {
                $("div#myTabContent22").show();
                $("div#myTabContent11").hide();
            });
        });
        $(document).ready(function() {
            $('#addresstype').on('change', function() {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function() {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
    </script>

</body>

</html>