<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/ba_style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/ba_media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light" data-kt-drawer="true" data-kt-drawer-name="aside"
                data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start"
                data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="index.php">
                        <img alt="Logo" src="../assets/media/logos/logo.png" class="w-60px">
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                    <!--end::Logo-->
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0" style="height: 188px;">
                    </div>
                    <div class="aside-footer flex-column-auto pt-5 pb-7 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link" href="#.">
                                <span class="menu-icon">
                                    <span class="svg-icon svg-icon-2">
                                        <i class="fas fa-bell fa-lg" "=""></i>
                                    </span>
                   
                                </span>
                                <!-- <span class=" menu-title">Notification</span> -->
                            </a>
                        </div>
        
                        <div class="d-flex align-items-center" id="kt_header_user_menu_toggle">
                            <!--begin::Menu wrapper-->
                            <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                <div class="menu-item">
                                    <a class="menu-link">
                                        <span class="menu-icon">
                                            <span class="svg-icon svg-icon-2 position-relative">
                                                <i class="fas fa-user-circle fa-lg"><div class="position-absolute top-0 start-100 translate-middle  badge badge-circle badge-success h-10px w-10px">
                                                </div></i>
                                                
                                            </span>
                                        </span>
                                        <!-- <span class=" menu-title">User Profile</span> -->
                                    </a>
                                </div>
                            </div>
                            <!--begin::Menu-->
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                                data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <div class="menu-content d-flex align-items-center px-3 row">
                                        <!--begin::Username-->
                                        <div class="col-3">
                                            <div class="profile_img">
                                                <img src="../assets/images/150-2.jpg" alt="" class="w-100 rounded-circle">
                                            </div>
                                        </div>
                                        <div class="col-6 align-items-center">
                                            <div class="fw-bolder d-flex align-items-center fs-5">Tothiq User
                                            </div>
                                            <p class="fw-bold text-muted text-hover-primary fs-7 mb-0">Free Membership</p>
                                        </div>
                                        <!--end::Username-->
                                    </div>
                                </div>
                                <div class="separator my-2"></div>
                                <div class="menu-item px-5">
                                    <a href="" class="menu-link px-5">My Profile</a>
                                </div>
                                <div class="menu-item px-5">
                                    <a href="sign_in.php" class="menu-link px-5">Sign Out</a>
                                </div>
                            </div>
                            <!--end::Menu-->
                            <!--end::Menu wrapper-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div id="kt_header" class="header">
                <div class="container-fluid px-0">
                    <div class="row mx-0">
                        <div class="col-9 d-flex align-items-center justify-content-evenly">
                            <div class="header_icon">
                                <i class="fas fa-clock fa-lg"></i>
                            </div>
                            <div class="header_text">
                                <h3 class="fs-6 fw-bold text-gray-500 mb-0">Your account isn't active yet. Please fill out the necessary information, send in the necessary documents, and verify Hawati</h3>
                            </div>
                        </div>
                        <div class="col-3 d-flex justify-content-center">
                            <a href="profile&amp;activation.php" class="btn btn-primary text-white">Activate my account</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content d-flex flex-column p-0" id="kt_content">
                <div class="toolbar" id="kt_toolbar">
                    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                        <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                            class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                            <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-xxl mt-5">
                    <div class="card mx-5 mt-5">
                        <div class="card-body">
                            <div class="card_body_highlited_text mb-10">
                                <span class="bg-warning text-dark">Stactic content start</span>
                            </div>
                            <h2 class="lh-base fw-700 mb-15">Rental Agreement For Al-John<br>Tower Office
                                with Tothiq</h2>
                            <div>
                                <span class="fs-6 fw-bold">Contract Starting Date : 01-01-2023
                                    12:00:00</span>
                            </div>
                            <div>
                                <span class="fs-6 fw-bold">Contract Period : 1 Year</span>
                            </div>
                            <div>
                                <span class="fs-6 fw-bold">Contract Value : KWD 12000.000</span>
                            </div>
                            <div class="card_table my-15">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr class="fw-bold fs-6 text-gray-800">
                                                <th class="border border-1 border-secondary p-2">Parties
                                                    Information</th>
                                                <th class="border border-1 border-secondary p-2">Name Of
                                                    Person</th>
                                            </tr>
                                        </thead>
                                        <tbody class="border border-1 border-secondary p-2">
                                            <tr>
                                                <td class="border border-1 border-secondary p-2">First Party
                                                </td>
                                                <td class="border border-1 border-secondary p-2">Aabirah
                                                    Aadab Aadil</td>
                                            </tr>
                                            <tr>
                                                <td class="border border-1 border-secondary p-2">Second
                                                    Party</td>
                                                <td class="border border-1 border-secondary p-2">Aadil
                                                    Aabirah Aadab</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card_body_highlited_text mb-10">
                                <span class="bg-warning text-dark">Stactic content end</span>
                            </div>
                            <div class="card_body_highlited_text mb-10">
                                <span class="bg-warning text-dark">Below content are dynamic content</span>
                            </div>
                            <!--begin::Scroll-->
                            <div class="scroll pe-5" data-kt-scroll="true" data-kt-scroll-height="200px" data-kt-scroll-wrappers="#kt_example_js_content" data-kt-scroll-dependencies="#kt_example_js_header, #kt_example_js_footer, #kt_header" data-kt-scroll-offset="100px" style="height: 200px;">
                                <div id="kt_example_js_content">
                                    <p>Quando ambulabat agendis admonere te qualis actio. Si ad corpus, quae
                                        plerumque Imaginare tecum in balineo quidam aquam fundes aliquod discrimen
                                        vituperiis usum alii furantur.</p>

                                    <p>de isto el tutius perducit ad actum ipsum, ut si dico "Ego autem vadam
                                        lavari, ut mens mea in statu naturae conformior. Et similiter circa alias
                                        res. Et sic, si contingit ex per se lavantem, et erit hoc paratus ut
                                        diceret, "Hoc non solum lavari ut desideravit, sed ut animus in statu
                                        naturae convenienter naturae, et non ut si ego quæ ventura offendit."</p>

                                    <p>Quando ambulabat agendis admonere te qualis actio. Si ad corpus, quae
                                        plerumque Imaginare tecum in balineo quidam aquam fundes aliquod discrimen
                                        vituperiis usum alii furantur.</p>

                                    <p>de isto el tutius perducit ad actum ipsum, ut si dico "Ego autem vadam
                                        lavari, ut mens mea in statu naturae conformior. Et similiter circa alias
                                        res. Et sic, si contingit ex per se lavantem, et erit hoc paratus ut
                                        diceret, "Hoc non solum lavari ut desideravit, sed ut animus in statu
                                        naturae convenienter naturae, et non ut si ego quæ ventura offendit."</p>

                                    <p>Quando ambulabat agendis admonere te qualis actio. Si ad corpus, quae
                                        plerumque Imaginare tecum in balineo quidam aquam fundes aliquod discrimen
                                        vituperiis usum alii furantur.</p>

                                    <p>de isto el tutius perducit ad actum ipsum, ut si dico "Ego autem vadam
                                        lavari, ut mens mea in statu naturae conformior. Et similiter circa alias
                                        res. Et sic, si contingit ex per se lavantem, et erit hoc paratus ut
                                        diceret, "Hoc non solum lavari ut desideravit, sed ut animus in statu
                                        naturae convenienter naturae, et non ut si ego quæ ventura offendit."</p>
                                    <p>Quando ambulabat agendis admonere te qualis actio. Si ad corpus, quae
                                        plerumque Imaginare tecum in balineo quidam aquam fundes aliquod discrimen
                                        vituperiis usum alii furantur.</p>

                                    <p>de isto el tutius perducit ad actum ipsum, ut si dico "Ego autem vadam
                                        lavari, ut mens mea in statu naturae conformior. Et similiter circa alias
                                        res. Et sic, si contingit ex per se lavantem, et erit hoc paratus ut
                                        diceret, "Hoc non solum lavari ut desideravit, sed ut animus in statu
                                        naturae convenienter naturae, et non ut si ego quæ ventura offendit."</p>
                                </div>
                            </div>
                            <!--end::Scroll-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="footer py-4 d-flex  flex-lg-column app-footer" id="kt_footer">
            <!--begin::Container-->
            <div
                class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                <!--begin::Copyright-->
                <div class="text-dark order-2 order-md-1">
                    <span class="text-muted fw-bold me-1">© 2023</span>
                    <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right Reserved</a>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Footer-->

    </div>
    <!--end::Root-->



    <!--end::Modals-->
    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black">
                </rect>
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black"></path>
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <!--begin::Javascript-->
    <!--begin::Global Javascript Bundle(used by all pages)-->
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <!--end::Global Javascript Bundle-->
    <!--begin::Page Vendors Javascript(used by this page)-->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <!--end::Page Vendors Javascript-->
    <!--begin::Page Custom Javascript(used by this page)-->
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <div class="ck-body-wrapper">
        <div class="ck ck-reset_all ck-body ck-rounded-corners" dir="ltr">
            <div class="ck ck-balloon-panel ck-balloon-panel_arrow_nw ck-balloon-panel_with-arrow"
                style="top: 0px; left: 0px;">
                <div class="ck ck-balloon-rotator" z-index="-1">
                    <div class="ck-balloon-rotator__navigation ck-hidden"><button class="ck ck-button ck-off"
                            type="button" tabindex="-1"
                            aria-labelledby="ck-editor__aria-label_e84e17d694c7a68dead65ac61d6bfc554"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M11.463 5.187a.888.888 0 1 1 1.254 1.255L9.16 10l3.557 3.557a.888.888 0 1 1-1.254 1.255L7.26 10.61a.888.888 0 0 1 .16-1.382l4.043-4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Previous</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_e84e17d694c7a68dead65ac61d6bfc554">Previous</span></button><span
                            class="ck-balloon-rotator__counter"></span><button class="ck ck-button ck-off" type="button"
                            tabindex="-1" aria-labelledby="ck-editor__aria-label_eca75067e1fdbd0a1efde20a8987ea845"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M8.537 14.813a.888.888 0 1 1-1.254-1.255L10.84 10 7.283 6.442a.888.888 0 1 1 1.254-1.255L12.74 9.39a.888.888 0 0 1-.16 1.382l-4.043 4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Next</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_eca75067e1fdbd0a1efde20a8987ea845">Next</span></button></div>
                    <div class="ck-balloon-rotator__content"></div>
                </div>
            </div>
            <div class="ck-fake-panel ck-hidden" style="top: 0px; left: 0px; width: 0px; height: 0px;"></div>
        </div>
        <div class="ck ck-reset_all ck-body ck-rounded-corners" dir="ltr">
            <div class="ck ck-balloon-panel ck-balloon-panel_arrow_nw ck-balloon-panel_with-arrow"
                style="top: 0px; left: 0px;">
                <div class="ck ck-balloon-rotator" z-index="-1">
                    <div class="ck-balloon-rotator__navigation ck-hidden"><button class="ck ck-button ck-off"
                            type="button" tabindex="-1"
                            aria-labelledby="ck-editor__aria-label_e25c2e2a3c1eb5908c8ec85cf3a49855a"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M11.463 5.187a.888.888 0 1 1 1.254 1.255L9.16 10l3.557 3.557a.888.888 0 1 1-1.254 1.255L7.26 10.61a.888.888 0 0 1 .16-1.382l4.043-4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Previous</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_e25c2e2a3c1eb5908c8ec85cf3a49855a">Previous</span></button><span
                            class="ck-balloon-rotator__counter"></span><button class="ck ck-button ck-off" type="button"
                            tabindex="-1" aria-labelledby="ck-editor__aria-label_ede184070aea39f545b9b5589c353583c"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M8.537 14.813a.888.888 0 1 1-1.254-1.255L10.84 10 7.283 6.442a.888.888 0 1 1 1.254-1.255L12.74 9.39a.888.888 0 0 1-.16 1.382l-4.043 4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Next</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_ede184070aea39f545b9b5589c353583c">Next</span></button></div>
                    <div class="ck-balloon-rotator__content"></div>
                </div>
            </div>
            <div class="ck-fake-panel ck-hidden" style="top: 0px; left: 0px; width: 0px; height: 0px;"></div>
        </div>
        <div class="ck ck-reset_all ck-body ck-rounded-corners" dir="ltr">
            <div class="ck ck-balloon-panel ck-balloon-panel_arrow_nw ck-balloon-panel_with-arrow"
                style="top: 0px; left: 0px;">
                <div class="ck ck-balloon-rotator" z-index="-1">
                    <div class="ck-balloon-rotator__navigation ck-hidden"><button class="ck ck-button ck-off"
                            type="button" tabindex="-1"
                            aria-labelledby="ck-editor__aria-label_e4e0f1a1dc8a5629c67a1cb3247687d8d"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M11.463 5.187a.888.888 0 1 1 1.254 1.255L9.16 10l3.557 3.557a.888.888 0 1 1-1.254 1.255L7.26 10.61a.888.888 0 0 1 .16-1.382l4.043-4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Previous</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_e4e0f1a1dc8a5629c67a1cb3247687d8d">Previous</span></button><span
                            class="ck-balloon-rotator__counter"></span><button class="ck ck-button ck-off" type="button"
                            tabindex="-1" aria-labelledby="ck-editor__aria-label_e5f13649012987adab9cfb8964a9f85e9"><svg
                                class="ck ck-icon ck-button__icon" viewBox="0 0 20 20">
                                <path
                                    d="M8.537 14.813a.888.888 0 1 1-1.254-1.255L10.84 10 7.283 6.442a.888.888 0 1 1 1.254-1.255L12.74 9.39a.888.888 0 0 1-.16 1.382l-4.043 4.042z">
                                </path>
                            </svg><span class="ck ck-tooltip ck-tooltip_s"><span
                                    class="ck ck-tooltip__text">Next</span></span><span class="ck ck-button__label"
                                id="ck-editor__aria-label_e5f13649012987adab9cfb8964a9f85e9">Next</span></button></div>
                    <div class="ck-balloon-rotator__content"></div>
                </div>
            </div>
            <div class="ck-fake-panel ck-hidden" style="top: 0px; left: 0px; width: 0px; height: 0px;"></div>
        </div>
    </div>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>



    <svg id="SvgjsSvg1001" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1"
        xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev"
        style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
        <defs id="SvgjsDefs1002"></defs>
        <polyline id="SvgjsPolyline1003" points="0,0"></polyline>
        <path id="SvgjsPath1004" d="M0 0 "></path>
    </svg>
</body>

</html>