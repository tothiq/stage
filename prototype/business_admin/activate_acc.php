<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="shortcut icon" href="./images/logo.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/ba_style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/ba_media.css" id="media_style">
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body class="bg-body">

    <div class="d-flex flex-column flex-root">
        <div
            class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
            <div class="d-flex flex-center flex-column flex-column-fluid pt-10 pb-lg-20">
                <div class="activate_acc_inr">
                    <div class="acctivate_acc_img">
                        <img src="../assets/media/logos/activate_Acc.png" alt="">
                    </div>
                    <div class="acctivate_acc_t pt-8">
                        <h1 class="d-flex justify-content-center align-items-center fw-boldest" style="font-size: 40px;">You're One Click Away</h1>
                        <p class="d-flex justify-content-center align-items-center fs-4 text-center">Get Smarter contrcts in the blink of an eye!<br>Verify your email to get startted</p>
                    </div>
                    <div class="acctivate_acc_btn d-flex justify-content-center align-items-center pt-8">
                        <a href="basic_set_password.php"><button class="btn btn-warning text-dark">Activate my account</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
        tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-body" data-bs-target="#exampleModalToggle5"
                data-bs-toggle="modal">
                    <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
                        <img class="border border-2" src="../assets/media/logos/process_1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- <div class="modal fade" id="exampleModalToggle5" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
        tabindex="-1" >
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body"  data-bs-target="#exampleModalToggle6"
                data-bs-toggle="modal">
                    <div class="align-center w-100 text-center">
                        <h3>We are grateful that you chose to register with Tothiq.</h3>
                        <p class="my-3">rogipe3528@eilnews.com has received an email with further instructions on how to
                            successfully complete your sign up with Tothiq</p>
                        <p>If you haven't received the instructions within a few minutes of signing up, check the spam
                            folder in your email client.</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- <div class="modal fade" id="exampleModalToggle6" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
        tabindex="-1" data-bs-dismiss="modal">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="align-center w-100 text-center">
                        <img class="border border-2" src="../assets/media/logos/email_verification.png" alt="">
                    </div>
                    <div class="pt-3 text-end">
                        <a type="button" class="btn btn-primary" href="profile&activation.php" type="button">Submit</a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>

</html>