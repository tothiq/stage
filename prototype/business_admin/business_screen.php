
<?php include("header.php") ?>

<div id="kt_app_toolbar" class="app-toolbar py-8">
  <!--begin::Toolbar container-->
  <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
    <!--begin::Page title-->
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
      <!--begin::Title-->
      <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
        Business Panel</h1>
      <!--end::Title-->
    </div>
    <!--end::Page title-->
  </div>
  <!--end::Toolbar container-->
</div>
<div class="post" id="kt_post">
  <div class="content_heading mt-5 container-xxl">
    <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">
      <li class="nav-item ">
        <a class="nav-link text-active-primary pb-4 active border-active-primary border-bottom border-2 ms-0"
          data-bs-toggle="tab" href="#business_management">Business Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2" data-bs-toggle="tab"
          href="#user_management">User Management</a>
      </li>
      <!-- <li class="nav-item">
                            <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2"
                                data-kt-countup-tabs="true" data-bs-toggle="tab" href="#department_management">Department
                                Management</a>
                        </li> -->
      <li class="nav-item">
        <a class="nav-link text-active-primary pb-4 border-active-primary border-bottom border-2"
          data-kt-countup-tabs="true" data-bs-toggle="tab" href="#membership_management">Membership
          Management</a>
      </li>
    </ul>
    <div class="py-3" data-select2-id="select2-data-6-xepc" style="margin-bottom: 60px;">
      <div class="tab-content">
        <div class="tab-pane fade active show" id="business_management">
          <div class="table-responsive">
            <div class="card-body bg-white pt-8">
              <div class="card_body_highlited_text d-flex justify-content-between align-items-center">
                <span class="text-dark fs-3 fw-bolder">Business Information</span>
                <a href="#." class="btn btn-primary text-white">Edit</a>
              </div>
              <div class="d-flex flex-wrap">
                <div class="fv-row p-5 ps-0 col-4">
                  <label class="form-label fs-6 fw-bolder text-dark">Business Name</label>
                  <h3 class="fs-6 fw-normal">Tothiq Web Design Company</h3>
                </div>
                <div class="fv-row p-5 col-3">
                  <label class="form-label fs-6 fw-bolder text-dark">Business Email
                    Address</label>
                  <h3 class="fs-6 fw-normal">info@tothiq.com</h3>
                </div>
                <div class="fv-row p-5 col-3">
                  <label class="form-label fs-6 fw-bolder text-dark">Business Contact
                    Number</label>
                  <h3 class="fs-6 fw-normal">+965-1234 1234</h3>
                </div>
                <div class="fv-row p-5 col-2">
                  <label class="form-label fs-6 fw-bolder text-dark">Extension
                    Number</label>
                  <h3 class="fs-6 fw-normal">2245</h3>
                </div>
              </div>
              <div class="d-flex flex-wrap">
                <div class="fv-row p-5 ps-0 col-4">
                  <label class="form-label fs-6 fw-bolder text-dark">Business
                    Address</label>
                  <h3 class="fs-6 fw-normal">Above Kitchen Park, Block 13 Street 24,
                    Salhiya
                    St, Kuwait City, Kuwait</h3>
                </div>
                <div class="fv-row p-5 col-3">
                  <label class="form-label fs-6 fw-bolder text-dark">Licence Expiry
                    Date</label>
                  <h3 class="fs-6 fw-normal">12-12-2026</h3>
                </div>
                <div class="fv-row p-5 col-3">
                  <label class="form-label fs-6 fw-bolder text-dark">Authorized Signatory Expiry Date</label>
                  <h3 class="fs-6 fw-normal">12-12-2026</h3>
                </div>
              </div>

              <div class="d-flex flex-wrap">
                <div class="card_body_highlited_text mb-10 col-md-6">
                  <span class="text-dark fs-3 fw-bolder">Licence</span>
                  <div class="fv-row col-5 mt-3">

                    <img data-bs-toggle="modal" class="cursor-pointer" data-bs-target="#kt_modal_2"
                      src="../assets/media/logos/business_reg_1.png" style="height: 444px; width: 320px;" alt="">

                    <!-- <img src="../assets/media/logos/business_reg_1.png" alt=""> -->
                  </div>
                </div>

                <div class="card_body_highlited_text mb-10 col-md-6">
                  <span class="text-dark fs-3 fw-bolder">Authorized Signatory</span>
                  <div class="fv-row col-5 mt-3">

                    <img data-bs-toggle="modal" class="cursor-pointer" data-bs-target="#kt_modal_1"
                      src="../assets/media/logos/business_reg_2.png" style="height: 444px; width: 320px;" alt="">
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="membership_management">
          <div class="table-responsive">
            <div class="card-body bg-white pt-8">
              <div class="card_body_highlited_text mb-10 d-flex justify-content-between align-items-center">
                <div class="card_body_head_text">
                  <h3 class="text-dark fs-3 fw-bolder">Membership - Free</h3>

                </div>
                <div class="business_membership_manag_btn">
                  <a href="index.php" class="btn btn-primary text-white" style="margin-right: 10px;">Print Report</a>
                  <a href="index.php" class="btn btn-primary text-white">Upgrade
                    Membership</a>
                </div>
              </div>
              <div class="separator border-4 my-10"></div>
              <h3 class="text-dark fs-3 fw-bolder">Contracts</h3>
              <div class="d-flex flex-wrap">
                <div class="fv-row p-5 col-2" style="background-color: aliceblue;margin: 5px;">
                  <label class="d-flex form-label fs-6 fw-bolder text-dark">Current
                    Contract</label>
                  <div class="separator border-4 my-5"></div>
                  <div class="total_current_cont d-flex justify-content-between">
                    <span>Total</span>
                    <span class="fw-bold">03</span>
                  </div>
                  <div class="total_current_cont d-flex justify-content-between">
                    <span>Used</span>
                    <span class="fw-bold">01</span>
                  </div>
                </div>
                <div class="fv-row p-5 col-9" style="background-color: aliceblue;margin: 5px;">
                  <label class="d-flex form-label fs-6 fw-bolder text-dark">Add
                    Contract</label>
                  <div class="separator border-4 my-5"></div>
                  <span>Number of Contracts</span>
                  <div class="contrct_table d-flex align-items-center">
                    <div class="fv-row p-5 ps-0 col-2">
                      <input type="number" class="form-control form-control-solid form-control-sm bg-white">
                    </div>
                    <div class="fv-row p-5 col-1">
                      <h3 class="fw-normal">X</h3>
                    </div>
                    <div class="fv-row p-5 col-2">
                      <h3 class="fw-normal">KWD 2.000</h3>
                    </div>
                    <div class="fv-row p-5 col-1">
                      <h3 class="fw-normal">=</h3>
                    </div>
                    <div class="fv-row p-5 col-2">
                      <h3 class="fw-normal">KWD 0.000</h3>
                    </div>
                    <div class="fv-row p-5 col-4 d-flex justify-content-end">
                      <a href="#." class="btn text-white" data-bs-toggle="modal" data-bs-target="#add_contract"
                        style="background-color: orange;">Add Contract</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="separator border-4 my-10"></div>
              <h3 class="text-dark fs-3 fw-bolder">Users</h3>
              <div class="d-flex flex-wrap">
                <div class="fv-row p-5 col-2" style="background-color: aliceblue;margin: 5px;">
                  <label class="d-flex form-label fs-6 fw-bolder text-dark">Current
                    Users</label>
                  <div class="separator border-4 my-5"></div>
                  <div class="total_current_cont d-flex justify-content-between">
                    <span>Total</span>
                    <span class="fw-bold">03</span>
                  </div>
                  <div class="total_current_cont d-flex justify-content-between">
                    <span>Used</span>
                    <span class="fw-bold">01</span>
                  </div>
                </div>
                <div class="fv-row p-5 col-9" style="background-color: aliceblue;margin: 5px;">
                  <label class="d-flex form-label fs-6 fw-bolder text-dark">Add
                    Users</label>
                  <div class="separator border-4 my-5"></div>
                  <span>Number of Users</span>
                  <div class="contrct_table d-flex align-items-center">
                    <div class="fv-row p-5 ps-0 col-2">
                      <input type="number" class="form-control form-control-solid form-control-sm bg-white">
                    </div>
                    <div class="fv-row p-5 col-1">
                      <h3 class="fw-normal">X</h3>
                    </div>
                    <div class="fv-row p-5 col-2">
                      <h3 class="fw-normal">KWD 10.000</h3>
                    </div>
                    <div class="fv-row p-5 col-1">
                      <h3 class="fw-normal">=</h3>
                    </div>
                    <div class="fv-row p-5 col-2">
                      <h3 class="fw-normal">KWD 0.000</h3>
                    </div>
                    <div class="fv-row p-5 col-4 d-flex justify-content-end">
                      <a href="#." class="btn text-white" data-bs-toggle="modal" data-bs-target="#add_contract"
                        style="background-color: orange;">Add Contract</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="user_management">
          <div class="table-responsive">
            <div>
              <div class="users_col d-flex flex-wrap align-items-center justify-content-between">
                <!--begin::Stat-->
                <div class="user_blocks d-flex">
                  <div
                    class="border border-gray-300 bg-white border-dashed border-secondary rounded min-w-125px py-3 px-4 me-6 mb-3">
                    <!--begin::Number-->
                    <div class="d-flex align-items-center">
                      <div class="fs-5 fw-bold counted" data-kt-countup="true" data-kt-countup-value="4500"
                        data-kt-countup-prefix="$" data-kt-initialized="1">Total Users</div>
                    </div>
                    <!--end::Number-->
                    <!--begin::Label-->
                    <div class="fw-semibold fs-5">4</div>
                    <!--end::Label-->
                  </div>
                  <!--end::Stat-->
                  <!--begin::Stat-->
                  <div
                    class="border border-gray-300 bg-white border-dashed border-secondary rounded min-w-125px py-3 px-4 me-6 mb-3">
                    <!--begin::Number-->
                    <div class="d-flex align-items-center">
                      <div class="fs-5 fw-bold counted" data-kt-countup="true" data-kt-countup-value="75"
                        data-kt-initialized="1">
                        Authorized
                        Person</div>
                    </div>
                    <!--end::Number-->
                    <!--begin::Label-->
                    <div class="fw-semibold fs-5">1</div>
                    <!--end::Label-->
                  </div>
                  <!--end::Stat-->
                  <!--begin::Stat-->
                  <div
                    class="border border-gray-300 bg-white border-dashed border-secondary rounded min-w-125px py-3 px-4 me-6 mb-3">
                    <!--begin::Number-->
                    <div class="d-flex align-items-center">
                      <div class="fs-5 fw-bold counted" data-kt-countup="true" data-kt-countup-value="60"
                        data-kt-countup-prefix="%" data-kt-initialized="1">Restricted Users</div>
                    </div>
                    <!--end::Number-->
                    <!--begin::Label-->
                    <div class="fw-semibold fs-5">2</div>
                    <!--end::Label-->
                  </div>
                </div>
                <div class="user_mang_btn">
                  <a href="#." class="btn btn-primary text-white" data-bs-toggle="modal"
                    data-bs-target="#create_users">Create Users</a>
                </div>
                <!--end::Stat-->
              </div>
              <div class="my-5 p-0 bg-white rounded">
                <div>
                  <div class="d-flex align-items-center justify-content-between py-4">
                    <div class="alphabet_tabs px-4">
                      <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold">
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">A</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">B</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">C</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">D</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">E</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">F</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">G</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">H</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">I</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">J</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">K</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">L</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">M</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">N</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">O</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">P</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">Q</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">R</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">S</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">T</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">U</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">V</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">W</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-kt-countup-tabs="true" data-bs-toggle="tab"
                            href="#.">X</a>
                        </li>
                        <li class="nav-item ">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">Y</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-active-primary pb-4" data-bs-toggle="tab" href="#.">Z</a>
                        </li>
                      </ul>
                    </div>
                    <div class="d-flex">
                      <form data-kt-search-element="form"
                        class="d-none d-lg-block w-75 mb-5 mb-lg-0 position-relative mx-5" autocomplete="off">
                        <input type="hidden">
                        <span
                          class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4 ">
                          <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="17.0365" y="13.1223" width="8.15546" height="2" rx="1"
                              transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                            </rect>
                            <path
                              d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                              fill="currentColor"></path>
                          </svg>
                        </span>
                        <input type="text" class="form-control form-control-solid h-40px bg-body ps-13 fs-7"
                          name="search" value="" placeholder="Search Contact" data-kt-search-element="input">
                      </form>
                      <button class="btn btn-primary btn-sm">Search</button>
                      <i class="fas fa-sort-alpha-down fa-3x text-dark mx-5"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="post" id="kt_post">
              <div id="kt_content_container">
                <div class="d-flex flex-column  ">
                  <div class="row gy-5 g-xl-8 mx-0 my-5">
                    <div class="col-xl-3 m-0 ps-0">
                      <div class="card card-xl-stretch mb-xl-8 h-100">
                        <div class="card-body p-0 mt-3">
                          <label
                            class="d-flex form-label fs-4 fw-bolder text-dark ps-3 pb-3 border-bottom-dashed border-secondary">Department
                            Management</label>
                          <ul class="department_tab nav flex-row flex-md-column mb-3 mb-md-0">
                            <li class="nav-item  me-0 w-100">
                              <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark active"
                                data-bs-toggle="tab" href="#general_dep"><i class="fas fa-folder-open fa-1x me-3"></i>
                                General Department</a>
                            </li>
                            <li class="nav-item  me-0 w-100">
                              <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark"
                                data-bs-toggle="tab" href="#hr_dep"><i class="fas fa-folder-open fa-1x me-3"></i>HR
                                Department</a>
                            </li>
                            <li class="nav-item  me-0 w-100">
                              <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark"
                                data-bs-toggle="tab" href="#sales_dep"><i
                                  class="fas fa-folder-open fa-1x me-3"></i>Sales
                                Department</a>
                            </li>
                            <li class="nav-item  me-0 w-100">
                              <a class="fs-6 fw-bold nav-link text-active-primary border-active-primary border-start border-2 text-dark"
                                data-bs-toggle="tab" href="#purchase_dep"><i
                                  class="fas fa-folder-open fa-1x me-3"></i>Purchase
                                Department</a>
                            </li>
                            <li class="nav-item  me-0 w-100">
                              <a class="fs-6 fw-bold nav-link active text-active-primary text-dark"
                                data-bs-toggle="modal" data-bs-target="#create_dept" href="#tab1_all"><i
                                  class="fas fa-folder-open fa-1x me-3"></i>Add
                                Department</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-9 p-0 mt-0">
                      <div class="card">
                        <div class="card-body">
                          <div class="tab-content" id="myTabContent_main">
                            <div class="tab-pane fade active show" id="general_dep" role="tabpanel">
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-1.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-2.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-3.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-4.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane fade" id="hr_dep" role="tabpanel">
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-5.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-6.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-7.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-8.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane fade" id="sales_dep" role="tabpanel">
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-9.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-10.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-1.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-2.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane fade" id="purchase_dep" role="tabpanel">
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-3.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-4.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-5.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                              <div class="notice row border-secondary border border-2 m-5 mt-0">
                                <div class="left_block col-2 p-5">
                                  <img src="../assets/images/150-6.jpg" alt="Emma Smith" class="w-100 rounded-circle">
                                </div>
                                <div
                                  class="center_block bg-white col-7 d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap ps-10">
                                  <div class="mb-3 mb-md-0 fw-bold">
                                    <div class="notification_block_heading">
                                      <h4 class="text-gray-900 fw-bolder text-dark">
                                        Aabel Baasiqaat Baazighah
                                      </h4>
                                      <p>General Department</p>
                                    </div>
                                    <div class="fs-6 text-gray-700 pe-7">
                                      <p class="mb-0 fw-normal text-dark">
                                        Email
                                        Address -
                                        aabel.baazighah@domain.com
                                      </p>
                                      <p class="mb-0 fw-normal">
                                        Contact Number
                                        -
                                        965 8432 1234
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="right_block bg-white col-3 pe-0">
                                  <div class="right_upr text-end">
                                    <a href="#." class="btn btn-white btn-sm text-white" data-kt-menu-trigger="click"
                                      data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"><i
                                        class="fas fa-ellipsis-h fa-lg"></i>
                                    </a>
                                    <div
                                      class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px"
                                      data-kt-menu="true">
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Profile</a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">
                                          <span class="menu-text">My Projects</span>
                                          </span>
                                        </a>
                                      </div>
                                      <!--end::Menu item-->
                                      <!--begin::Menu item-->
                                      <div class="menu-item px-5">
                                        <a href="#." class="menu-link px-5">My Statements</a>
                                      </div>
                                      <!--end::Menu item-->
                                    </div>
                                  </div>
                                  <div class="right_lower mt-10">
                                    <p class="mb-0 fs-6 fw-normal">Last
                                      Login
                                      01-12-2022</p>
                                    <p class="mb-0 fs-6 fw-normal">
                                      11:12:59</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="kt_modal_1" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="">
      <div class="modal-body">
        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2 d-flex justify-content-end"
          data-bs-dismiss="modal" aria-label="Close">
          <i class="fas fa-times text-dark fs-1"></i>
        </div>
        <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
          <img class="border border-2" src="../assets/images/business_reg_2.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="create_dept" tabindex="-1" style="display: none;" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog modal-dialog-centered mw-900px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2>Department Management</h2>
        <!--end::Modal title-->
        <!--begin::Close-->
        <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
          <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
          <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
              <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)"
                fill="black"></rect>
              <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black">
              </rect>
            </svg>
          </span>
          <!--end::Svg Icon-->
        </div>
        <!--end::Close-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body py-lg-10 px-lg-10">
        <!--begin::Stepper-->
        <div class="d-flex flex-wrap">
          <div class="fv-row p-5 col-12">
            <label class="form-label fs-6 fw-bolder text-dark">Department Name</label>
            <input type="text" class="form-control" placeholder="Enter Civil ID">
          </div>
        </div>
        <div class="popup_create_folder_btn d-flex justify-content-end">
          <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_contract">Create
            Department</a>
        </div>

        <!--end::Stepper-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
<div class="modal fade" id="create_users" tabindex="-1" style="display: none;" aria-hidden="true">
  <!--begin::Modal dialog-->
  <div class="modal-dialog modal-dialog-centered mw-900px">
    <!--begin::Modal content-->
    <div class="modal-content">
      <!--begin::Modal header-->
      <div class="modal-header">
        <!--begin::Modal title-->
        <h2>Create users</h2>
        <!--end::Modal title-->
        <!--begin::Close-->
        <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
          <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
          <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
              <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)"
                fill="black"></rect>
              <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black">
              </rect>
            </svg>
          </span>
          <!--end::Svg Icon-->
        </div>
        <!--end::Close-->
      </div>
      <!--end::Modal header-->
      <!--begin::Modal body-->
      <div class="modal-body py-lg-10 px-lg-10">
        <!--begin::Stepper-->
        <div class="d-flex flex-wrap">
          <div class="fv-row p-5 col-12">
            <label class="form-label fs-6 fw-bolder text-dark">Civil ID</label>
            <input type="text" class="form-control" placeholder="Enter Civil ID">
          </div>
        </div>
        <div class="d-flex flex-wrap">
          <div class="fv-row p-5 col-6">
            <label class="form-label fs-6 fw-bolder text-dark">Email Address</label>
            <input type="text" class="form-control" placeholder="Enter Email Address">
          </div>
          <div class="fv-row p-5 col-6">
            <label class="form-label fs-6 fw-bolder text-dark">Select Department</label>
            <select class="form-select" aria-label="Select example">
              <option>Select Department</option>
              <option value="1">General Depatment</option>
              <option value="2">HR Depatment</option>
              <option value="3">Sales Depatment</option>
              <option value="3">Purchase Depatment</option>
            </select>
          </div>
        </div>
        <div class="popup_create_folder_btn d-flex justify-content-end">
          <a href="#." class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#create_contract">Create
            Usres</a>
        </div>

        <!--end::Stepper-->
      </div>
      <!--end::Modal body-->
    </div>
    <!--end::Modal content-->
  </div>
  <!--end::Modal dialog-->
</div>
<div class="modal fade" id="add_contract" aria-hidden="true" aria-labelledby="exampleModalToggle7" tabindex="-1"
  data-bs-dismiss="modal">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div class=" p-20">
          <form action="">

            <h3 class="text-primary">Additional contracts</h3>

            <div class="row mt-4 mb-7">
              <div class="col-9">
                <p>1 Additional Contract</p>
              </div>
              <div class="col-3">
                KWD 2.000
              </div>
            </div>
            <div class="fs-6 text-danger mb-2"><a style="background-color: rgba(255, 184, 70, 0.612);"
                class="p-2 text-danger rounded-pill"> Enter Coupon Code</a>
            </div>
            <div class="row text-danger">
              <div class="col-9">
                <p>No Code</p>
              </div>
              <div class="col-3">
                KWD 00.000
              </div>
            </div>
            <div class="row mt-4 mb-2 border-bottom border-dark">
              <div class="col-9">
                <p>My Fatoorah Knet Charges</p>
              </div>
              <div class="col-3">
                KWD 00.100
              </div>
            </div>
            <div class="row my-5" style="border-bottom-style: dashed;">
              <div class="col-9">
                <p>Total</p>
              </div>
              <div class="col-3">
                KWD 02.100
              </div>
            </div>

            <a class="btn btn-primary mt-15 mb-5 w-100 fw-bold" type="button" href="free_payment_screen.php">Submit</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="kt_modal_2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="">
      <div class="modal-body">
        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2 d-flex justify-content-end"
          data-bs-dismiss="modal" aria-label="Close">
          <i class="fas fa-times text-dark fs-1"></i>
        </div>
        <div class="d-flex justify-content-center align-center w-100 m-auto text-center">
          <img class="border border-2" src="../assets/images/business_reg_1.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</div>
<!--end::Root-->



<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
  <span class="svg-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
      <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
      <path
        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
        fill="black" />
    </svg>
  </span>
</div>

<script>
  $(document).ready(function () {

    $('.image-popup-vertical-fit').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      mainClass: 'mfp-img-mobile',
      image: {
        verticalFit: true
      }

    });

    $('.image-popup-fit-width').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      image: {
        verticalFit: false
      }
    });

    $('.image-popup-no-margins').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      closeBtnInside: false,
      fixedContentPos: true,
      mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
      image: {
        verticalFit: true
      },
      zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS
      }
    });

  });
</script>



<script>
  ClassicEditor
    .create(document.querySelector('#ckeditor_1'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_2'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_3'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
</script>
<script>
  $("#business-tab").click(function () {
    $("div#myTabContent1").hide();
    $("div#myTabContent2").show();
  });
  $("#individual-tab").click(function () {
    $("div#myTabContent1").show();
    $("div#myTabContent2").hide();
  });
  $(document).ready(function () {
    $('#addresstype').on('change', function () {
      var demovalue = $(this).val();
      $("div.myDiv").hide();
      $("#show" + demovalue).show();
    });
    $('#addresstype1').on('change', function () {
      var demovalue1 = $(this).val();
      $("div.myDiv1").hide();
      $("#show" + demovalue1).show();
    });
  });
  // var profileborder = "border-danger";
  $(".userprofile").addClass("border-danger");
  // function changeuserborder() {
  //     $(".userprofile").removeClass(profileborder);
  //     var profileborder = "border-success";
  // }
  $("#userheaderchange").click(function () {
    $(".userprofile").removeClass("border-danger");
    $(".userprofile").addClass("border-success");
    $("#headererror").addClass("d-none");

  });
</script>
<script>
  $.fn.equalHeights = function () {
    var max_height = 0;
    $(this).each(function () {
      max_height = Math.max($(this).height(), max_height);
    });
    $(this).each(function () {
      $(this).height(max_height);
    });
  };

  $(document).ready(function () {
    $('.userdasboardbox ul li a .card').equalHeights();
  });
</script>


<?php include("footer.php") ?>