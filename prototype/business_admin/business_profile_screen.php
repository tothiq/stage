<!DOCTYPE php>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tothiq - Digital Contracts Platform</title>
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <!--end::Fonts-->
        <!--begin::Page Vendor Stylesheets(used by this page)-->
        <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Page Vendor Stylesheets-->
        <!--begin::Global Stylesheets Bundle(used by all pages)-->
        <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/bu_style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Global Stylesheets Bundle-->
        <link href="../assets/css/bu_media.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/magnific-popup.css">
        <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    </head>

    <!--begin::Body-->

    <body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
        <!--begin::Main-->
        <!--begin::Root-->
        <div class="d-flex flex-column flex-root">
            <!--begin::Page-->
            <div class="page d-flex flex-row">
                <!--begin::Aside-->
                <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                    <!--begin::Brand-->
                    <div class="aside-logo flex-column-auto h-90px" id="kt_aside_logo">
                        <!--begin::Logo-->
                        <a href="dashboard.php">
                            <img alt="Logo" src="../assets/media/logos/logo.png" class="w-60px">
                            <!-- <p class=" mb-0 pt-3 fs-5 fw-bolder text-dark">Digital Contracts Platform</p> -->
                        </a>
                        <!--end::Logo-->
                        <!--begin::Aside toggler-->
                        <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
                            <span class="svg-icon svg-icon-1 rotate-180">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="black"></path>
                                    <path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="black"></path>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                        <!--end::Aside toggler-->
                    </div>
                    <!--end::Brand-->
                    <!--begin::Aside menu-->
                    <div class="aside-menu flex-column-fluid">
                        <!--begin::Aside Menu-->
                        <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0" style="height: 350px;">
                            <!--begin::Menu-->
                            <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
                                <div class="menu-item">
                                    <a class="menu-link" href="dashboard.php">
                                        <span class="menu-icon">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                                <defs>
                                                    <style>
                                                        .cls-1 {
                                                            fill: #183052;
                                                        }
                                                    </style>
                                                </defs>
                                                <path class="cls-1" d="m65.28,241.34c-6.65,5.52-12.12,10.33-17.89,14.76-6.06,4.66-12.57,8.43-20.74,5.99-8.39-2.51-12.32-8.93-13.85-16.9-1.42-7.42,2.19-13.22,7.61-17.82,32.59-27.62,65.25-55.16,97.89-82.72,39.09-33.01,78.18-66.01,117.26-99.03,10.65-9,19.38-9.09,29.91-.17,71.35,60.5,142.68,121,214.02,181.51,12.67,10.74,9.87,29.51-5.23,34.97-7.77,2.81-13.94-.92-19.75-5.25-6.27-4.67-12.29-9.68-19.45-15.36v6.9c0,47.7.11,95.4-.06,143.09-.05,14.47-2.78,28.39-11.09,40.75-11.13,16.55-26.72,25.78-46.37,27.96-8.17.91-16.46,1.06-24.69,1.07-72.66.08-145.32.14-217.98-.03-17.45-.04-33.64-4.3-47.25-16.25-12.48-10.95-19.36-24.67-21.19-40.91-.92-8.17-1.07-16.45-1.09-24.69-.1-43.48-.05-86.96-.05-130.44v-7.46Zm105.71,180.06v-7.15c0-38.17-.02-76.34,0-114.5.01-22.87,13.76-36.54,36.75-36.55,28.26-.02,56.54.58,84.78-.19,21.71-.59,37.43,16.22,37.02,37.23-.74,38.15-.21,76.33-.21,114.5v7c12.45,0,24.32,0,36.2,0,20.44-.01,30.09-9.6,30.1-29.94,0-59.45-.04-118.91.09-178.36,0-4.23-1.22-7.01-4.49-9.77-34.18-28.91-68.21-58-102.29-87.03-12.82-10.92-25.66-21.83-38.63-32.86-1.16.86-2.05,1.45-2.86,2.14-46.51,39.61-93.05,79.2-139.43,118.96-1.86,1.59-3.19,4.89-3.2,7.4-.18,60.19-.14,120.37-.13,180.56,0,2.02,0,4.05.22,6.05,1.29,11.68,9.89,21.56,21.57,22.3,14.57.92,29.25.22,44.5.22Zm39.8-118.61v118.56h78.75v-118.56h-78.75Z">
                                                </path>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                        <span class="menu-title fs-5 ms-2">Home</span>
                                    </a>
                                </div>

                                <!-- <div class="menu-item">
                                    <a class="menu-link" href="profile&amp;activation.php">
                                        <span class="menu-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                                <defs>
                                                    <style>
                                                        .cls-1 {
                                                            fill: #183052;
                                                        }
                                                    </style>
                                                </defs>
                                                <path d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"></path>
                                            </svg>
                                        </span>
                                        <span class="menu-title fs-5 ms-2 ">Profile</span>
                                    </a>
                                </div> -->

                                <div class="menu-item">
                                    <a class="menu-link" href="#.">
                                        <span class="menu-icon">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                                <path d="M506.1 127.1c-17.97-20.17-61.46-61.65-122.7-71.1c-22.5-3.354-45.39 3.606-63.41 18.21C302 60.47 279.1 53.42 256.5 56.86C176.8 69.17 126.7 136.2 124.6 139.1c-7.844 10.69-5.531 25.72 5.125 33.57c4.281 3.157 9.281 4.657 14.19 4.657c7.406 0 14.69-3.375 19.38-9.782c.4062-.5626 40.19-53.91 100.5-63.23c7.457-.9611 14.98 .67 21.56 4.483L227.2 168.2C214.8 180.5 207.1 196.1 207.1 214.5c0 17.5 6.812 33.94 19.16 46.29C239.5 273.2 255.9 279.1 273.4 279.1s33.94-6.813 46.31-19.19l11.35-11.35l124.2 100.9c2.312 1.875 2.656 5.251 .5 7.97l-27.69 35.75c-1.844 2.25-5.25 2.594-7.156 1.063l-22.22-18.69l-26.19 27.75c-2.344 2.875-5.344 3.563-6.906 3.719c-1.656 .1562-4.562 .125-6.812-1.719l-32.41-27.66L310.7 392.3l-2.812 2.938c-5.844 7.157-14.09 11.66-23.28 12.6c-9.469 .8126-18.25-1.75-24.5-6.782L170.3 319.8H96V128.3L0 128.3v255.6l64 .0404c11.74 0 21.57-6.706 27.14-16.14h60.64l77.06 69.66C243.7 449.6 261.9 456 280.8 456c2.875 0 5.781-.125 8.656-.4376c13.62-1.406 26.41-6.063 37.47-13.5l.9062 .8126c12.03 9.876 27.28 14.41 42.69 12.78c13.19-1.375 25.28-7.032 33.91-15.35c21.09 8.188 46.09 2.344 61.25-16.47l27.69-35.75c18.47-22.82 14.97-56.48-7.844-75.01l-120.3-97.76l8.381-8.382c9.375-9.376 9.375-24.57 0-33.94c-9.375-9.376-24.56-9.376-33.94 0L285.8 226.8C279.2 233.5 267.7 233.5 261.1 226.8c-3.312-3.282-5.125-7.657-5.125-12.31c0-4.688 1.812-9.064 5.281-12.53l85.91-87.64c7.812-7.845 18.53-11.75 28.94-10.03c59.75 9.22 100.2 62.73 100.6 63.29c3.088 4.155 7.264 6.946 11.84 8.376H544v175.1c0 17.67 14.33 32.05 31.1 32.05L640 384V128.1L506.1 127.1zM48 352c-8.75 0-16-7.245-16-15.99c0-8.876 7.25-15.99 16-15.99S64 327.2 64 336.1C64 344.8 56.75 352 48 352zM592 352c-8.75 0-16-7.245-16-15.99c0-8.876 7.25-15.99 16-15.99s16 7.117 16 15.99C608 344.8 600.8 352 592 352z" />
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                        <span class="menu-title fs-5 ms-2">Support</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link" href="business_screen.php">
                                        <span class="menu-icon">

                                            <span class="menu-icon">

                                                <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                                    <defs>
                                                        <style>
                                                            .cls-1 {
                                                                fill: #1e304f;
                                                            }
                                                        </style>
                                                    </defs>
                                                    <path class="cls-1" d="m197.27,78.37h-5.73c-40.87,0-81.74,0-122.61,0-12.12,0-17.06,4.91-17.06,16.93,0,103.18,0,206.35,0,309.53,0,12.53,4.32,16.87,16.82,16.87,83.75,0,167.51,0,251.26,0h6.54c-3.33,17.96-22.95,36.14-41.2,38.29-5.62.66-11.32,1.02-16.98,1.02-65.43.07-130.85.03-196.28.05-11.5,0-22.6-1.58-32.73-7.51-14.9-8.71-23.5-21.68-25.71-38.7-.84-6.5-1.03-13.13-1.04-19.7-.06-99.69-.07-199.39-.02-299.08,0-13.5,3.24-26.1,11.86-36.87,10.95-13.66,25.68-19.98,42.72-20.09,48.56-.32,97.13-.2,145.7-.06,15.85.05,29.56,5.51,40.88,17.03,19.14,19.47,38.46,38.78,57.92,57.93,11.78,11.59,17.32,25.54,17.37,41.85.06,16.49.05,32.99-.04,49.48,0,1.72-.17,4.01-1.23,5.05-12.5,12.29-25.19,24.39-38.71,37.39v-77.05c-2.45,0-4.38,0-6.32,0-18.51,0-37.02.02-55.53,0-19.84-.03-29.85-9.96-29.89-29.67-.04-18.69,0-37.38,0-56.07,0-1.98,0-3.96,0-6.63Z">
                                                    </path>
                                                    <path class="cls-1" d="m384.23,192.33c18.52,18.51,36.88,36.87,55.47,55.45-1.09,1.16-2.3,2.52-3.58,3.8-40.52,40.83-81.11,81.6-121.52,122.54-5.41,5.48-11.29,8.01-19.01,7.87-17.77-.33-35.54-.15-53.31-.09-15.09.05-27.34-4.92-35.8-18.25-6.12-9.64-13.75-8.82-19.36,1.44-1.93,3.53-3.75,7.13-5.9,10.53-5.23,8.27-16.39,8.32-21.01-.29-2.99-5.58-4.73-11.85-6.86-17.87-2.75-7.75-5.32-15.56-7.99-23.34-.11-.33-.4-.59-1.31-1.86-1.19,3.37-2.12,6.15-3.16,8.89-3.92,10.36-6.28,21.32-14.31,29.9-9.84,10.53-29.8,12.87-40.36,9.48-4.72-1.51-8.06-7.17-7.54-12.26.56-5.49,3.07-9.98,8.7-11.42,3.66-.93,7.59-.98,11.4-1.08,7.73-.2,10.29-1.91,12.81-9.41,4.44-13.17,8.72-26.4,13.23-39.55,4.9-14.25,17.33-19.84,29.26-13.29,5.52,3.03,8.04,8.31,9.98,13.92,3.42,9.85,6.76,19.71,10.17,29.56.1.29.5.49,1.2,1.14,7.59-5.29,16.19-7.81,25.63-6.62,9.84,1.24,19.22,4.73,23.96,13.56,5.74,10.69,13.97,12.08,25,9.98,0-6.8.12-13.87-.04-20.95-.13-5.84,1.77-10.69,5.92-14.81,42.02-41.67,84-83.37,126-125.05.77-.76,1.65-1.4,2.29-1.94Z">
                                                    </path>
                                                    <path class="cls-1" d="m459.07,228.76c-18.72-18.76-37.06-37.14-55.83-55.94,7.65-7.21,15.2-15.26,23.7-22.12,10.19-8.23,20.2-7.07,30.02,2.27,7.43,7.07,14.76,14.25,22.1,21.42,10.23,9.99,11.33,19.6,2.35,30.54-6.94,8.45-14.95,16.03-22.34,23.84Z">
                                                    </path>
                                                </svg>

                                            </span>

                                        </span>
                                        <span class="menu-title fs-5 ms-2">Company Profile</span>
                                    </a>
                                </div>
                                <div class="menu-item">
                                    <a class="menu-link" href="sign_in.php">
                                        <span class="menu-icon">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><!--! Font Awesome Pro 6.2.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                                <path d="M534.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-128-128c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L434.7 224 224 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l210.7 0-73.4 73.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l128-128zM192 96c17.7 0 32-14.3 32-32s-14.3-32-32-32l-64 0c-53 0-96 43-96 96l0 256c0 53 43 96 96 96l64 0c17.7 0 32-14.3 32-32s-14.3-32-32-32l-64 0c-17.7 0-32-14.3-32-32l0-256c0-17.7 14.3-32 32-32l64 0z" />
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                        <span class="menu-title fs-5 ms-2">Log Out</span>
                                    </a>
                                </div>
                            </div>
                            <!--end::Menu-->
                        </div>

                    </div>

                    <div class="aside-footer flex-column-auto pt-4 pb-4 menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="kt_aside_footer">
                        <div class="menu-item">
                            <a class="menu-link pe-none justify-content-center">
                                <span class="menu-icon">
                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px position-relative show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                        <img src="../assets/images/ms logo.jfif" alt="" class="h-40x w-40px rounded-circle">

                                    </div>
                                </span>
                                <div class="align-items-center ms-2">
                                    <div class="fw-bolder d-flex align-items-center fs-5  cursor-default">Tothiq User
                                    </div>
                                    <p class="fw-bold text-muted text-hover-primary fs-7 mb-0 ">Micro Solutions Kuwait</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div id="kt_header" class="header justify-content-between align-items-stretch">
                    <div class="d-flex align-items-center  pe-0">
                        <div class="header_icon mx-5">
                            <i class="fas fa-clock fa-lg"></i>
                        </div>
                        <div class="header_text">
                            <h3 class="fs-6 fw-bold text-gray-500 mb-0">Your account isn't active yet. Please fill out
                                the necessary information, send in the necessary documents, and verify Hawati</h3>
                        </div>
                    </div>
                    <div class="menu-item d-flex align-items-center">
                        <a class="menu-link" href="notification.php">
                            <span class="menu-icon">
                                <span class="menu-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                        <defs>
                                            <style>
                                                .cls-1 {
                                                    fill: #183052;
                                                }
                                            </style>
                                        </defs>
                                        <path class="cls-1" d="m219.83,57.41c.42-6.52.45-14.16,1.51-21.66,1.95-13.74,13.96-23.25,28.49-23.25,14.53,0,26.54,9.52,28.49,23.25,1.06,7.5,1.09,15.14,1.6,22.99,7.39,2.08,15.03,3.74,22.32,6.37,51.87,18.71,83.3,55.33,93.24,109.49,2.73,14.85,2.55,30.35,2.6,45.56.09,30.07,4.87,59.04,17.56,86.47,9.08,19.62,21.89,36.76,35.33,53.51,9.84,12.26,9.22,25.33-2.62,33.25-4.92,3.29-11.9,4.89-17.95,4.91-102.06.29-204.12.21-306.18.21-19.18,0-38.35.13-57.52-.05-14.75-.14-25.85-10.36-24.43-23.46.58-5.32,3.64-10.92,7-15.29,13.71-17.83,27.3-35.59,36.36-56.47,10.66-24.58,15.28-50.2,15.95-76.87.48-19.1.36-38.51,3.8-57.18,7.92-42.97,32.95-74.55,71.11-95.05,13.26-7.12,28.25-11.01,43.33-16.71Zm171.25,296.44c-2.49-4.18-4.38-7.34-6.26-10.52-17.99-30.54-28.18-63.48-29.94-98.9-.97-19.55-.3-39.25-2.41-58.66-3.82-35.06-22.42-60.68-54.51-75.35-25.47-11.64-52.54-12.14-79.27-5.63-36.78,8.96-59.93,32.61-69.63,69.37-4.39,16.62-3.26,33.56-3.29,50.42-.05,37.67-7.29,73.68-24.57,107.36-3.72,7.25-8.01,14.22-12.37,21.91h282.24Z" />
                                        <path class="cls-1" d="m308.81,428.92c-.55,33.65-28.35,58.34-58.65,58.58-29.66.23-58.64-23.89-59.3-58.58h117.95Z" />
                                    </svg>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <div id="kt_app_toolbar" class="app-toolbar py-8">
                    <!--begin::Toolbar container-->
                    <div id="kt_app_toolbar_container" class="app-container px-5 d-flex flex-stack">
                        <!--begin::Page title-->
                        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                            <!--begin::Title-->
                            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                                Business Panel</h1>
                            <!--end::Title-->
                        </div>
                        <!--end::Page title-->
                    </div>
                    <!--end::Toolbar container-->
                </div>
                <div class="content_heading p-5">
                    <h3 class="fs-3 fw-bold text-primary">Business Profile</h3>
                </div>
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container">
                        <div class="card m-5 mt-5">
                            <div class="card-body">
                                <div class="card_body_highlited_text d-flex justify-content-between align-items-center">
                                    <span class="text-dark fs-3 fw-bolder">Business Information</span>
                                    <a href="business_screen.php" class="btn btn-warning text-dark">Submit</a>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 ps-0 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Name</label>
                                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="Tothiq">
                                    </div>
                                    <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Email Address</label>
                                        <input class="form-control form-control-lg form-control-solid" type="email" placeholder="webinfo@mskuwait.com">
                                    </div>
                                    <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Contact Number</label>
                                        <input class="form-control form-control-lg form-control-solid" type="number" placeholder="+965-1234 1234">
                                    </div>
                                    <div class="fv-row p-5 col-2">
                                        <label class="form-label fs-6 fw-bolder text-dark">Extension Number</label>
                                        <input class="form-control form-control-lg form-control-solid" type="number" placeholder="2245" maxlength="4">
                                    </div>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 ps-0 col-4">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Address</label>
                                        <textarea class="form-control form-control-solid" placeholder="Above Kitchen Park, Block 13 Street 24, Salhiya St, Kuwait City, Kuwait" id="floatingTextarea2" style="height: 100px;"></textarea>
                                    </div>
                                    <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Licence Expiry Date</label>
                                        <input class="form-control form-control-lg form-control-solid" type="date" placeholder="12-12-2026">
                                    </div>
                                    <div class="fv-row p-5 col-3">
                                        <label class="form-label fs-6 fw-bolder text-dark">Licence Number</label>
                                        <input class="form-control form-control-lg form-control-solid" type="input" placeholder="123-123-12ASD-2345">
                                    </div>
                                </div>
                                <div class="card_body_highlited_text mb-10">
                                    <span class="text-dark fs-3 fw-bolder">Business Registration Document</span>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="fv-row p-5 ps-0 col-6">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Registration Document
                                            <a href="#."><i class="fas fa-camera fa-lg px-5"></i></a>
                                            <a href="#."><i class="fas fa-upload fa-lg"></i></a>
                                        </label>
                                        <img src="../assets/media/logos/business_reg_1.png" alt="">
                                    </div>
                                    <div class="fv-row p-5 ps-0 col-6">
                                        <label class="form-label fs-6 fw-bolder text-dark">Authorized Signatory Certificate
                                            <a href="#."><i class="fas fa-camera fa-lg px-5"></i></a>
                                            <a href="#."><i class="fas fa-upload fa-lg"></i></a>
                                        </label>
                                        <img src="../assets/media/logos/business_reg_2.png" alt="">
                                    </div>
                                    <div class="fv-row p-5 col-6">
                                        <label class="form-label fs-6 fw-bolder text-dark">Business Registration Document
                                            Expiry Date</label>
                                        <input class="form-control form-control-lg form-control-solid w-50" type="date" placeholder="12-12-2026">
                                    </div>
                                    <div class="fv-row p-5 col-6">
                                        <label class="form-label fs-6 fw-bolder text-dark">Authorized Signatory Certificate
                                            Expiry Date</label>
                                        <input class="form-control form-control-lg form-control-solid w-50" type="date" placeholder="12-12-2026">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--begin::Scrolltop-->
                <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
                    <span class="svg-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
                            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
                        </svg>
                    </span>
                </div>

                <script>
                    ClassicEditor
                        .create(document.querySelector('#ckeditor_1'))
                        .then(editor => {
                            console.log(editor);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                    ClassicEditor
                        .create(document.querySelector('#ckeditor_2'))
                        .then(editor => {
                            console.log(editor);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                    ClassicEditor
                        .create(document.querySelector('#ckeditor_3'))
                        .then(editor => {
                            console.log(editor);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                </script>
                <script>
                    $("#business-tab").click(function() {
                        $("div#myTabContent1").hide();
                        $("div#myTabContent2").show();
                    });
                    $("#individual-tab").click(function() {
                        $("div#myTabContent1").show();
                        $("div#myTabContent2").hide();
                    });
                    $(document).ready(function() {
                        $('#addresstype').on('change', function() {
                            var demovalue = $(this).val();
                            $("div.myDiv").hide();
                            $("#show" + demovalue).show();
                        });
                        $('#addresstype1').on('change', function() {
                            var demovalue1 = $(this).val();
                            $("div.myDiv1").hide();
                            $("#show" + demovalue1).show();
                        });
                    });
                    // var profileborder = "border-danger";
                    $(".userprofile").addClass("border-danger");
                    // function changeuserborder() {
                    //     $(".userprofile").removeClass(profileborder);
                    //     var profileborder = "border-success";
                    // }
                    $("#userheaderchange").click(function() {
                        $(".userprofile").removeClass("border-danger");
                        $(".userprofile").addClass("border-success");
                        $("#headererror").addClass("d-none");

                    });
                </script>
                <script>
                    $.fn.equalHeights = function() {
                        var max_height = 0;
                        $(this).each(function() {
                            max_height = Math.max($(this).height(), max_height);
                        });
                        $(this).each(function() {
                            $(this).height(max_height);
                        });
                    };

                    $(document).ready(function() {
                        $('.userdasboardbox ul li a .card').equalHeights();
                    });
                </script>
                <!--begin::footer-->
                <div class="footer py-4 d-flex flex-lg-column app-footer position-fixed bottom-0 end-0 w-100" id="kt_footer">
                    <!--begin::Container-->
                    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-end">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-bold me-1">© 2023</span>
                            <a href="#" target="_blank" class="text-gray-800 text-hover-primary">Tothiq Web Design Company. All Right Reserved.</a>
                        </div>
                        <!--end::Copyright-->
                    </div>
                    <!--end::Container-->
                </div>
                <div class="footer py-4 d-flex flex-lg-column d-lg-none position-fixed bottom-0 w-100 justify-content-center" id="kt_icons_footer">
                    <div>
                        <div class="social-media hidden-xs mb-5">
                            <li>
                                <a href="dashboard.php">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        fill: #183052;
                                                    }
                                                </style>
                                            </defs>
                                            <path class="cls-1" d="m65.28,241.34c-6.65,5.52-12.12,10.33-17.89,14.76-6.06,4.66-12.57,8.43-20.74,5.99-8.39-2.51-12.32-8.93-13.85-16.9-1.42-7.42,2.19-13.22,7.61-17.82,32.59-27.62,65.25-55.16,97.89-82.72,39.09-33.01,78.18-66.01,117.26-99.03,10.65-9,19.38-9.09,29.91-.17,71.35,60.5,142.68,121,214.02,181.51,12.67,10.74,9.87,29.51-5.23,34.97-7.77,2.81-13.94-.92-19.75-5.25-6.27-4.67-12.29-9.68-19.45-15.36v6.9c0,47.7.11,95.4-.06,143.09-.05,14.47-2.78,28.39-11.09,40.75-11.13,16.55-26.72,25.78-46.37,27.96-8.17.91-16.46,1.06-24.69,1.07-72.66.08-145.32.14-217.98-.03-17.45-.04-33.64-4.3-47.25-16.25-12.48-10.95-19.36-24.67-21.19-40.91-.92-8.17-1.07-16.45-1.09-24.69-.1-43.48-.05-86.96-.05-130.44v-7.46Zm105.71,180.06v-7.15c0-38.17-.02-76.34,0-114.5.01-22.87,13.76-36.54,36.75-36.55,28.26-.02,56.54.58,84.78-.19,21.71-.59,37.43,16.22,37.02,37.23-.74,38.15-.21,76.33-.21,114.5v7c12.45,0,24.32,0,36.2,0,20.44-.01,30.09-9.6,30.1-29.94,0-59.45-.04-118.91.09-178.36,0-4.23-1.22-7.01-4.49-9.77-34.18-28.91-68.21-58-102.29-87.03-12.82-10.92-25.66-21.83-38.63-32.86-1.16.86-2.05,1.45-2.86,2.14-46.51,39.61-93.05,79.2-139.43,118.96-1.86,1.59-3.19,4.89-3.2,7.4-.18,60.19-.14,120.37-.13,180.56,0,2.02,0,4.05.22,6.05,1.29,11.68,9.89,21.56,21.57,22.3,14.57.92,29.25.22,44.5.22Zm39.8-118.61v118.56h78.75v-118.56h-78.75Z">
                                            </path>
                                        </svg>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="contract.php">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        fill: #1e304f;
                                                    }
                                                </style>
                                            </defs>
                                            <path class="cls-1" d="m197.27,78.37h-5.73c-40.87,0-81.74,0-122.61,0-12.12,0-17.06,4.91-17.06,16.93,0,103.18,0,206.35,0,309.53,0,12.53,4.32,16.87,16.82,16.87,83.75,0,167.51,0,251.26,0h6.54c-3.33,17.96-22.95,36.14-41.2,38.29-5.62.66-11.32,1.02-16.98,1.02-65.43.07-130.85.03-196.28.05-11.5,0-22.6-1.58-32.73-7.51-14.9-8.71-23.5-21.68-25.71-38.7-.84-6.5-1.03-13.13-1.04-19.7-.06-99.69-.07-199.39-.02-299.08,0-13.5,3.24-26.1,11.86-36.87,10.95-13.66,25.68-19.98,42.72-20.09,48.56-.32,97.13-.2,145.7-.06,15.85.05,29.56,5.51,40.88,17.03,19.14,19.47,38.46,38.78,57.92,57.93,11.78,11.59,17.32,25.54,17.37,41.85.06,16.49.05,32.99-.04,49.48,0,1.72-.17,4.01-1.23,5.05-12.5,12.29-25.19,24.39-38.71,37.39v-77.05c-2.45,0-4.38,0-6.32,0-18.51,0-37.02.02-55.53,0-19.84-.03-29.85-9.96-29.89-29.67-.04-18.69,0-37.38,0-56.07,0-1.98,0-3.96,0-6.63Z">
                                            </path>
                                            <path class="cls-1" d="m384.23,192.33c18.52,18.51,36.88,36.87,55.47,55.45-1.09,1.16-2.3,2.52-3.58,3.8-40.52,40.83-81.11,81.6-121.52,122.54-5.41,5.48-11.29,8.01-19.01,7.87-17.77-.33-35.54-.15-53.31-.09-15.09.05-27.34-4.92-35.8-18.25-6.12-9.64-13.75-8.82-19.36,1.44-1.93,3.53-3.75,7.13-5.9,10.53-5.23,8.27-16.39,8.32-21.01-.29-2.99-5.58-4.73-11.85-6.86-17.87-2.75-7.75-5.32-15.56-7.99-23.34-.11-.33-.4-.59-1.31-1.86-1.19,3.37-2.12,6.15-3.16,8.89-3.92,10.36-6.28,21.32-14.31,29.9-9.84,10.53-29.8,12.87-40.36,9.48-4.72-1.51-8.06-7.17-7.54-12.26.56-5.49,3.07-9.98,8.7-11.42,3.66-.93,7.59-.98,11.4-1.08,7.73-.2,10.29-1.91,12.81-9.41,4.44-13.17,8.72-26.4,13.23-39.55,4.9-14.25,17.33-19.84,29.26-13.29,5.52,3.03,8.04,8.31,9.98,13.92,3.42,9.85,6.76,19.71,10.17,29.56.1.29.5.49,1.2,1.14,7.59-5.29,16.19-7.81,25.63-6.62,9.84,1.24,19.22,4.73,23.96,13.56,5.74,10.69,13.97,12.08,25,9.98,0-6.8.12-13.87-.04-20.95-.13-5.84,1.77-10.69,5.92-14.81,42.02-41.67,84-83.37,126-125.05.77-.76,1.65-1.4,2.29-1.94Z">
                                            </path>
                                            <path class="cls-1" d="m459.07,228.76c-18.72-18.76-37.06-37.14-55.83-55.94,7.65-7.21,15.2-15.26,23.7-22.12,10.19-8.23,20.2-7.07,30.02,2.27,7.43,7.07,14.76,14.25,22.1,21.42,10.23,9.99,11.33,19.6,2.35,30.54-6.94,8.45-14.95,16.03-22.34,23.84Z">
                                            </path>
                                        </svg>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="contact.php">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" id="Layer_1" viewBox="0 0 500 500">
                                            <defs>
                                                <style>
                                                    .cls-1 {
                                                        fill: #183052;
                                                    }
                                                </style>
                                            </defs>
                                            <path class="cls-1" d="m219.83,57.41c.42-6.52.45-14.16,1.51-21.66,1.95-13.74,13.96-23.25,28.49-23.25,14.53,0,26.54,9.52,28.49,23.25,1.06,7.5,1.09,15.14,1.6,22.99,7.39,2.08,15.03,3.74,22.32,6.37,51.87,18.71,83.3,55.33,93.24,109.49,2.73,14.85,2.55,30.35,2.6,45.56.09,30.07,4.87,59.04,17.56,86.47,9.08,19.62,21.89,36.76,35.33,53.51,9.84,12.26,9.22,25.33-2.62,33.25-4.92,3.29-11.9,4.89-17.95,4.91-102.06.29-204.12.21-306.18.21-19.18,0-38.35.13-57.52-.05-14.75-.14-25.85-10.36-24.43-23.46.58-5.32,3.64-10.92,7-15.29,13.71-17.83,27.3-35.59,36.36-56.47,10.66-24.58,15.28-50.2,15.95-76.87.48-19.1.36-38.51,3.8-57.18,7.92-42.97,32.95-74.55,71.11-95.05,13.26-7.12,28.25-11.01,43.33-16.71Zm171.25,296.44c-2.49-4.18-4.38-7.34-6.26-10.52-17.99-30.54-28.18-63.48-29.94-98.9-.97-19.55-.3-39.25-2.41-58.66-3.82-35.06-22.42-60.68-54.51-75.35-25.47-11.64-52.54-12.14-79.27-5.63-36.78,8.96-59.93,32.61-69.63,69.37-4.39,16.62-3.26,33.56-3.29,50.42-.05,37.67-7.29,73.68-24.57,107.36-3.72,7.25-8.01,14.22-12.37,21.91h282.24Z">
                                            </path>
                                            <path class="cls-1" d="m308.81,428.92c-.55,33.65-28.35,58.34-58.65,58.58-29.66.23-58.64-23.89-59.3-58.58h117.95Z">
                                            </path>
                                        </svg>
                                    </span>
                                </a>
                            </li>
                        </div>
                        <div class="text-dark order-2 order-md-1 d-flex justify-content-center">
                            <span class="text-muted fw-bold me-1">© 2023</span>
                            <a href="#" target="_blank" class="text-gray-800 text-hover-primary">Tothiq Web Design Company. All Right Reserved.</a>
                        </div>
                    </div>
                </div>
                <!--end::footer-->
            </div>
        </div>
        <!-- End :: Contetn Area-->


        <script>
            var hostUrl = "../assets/";
        </script>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src="../assets/plugins/global/plugins.bundle.js"></script>
        <script src="../assets/js/scripts.bundle.js"></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Vendors Javascript(used by this page)-->
        <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
        <!--end::Page Vendors Javascript-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src="../assets/js/custom/widgets.js"></script>
        <script src="../assets/js/custom/apps/chat/chat.js"></script>
        <script src="../assets/js/custom/modals/create-app.js"></script>
        <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
        <!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
        <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
        <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
        <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
        <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
        <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>
    </body>
    <!--end::Body-->

    </html>