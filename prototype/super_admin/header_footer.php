<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tothiq - Digital Contracts Platform</title>
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css?v=1671443234" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link href="../assets/css/media.css?v=1671443234" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed"
    style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="page d-flex flex-row">
            <!--begin::Aside-->
            <div id="kt_aside" class="aside aside-light aside-hoverable" data-kt-drawer="true"
                data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
                <!--begin::Brand-->
                <div class="aside-logo flex-column-auto h-125px" id="kt_aside_logo">
                    <!--begin::Logo-->
                    <a href="free_dashboard.html">
                        <img alt="Logo" src="../assets/images/logo.png" class="w-60px" />
                        <!-- <p class=" mb-0 pt-3 fs-6 fw-bolder text-dark">Digital Contracts Platform</p> -->
                    </a>
                </div>
                <!--end::Brand-->
                <!--begin::Aside menu-->
                <div class="aside-menu flex-column-fluid">
                    <!--begin::Aside Menu-->
                    <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
                        data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
                        data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                        data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
                        <!--begin::Menu-->
                        <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                            id="#kt_aside_menu" data-kt-menu="true">
                            <div class="menu-item">
                                <a class="menu-link" href="dashboard.html">
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="template.html">
                                    <span class="menu-title ">Template</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="user.html">
                                    <span class="menu-title">Users</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="membership.html">
                                    <span class="menu-title">Membership</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" data-bs-toggle="modal" type="button" href="#exampleModalNoti">
                                    <span class="menu-title ">Notification</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="label_management.html">
                                    <span class="menu-title">Label Management</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="coupon_management.html">
                                    <span class="menu-title text-primary">Coupons Management</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="settings.html">
                                    <span class="menu-title">Setting</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="report.html">
                                    <span class="menu-title">Reports</span>
                                </a>
                            </div>
                            <div class="menu-item">
                                <a class="menu-link" href="#">
                                    <span class="menu-title">Tothiq Users</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="aside-footer flex-column-auto  menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
                        id="kt_aside_footer">
                        <div class="separator  my-3 text-dark"></div>
                        <div id="kt_header_user_menu_toggle">
                            <div class="menu-item text-center ps-20 ms-2">
                                <a class="menu-link" href="notification_bell.html">
                                    <span class="menu-title">
                                        <i class="fas fa-bell p-2 bg-light-success text-success me-15 text-hover-primary "></i>
                                    </span>
                                </a>
                            </div>
                            <div class="text-center pe-15 my-3 border-top border-2 pt-5">
                                <div class="cursor-pointer symbol symbol-30px symbol-md-40px show menu-dropdown text-hover-primary position-relative"
                                    data-kt-menu-trigger="hover" data-kt-menu-attach="parent"
                                    data-kt-menu-placement="right-end">
                                    <div><i class="fas fa-user p-5 bg-secondary "></i></div>
                                    <h4 class="mt-5">Ahmed Ashor</h4>
                                    <p>Super Admin</p>
                                </div>
                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px " data-kt-menu="true" data-popper-placement="right-end" style="z-index: 105; position: fixed; inset: 0px 0px auto auto; margin: 0px; transform: translate3d(-30px, 65px, 0px);">
                                    <div class="menu-item  bg-light-danger p-5 text-dark">
                                        <h2>Ahmed Ashor</h2>
                                        <p>Super Admin</p>
                                    </div>
                                    <div class="separator border-3 my-5"></div>
                                    <div class="menu-item px-5">
                                        <a href="login_screen.html" class="menu-link px-5"><div class="ps-5 d-flex"><i
                                            class="bi bi-box-arrow-right me-2 mt-1 text-dark fa-1x"></i><span>
                                            <p>Log Out</p>
                                        </span></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            <div id="kt_header" class="header align-items-stretch">
                <div class="container-fluid d-flex align-items-center justify-content-between">
                    <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                        data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                        class="page-title d-flex align-items-center justify-content-between flex-wrap me-3 mb-5 mb-lg-0 w-100">
                        <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Template Creation</h1>
                        <a href="#." class="btn btn-primary">Create Templates</a>
                    </div>
                </div>
            </div>
            <div class="content d-flex flex-column p-0" id="kt_content">
                <div class="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" class="container-xxl">
                        <div class="kt_content_containe_inr d-flex justify-content-between my-5">

                        </div>


                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModalNoti" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
                tabindex="-1">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header d-flex justify-content-between">
                            <h3 class="text-capitalize">Add New Notification</h3>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-5 mt-5">
                                    <label class="form-check-label" for="flexCheckChecked">Select Notification
                                        Type</label>
                                    <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                        data-placeholder="Select an option">
                                        <option></option>
                                        <option value="1">Renewal Notification</option>
                                        <option value="2">New Notification</option>
                                    </select>
                                </div>
                                <div class="col-5 mt-5">
                                    <label class="form-check-label" for="flexCheckChecked">Select Recipient</label>
                                    <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                        data-placeholder="Select an option">
                                        <option></option>
                                        <option value="1">Everyone</option>
                                        <option value="2">Individual User</option>
                                        <option value="3">Business User</option>
                                    </select>
                                </div>
                                <div class="col-2 position-relative">
                                    <div class="position-absolute start-75 bottom-0">
                                        <i class="fas fa-camera-retro fa-2x text-dark"></i>
                                    </div>
                                    <img src="./images/img_default.jpg" class="w-100" alt="">
                                </div>
                            </div>
                            <div class="mb-5">
                                <label for="exampleFormControlInput1" class="form-label">Notification Title</label>
                                <input type="email" class="form-control form-control-solid" placeholder="Type Here" />
                            </div>
                            <div class="form-group mb-5">
                                <label for="exampleFormControlInput1" class="form-label">Description</label>
                                <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="4"
                                    placeholder="Type Here"></textarea>
                            </div>
                            <div class="pt-3 text-end">
                                <a type="button" class="btn btn-primary me-5" href="notification.html">Send</a>
                                <a type="button" class="btn btn-danger" href="#.">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="footer py-4 d-flex  flex-lg-column app-footer mt-10" id="kt_footer">
                <!--begin::Container-->
                <div
                    class="app-container container-fluid  d-flex flex-column flex-md-row flex-center flex-md-stack py-3 justify-content-end">
                    <!--begin::Copyright-->
                    <div class="text-dark order-2 order-md-1">
                        <span class="text-muted fw-bold me-1">© 2023</span>
                        <a href="#" target="_blank" class="text-gray-800 text-hover-primary">TOTHIQ / All Right
                            Reserved</a>
                    </div>
                </div>
                <!--end::Container-->
            </div>
        </div>
    </div>


    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
        <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
                    fill="black" />
                <path
                    d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                    fill="black" />
            </svg>
        </span>
    </div>
    <script>
        var hostUrl = "../assets/";
    </script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
    <script src="../assets/js/custom/widgets.js"></script>
    <script src="../assets/js/custom/apps/chat/chat.js"></script>
    <script src="../assets/js/custom/modals/create-app.js"></script>
    <script src="../assets/js/custom/modals/upgrade-plan.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
    <script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

    <script>
        ClassicEditor
            .create(document.querySelector('#ckeditor_1'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_2'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#ckeditor_3'))
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
    <script>
        // $(document).ready(function () {
        //     $(".companyform").click(function () {
        //         var test = $(this).val();
        //         if(test =='yes'){
        //             $("div.desc2").hide();
        //             $("div.desc1").show();
        //         }else{
        //             $("div.desc1").hide();
        //             $("div.desc2").show();

        //         }
        //     });
        // });
        $("#business-tab").click(function () {
            $("div#myTabContent1").hide();
            $("div#myTabContent2").show();
        });
        $("#individual-tab").click(function () {
            $("div#myTabContent1").show();
            $("div#myTabContent2").hide();
        });
        $(document).ready(function () {
            $('#addresstype').on('change', function () {
                var demovalue = $(this).val();
                $("div.myDiv").hide();
                $("#show" + demovalue).show();
            });
            $('#addresstype1').on('change', function () {
                var demovalue1 = $(this).val();
                $("div.myDiv1").hide();
                $("#show" + demovalue1).show();
            });
        });
        // var profileborder = "border-danger";
        $(".userprofile").addClass("border-danger");
        // function changeuserborder() {
        //     $(".userprofile").removeClass(profileborder);
        //     var profileborder = "border-success";
        // }
        $("#userheaderchange").click(function () {
            $(".userprofile").removeClass("border-danger");
            $(".userprofile").addClass("border-success");
            $("#headererror").addClass("d-none");

        });
    </script>
    <script>
        $.fn.equalHeights = function () {
            var max_height = 0;
            $(this).each(function () {
                max_height = Math.max($(this).height(), max_height);
            });
            $(this).each(function () {
                $(this).height(max_height);
            });
        };

        $(document).ready(function () {
            $('.userdasboardbox ul li a .card').equalHeights();
        });
    </script>

</body>

</html>