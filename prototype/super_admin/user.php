
<?php include("header.php") ?>

<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container px-7 d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Users</h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
        <!--begin::Actions-->
        <div class="d-flex align-items-center gap-2 gap-lg-3">
        </div>
        <!--end::Actions-->
    </div>
    <!--end::Toolbar container-->
</div>
<div class="card mx-5 mt-5">
    <div class="d-flex row py-3 flex-wrap">
        <div class="d-flex align-items-center col-7">
            <a href="#." class="mx-4 text-dark text-hover-primary">A</a>
            <a href="#." class="text-dark text-hover-primary">B</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">C</a>
            <a href="#." class="text-dark text-hover-primary">D</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">E</a>
            <a href="#." class="text-dark text-hover-primary">F</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">G</a>
            <a href="#." class="text-dark text-hover-primary">H</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">I</a>
            <a href="#." class="text-dark text-hover-primary">J</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">K</a>
            <a href="#." class="text-dark text-hover-primary">L</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">M</a>
            <a href="#." class="text-dark text-hover-primary">N</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">O</a>
            <a href="#." class="text-dark text-hover-primary">P</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">Q</a>
            <a href="#." class="text-dark text-hover-primary">R</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">S</a>
            <a href="#." class="text-dark text-hover-primary">T</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">U</a>
            <a href="#." class="text-dark text-hover-primary">V</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">W</a>
            <a href="#." class="text-dark text-hover-primary">X</a>
            <a href="#." class="mx-4 text-dark text-hover-primary">Y</a>
            <a href="#." class="text-dark text-hover-primary">Z</a>
        </div>
        <div class="col-2">
            <form data-kt-search-element="form" class="d-none d-lg-block mb-5 mb-lg-0 position-relative"
                autocomplete="off">
                <span class="svg-icon svg-icon-2 svg-icon-gray-700 position-absolute top-50 translate-middle-y ms-4 ">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.5" x="17.0365" y="13.1223" width="8.15546" height="2" rx="1"
                            transform="rotate(45 17.0365 15.1223)" fill="currentColor">
                        </rect>
                        <path
                            d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                            fill="currentColor"></path>
                    </svg>
                </span>
                <input type="text" class="form-control form-control-solid h-40px bg-body ps-13 fs-7 w-100" name="search"
                    value="" placeholder="Search Contact" data-kt-search-element="input">
            </form>
        </div>
        <button class="col-2 btn btn-primary btn-sm">Search</button>
        <i class="col-1 fas fa-filter fa-2x text-dark d-flex justify-content-center align-items-center"></i>
    </div>
</div>
<div class="content d-flex flex-column p-0" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl p-0 m-5">
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 card">
                <div class="card-body">
                    <div class="fv-row  col-12">
                        <ul
                            class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold text-center">
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1 active"
                                    data-bs-toggle="tab" id="individual-tab" href="#Individualform">Individual
                                    User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1"
                                    data-bs-toggle="tab" id="business-tab" href="#Businessform">Business
                                    User</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content mb-20" id="myTabContent">
                <div class="tab-pane fade active show card" id="Individualform" role="tabpanel">
                    <div class=" card-body">
                        <div id="kt_ecommerce_report_sales_table_wrapper"
                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                    id="kt_ecommerce_report_sales_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                            <th class=" sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Date: activate to sort column ascending">Thotiq ID</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Business
                                                Name</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Admin
                                                Email Address</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Contact
                                                Number</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Membership
                                            </th>
                                            <th class="text-start  sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Total: activate to sort column ascending">Create Date</th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        <!--begin::Table row-->
                                        <tr class="odd">
                                            <!--begin::Date=-->
                                            <td data-order="2023-09-22T00:00:00+05:30">#198305</td>
                                            <!--end::Date=-->
                                            <!--begin::No Orders=-->
                                            <td class=" pe-0"><a href="user_individual_user_details.php" class="">Aabel
                                                    Baasiqaat
                                                    Baazighah</a></td>
                                            <!--end::No Orders=-->
                                            <!--begin::Products sold=-->
                                            <td class=" pe-0">aabel.baasi@gmail.com</td>
                                            <!--end::Products sold=-->
                                            <!--begin::Tax=-->
                                            <td class=" pe-0">+965-1234567</td>
                                            <!--end::Tax=-->
                                            <!--begin::Total=-->
                                            <td class=" pe-0">Free</td>
                                            <td class="">13-11-2022</td>
                                            <!--end::Total=-->
                                        </tr>
                                        <tr class="even">
                                            <!--begin::Date=-->
                                            <td data-order="2023-09-22T00:00:00+05:30"> #198412</td>
                                            <!--end::Date=-->
                                            <!--begin::No Orders=-->
                                            <td class=" pe-0"><a href="#." class="">Aabidoon Aabinus
                                                    Pabel</a></td>
                                            <!--end::No Orders=-->
                                            <!--begin::Products sold=-->
                                            <td class=" pe-0">Aabinus@421212gmail.com</td>
                                            <!--end::Products sold=-->
                                            <!--begin::Tax=-->
                                            <td class=" pe-0">+965-1234567</td>
                                            <!--end::Tax=-->
                                            <!--begin::Total=-->
                                            <td class=" pe-0">Premium</td>
                                            <td class="">12-12-2022</td>
                                            <!--end::Total=-->
                                        </tr>

                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                        </div>
                        <!-- <table class="table border border-gray-400 mb-20">
                                    <tr class="fw-bolder  border-bottom  border-gray-400 text-center">
                                        <th class="border-end  border-gray-400 ">Thotiq ID</th>
                                        <th class="border-end  border-gray-400 ">Full Name</th>
                                        <th class="border-end  border-gray-400 ">Email Address</th>
                                        <th class="border-end  border-gray-400 ">Contact Number</th>
                                        <th class="border-end  border-gray-400 ">Membership</th>
                                        <th>Create Date</th>
                                    </tr>
                                    <tr class="border-bottom  border-gray-400 text-center">
                                        <td class="border-end  border-gray-400 ">#198305</td>
                                        <td class="border-end  border-gray-400 "><a
                                                href="user_individual_user_details.html" class="">Aabel Baasiqaat
                                                Baazighah</a></td>
                                        <td class="border-end  border-gray-400 ">aabel.baasi@gmail.com</td>
                                        <td class="border-end  border-gray-400 ">+965-1234567</td>
                                        <td class="border-end  border-gray-400 ">Free</td>
                                        <td>13-11-2022</td>
                                    </tr>
                                    <tr class="border-bottom border-gray-400 text-center">
                                        <td class="border-end  border-gray-400 "> #198412</td>
                                        <td class="border-end  border-gray-400 "><a href="#." class="">Aabidoon Aabinus
                                                Pabel</a></td>
                                        <td class="border-end  border-gray-400 ">Aabinus@421212gmail.com</td>
                                        <td class="border-end  border-gray-400 ">+965-1234567</td>
                                        <td class="border-end  border-gray-400 ">Premium</td>
                                        <td>12-12-2022</td>
                                    </tr>
                                </table> -->
                        <ul class="pagination pagination-outline d-flex justify-content-end">
                            <li class="page-item previous disabled m-1"><a href="#" class="page-link"><i
                                        class="previous"></i></a></li>
                            <li class="page-item active m-1"><a href="#" class="page-link">1</a></li>
                            <li class="page-item next m-1"><a href="#" class="page-link"><i class="next"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="tab-pane fade card" id="Businessform" role="tabpanel">
                    <div class=" card-body">
                        <div id="kt_ecommerce_report_sales_table_wrapper"
                            class="dataTables_wrapper dt-bootstrap4 no-footer">
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                    id="kt_ecommerce_report_sales_table">
                                    <!--begin::Table head-->
                                    <thead>
                                        <!--begin::Table row-->
                                        <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                            <th class=" sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Date: activate to sort column ascending">Thotiq ID</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Business
                                                Name</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Admin
                                                Email Address</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Contact
                                                Number</th>
                                            <th class="text-start sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Products Sold: activate to sort column ascending">Membership
                                            </th>
                                            <th class="text-start  sorting" tabindex="0"
                                                aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                                aria-label="Total: activate to sort column ascending">Create Date</th>
                                        </tr>
                                        <!--end::Table row-->
                                    </thead>
                                    <!--end::Table head-->
                                    <!--begin::Table body-->
                                    <tbody class="fw-semibold text-gray-600">
                                        <!--begin::Table row-->
                                        <tr class="odd">
                                            <!--begin::Date=-->
                                            <td data-order="2023-09-22T00:00:00+05:30">#198305</td>
                                            <!--end::Date=-->
                                            <!--begin::No Orders=-->
                                            <td class=" pe-0"><a href="user_business_user_details.php"
                                                    class="">Microsolution Kuwait</a></td>
                                            <!--end::No Orders=-->
                                            <!--begin::Products sold=-->
                                            <td class=" pe-0">aabel.baasi@gmail.com</td>
                                            <!--end::Products sold=-->
                                            <!--begin::Tax=-->
                                            <td class=" pe-0">+965-1234567</td>
                                            <!--end::Tax=-->
                                            <!--begin::Total=-->
                                            <td class=" pe-0">Free</td>
                                            <td class="">13-11-2022</td>
                                            <!--end::Total=-->
                                        </tr>
                                        <tr class="even">
                                            <!--begin::Date=-->
                                            <td data-order="2023-09-22T00:00:00+05:30"> #198412</td>
                                            <!--end::Date=-->
                                            <!--begin::No Orders=-->
                                            <td class=" pe-0"><a href="user_business_user_details.php"
                                                    class="">Creativity Solutions</a></td>
                                            <!--end::No Orders=-->
                                            <!--begin::Products sold=-->
                                            <td class=" pe-0">Aabinus@421212gmail.com</td>
                                            <!--end::Products sold=-->
                                            <!--begin::Tax=-->
                                            <td class=" pe-0">+965-1234567</td>
                                            <!--end::Tax=-->
                                            <!--begin::Total=-->
                                            <td class=" pe-0">Premium</td>
                                            <td class="">12-12-2022</td>
                                            <!--end::Total=-->
                                        </tr>

                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                            </div>
                        </div>
                        <!-- <table class="table border border-gray-400 mb-20">
                                    <tr class="fw-bolder  border-bottom  border-gray-400 text-center">
                                        <th class="border-end  border-gray-400 ">Thotiq ID</th>
                                        <th class="border-end  border-gray-400 "> Business Name</th>
                                        <th class="border-end  border-gray-400 ">Admin Email Address</th>
                                        <th class="border-end  border-gray-400 ">Contact Number</th>
                                        <th class="border-end  border-gray-400 ">Membership</th>
                                        <th>Create Date</th>
                                    </tr>
                                    <tr class="border-bottom  border-gray-400 text-center">
                                        <td class="border-end  border-gray-400 ">B198305</td>
                                        <td class="border-end  border-gray-400 "><a
                                                href="user_business_user_details.html" class="">Microsolution Kuwait</a>
                                        </td>
                                        <td class="border-end  border-gray-400 ">aabel.baasi@gmail.com</td>
                                        <td class="border-end  border-gray-400 ">+965-1234567</td>
                                        <td class="border-end  border-gray-400 ">Free</td>
                                        <td>13-11-2022</td>
                                    </tr>
                                    <tr class="border-bottom border-gray-400 text-center">
                                        <td class="border-end  border-gray-400 "> B198412</td>
                                        <td class="border-end  border-gray-400 "><a
                                                href="user_business_user_details.html" class="">Creativity Solutions</a>
                                        </td>
                                        <td class="border-end  border-gray-400 ">Aabinus@421212gmail.com</td>
                                        <td class="border-end  border-gray-400 ">+965-1234567</td>
                                        <td class="border-end  border-gray-400 ">Premium</td>
                                        <td>12-12-2022</td>
                                    </tr>
                                </table> -->
                        <ul class="pagination pagination-outline d-flex justify-content-end">
                            <li class="page-item previous disabled m-1"><a href="#" class="page-link"><i
                                        class="previous"></i></a></li>
                            <li class="page-item active m-1"><a href="#" class="page-link">1</a></li>
                            <li class="page-item next m-1"><a href="#" class="page-link"><i class="next"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="exampleModalNoti" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
            tabindex="-1">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header d-flex justify-content-between">
                        <h3 class="text-capitalize">Add New Notification</h3>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-5 mt-5">
                                <label class="form-check-label" for="flexCheckChecked">Select Notification
                                    Type</label>
                                <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                    data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Renewal Notification</option>
                                    <option value="2">New Notification</option>
                                </select>
                            </div>
                            <div class="col-5 mt-5">
                                <label class="form-check-label" for="flexCheckChecked">Select Recipient</label>
                                <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                    data-placeholder="Select an option">
                                    <option></option>
                                    <option value="1">Everyone</option>
                                    <option value="2">Individual User</option>
                                    <option value="3">Business User</option>
                                </select>
                            </div>
                            <div class="col-2 position-relative">
                                <div class="position-absolute start-75 bottom-0">
                                    <i class="fas fa-camera-retro fa-2x text-dark"></i>
                                </div>
                                <img src="./images/img_default.jpg" class="w-100" alt="">
                            </div>
                        </div>
                        <div class="mb-5">
                            <label for="exampleFormControlInput1" class="form-label">Notification Title</label>
                            <input type="email" class="form-control form-control-solid" placeholder="Type Here" />
                        </div>
                        <div class="form-group mb-5">
                            <label for="exampleFormControlInput1" class="form-label">Description</label>
                            <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="4"
                                placeholder="Type Here"></textarea>
                        </div>
                        <div class="pt-3 text-end">
                            <a type="button" class="btn btn-primary me-5" href="notification.html">Send</a>
                            <a type="button" class="btn btn-danger" href="#.">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->




<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path
                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                fill="black" />
        </svg>
    </span>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function () {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function () {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function () {
        $('#addresstype').on('change', function () {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function () {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function () {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    /*$.fn.equalHeights = function () {
        var max_height = 0;
        $(this).each(function () {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function () {
            $(this).height(max_height);
        });
    };

    $(document).ready(function () {
        $('.userdasboardbox ul li a .card').equalHeights();
    });*/
</script>
<script>

	const urlParams = new URLSearchParams(window.location.search);
	const myParam = urlParams.get('user');	
	if(myParam){
		if(myParam == 'business'){
			$('#individual-tab').removeClass('active');
			$('#Individualform').removeClass('active show');
			
			$('#business-tab').addClass('active');
			$('#Businessform').addClass('active show');
			/*const individualTab = document.getElementById('individual-tab');
			individualTab.classList.remove('active');
			
			const individualForm = document.getElementById('Individualform');
			individualForm.classList.remove('active show');
			
			const businessTab = document.getElementById('business-tab');
			businessTab.classList.add('active');
			
			const businessForm = document.getElementById('Businessform');
			businessForm.classList.add('active show');*/
			
		}else{
			$('#business-tab').removeClass('active');
			$('#Businessform').removeClass('active show');
			
			$('#individual-tab').addClass('active');
			$('#Individualform').addClass('active show');
			
		}
	}

</script>
<?php include("footer.php") ?>