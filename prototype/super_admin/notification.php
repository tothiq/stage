<?php include("header.php") ?>

<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
  <div class="post d-flex flex-column-fluid mb-20" id="kt_post">
    <div id="kt_content_container" class="container-xxl mb-20">
      <div id="kt_app_toolbar" class="app-toolbar py-8">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container px-3 d-flex flex-stack">
          <!--begin::Page title-->
          <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Notification
            </h1>
            <!--end::Title-->
          </div>
          <!--end::Page title-->
          <!--begin::Actions-->
          <div class="d-flex align-items-center gap-2 gap-lg-3">
            <!--begin::Secondary button-->

            <!--end::Secondary button-->
            <!--begin::Primary button-->
            <a class="btn btn-primary btn-sm" data-bs-toggle="modal" type="button" href="#exampleModalNoti">Send
              Notification</a>
            <!--end::Primary button-->
          </div>
          <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
      </div>
      <div class="kt_content_containe_inr d-flex justify-content-between my-5 card mb-13">
        <div class="card-body mb-20">
          <table class="table border border-gray-400 mb-20">
            <tr class="fw-bolder  border-bottom  border-gray-400 text-center">
              <th class="border-end  border-gray-400 ">Notification Date</th>
              <th class="border-end  border-gray-400 ">Notification Title</th>
              <th class="border-end  border-gray-400 ">Sent to</th>
              <th>Notification Type</th>
            </tr>
            <tr class="border-bottom  border-gray-400 text-center">
              <td class="border-end  border-gray-400 ">12-dec-2022</td>
              <td class="border-end  border-gray-400 ">Discount</td>
              <td class="border-end  border-gray-400 ">Everyone</td>
              <td>Renewal notification</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="exampleModalNoti" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
  tabindex="-1">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-between">
        <h3 class="text-capitalize">Add New Notification</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-5 mt-5">
            <label class="form-check-label" for="flexCheckChecked">Select Notification
              Type</label>
            <select class="form-select form-select-sm form-select-solid" data-control="select2"
              data-placeholder="Select an option">
              <option></option>
              <option value="1">Renewal Notification</option>
              <option value="2">New Notification</option>
            </select>
          </div>
          <div class="col-5 mt-5">
            <label class="form-check-label" for="flexCheckChecked">Select Recipient</label>
            <select class="form-select form-select-sm form-select-solid" data-control="select2"
              data-placeholder="Select an option">
              <option></option>
              <option value="1">Everyone</option>
              <option value="2">Individual User</option>
              <option value="3">Business User</option>
            </select>
          </div>
          <div class="col-2 position-relative">
            <div class="position-absolute start-75 bottom-0">
              <i class="fas fa-camera-retro fa-2x text-dark"></i>
            </div>
            <img src="./images/img_default.jpg" class="w-100" alt="">
          </div>
        </div>
        <div class="mb-5">
          <label for="exampleFormControlInput1" class="form-label">Notification Title</label>
          <input type="email" class="form-control form-control-solid" placeholder="Type Here" />
        </div>
        <div class="form-group mb-5">
          <label for="exampleFormControlInput1" class="form-label">Description</label>
          <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="4"
            placeholder="Type Here"></textarea>
        </div>
        <div class="pt-3 text-end">
          <a type="button" class="btn btn-primary me-5" href="notification.php">Send</a>
          <a type="button" class="btn btn-danger" href="#.">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
  <span class="svg-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
      <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
      <path
        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
        fill="black" />
    </svg>
  </span>
</div>


<script>
  ClassicEditor
    .create(document.querySelector('#ckeditor_1'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_2'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_3'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
</script>
<script>

  $("#business-tab").click(function () {
    $("div#myTabContent1").hide();
    $("div#myTabContent2").show();
  });
  $("#individual-tab").click(function () {
    $("div#myTabContent1").show();
    $("div#myTabContent2").hide();
  });
  $(document).ready(function () {
    $('#addresstype').on('change', function () {
      var demovalue = $(this).val();
      $("div.myDiv").hide();
      $("#show" + demovalue).show();
    });
    $('#addresstype1').on('change', function () {
      var demovalue1 = $(this).val();
      $("div.myDiv1").hide();
      $("#show" + demovalue1).show();
    });
  });
  // var profileborder = "border-danger";
  $(".userprofile").addClass("border-danger");
  $("#userheaderchange").click(function () {
    $(".userprofile").removeClass("border-danger");
    $(".userprofile").addClass("border-success");
    $("#headererror").addClass("d-none");

  });
</script>
<script>
  $.fn.equalHeights = function () {
    var max_height = 0;
    $(this).each(function () {
      max_height = Math.max($(this).height(), max_height);
    });
    $(this).each(function () {
      $(this).height(max_height);
    });
  };

  $(document).ready(function () {
    $('.userdasboardbox ul li a .card').equalHeights();
  });
</script>

<?php include("footer.php") ?>