
<?php include("header.php") ?>

<div class="content d-flex flex-column p-0 position-relative" id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl">
            <div id="kt_app_toolbar" class="app-toolbar py-8">
                <!--begin::Toolbar container-->
                <div id="kt_app_toolbar_container" class="app-container px-3 d-flex flex-stack">
                    <!--begin::Page title-->
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                            Tothiq User</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Page title-->
                    <!--begin::Actions-->
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                    </div>
                    <!--end::Actions-->
                </div>
                <!--end::Toolbar container-->
            </div>
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 card">
                <div class="card-body">

                    <div class="d-flex  justify-content-between ">
                        <input type="text" class="form-control form-control-sm w-25" placeholder="Search User" />
                        <a id="kt_drawer_chat_toggle_1" class="btn btn-sm btn-primary ">Add New</a>
                    </div>
                    <div id="kt_ecommerce_report_sales_table_wrapper"
                        class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                                id="kt_ecommerce_report_sales_table">
                                <!--begin::Table head-->
                                <thead>
                                    <!--begin::Table row-->
                                    <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                        <th class=" sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Date: activate to sort column ascending"
                                            style="width: 232.65px;">User Name</th>
                                        <th class="text-start sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Products Sold: activate to sort column ascending"
                                            style="width: 253.975px;">Email Address</th>
                                        <th class="text-start sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Products Sold: activate to sort column ascending"
                                            style="width: 253.975px;">Department</th>
                                        <th class="text-start sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Products Sold: activate to sort column ascending"
                                            style="width: 253.975px;">Last Login</th>
                                        <th class="text-start sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Products Sold: activate to sort column ascending"
                                            style="width: 240.975px;">Account Status</th>
                                        <th class="text-start  sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Total: activate to sort column ascending"
                                            style="width: 200.55px;">Access</th>
                                        <th class="text-start  sorting" tabindex="0"
                                            aria-controls="kt_ecommerce_report_sales_table" rowspan="1" colspan="1"
                                            aria-label="Total: activate to sort column ascending"
                                            style="width: 200.55px;">Action</th>
                                    </tr>
                                    <!--end::Table row-->
                                </thead>
                                <!--end::Table head-->
                                <!--begin::Table body-->
                                <tbody class="fw-semibold text-gray-600">
                                    <!--begin::Table row-->
                                    <tr class="odd">
                                        <td data-order="2023-09-22T00:00:00+05:30">Abdul Karim</td>
                                        <td class=" pe-0">abdulkarim@gmail.com</td>
                                        <td class=" pe-0">General Management</td>
                                        <td class=" pe-0">Yesterday</td>
                                        <td class=" pe-0">Deactive</td>
                                        <td class=" pe-0">Only View</td>
                                        <td class="">None</td>
                                    </tr>
                                    <tr class="even">
                                        <!--begin::Date=-->
                                        <td data-order="2023-09-22T00:00:00+05:30"></td>
                                        <!--end::Date=-->
                                        <!--begin::No Orders=-->
                                        <td class=" pe-0"></td>
                                        <!--end::No Orders=-->
                                        <!--begin::Products sold=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Products sold=-->
                                        <!--begin::Tax=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Tax=-->
                                        <!--begin::Total=-->
                                        <td class=" pe-0"></td>
                                        <td class=" pe-0"></td>
                                        <td class=""></td>
                                        <!--end::Total=-->
                                    </tr>
                                    <tr class="odd">
                                        <!--begin::Date=-->
                                        <td data-order="2023-09-22T00:00:00+05:30"></td>
                                        <!--end::Date=-->
                                        <!--begin::No Orders=-->
                                        <td class=" pe-0"></td>
                                        <!--end::No Orders=-->
                                        <!--begin::Products sold=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Products sold=-->
                                        <!--begin::Tax=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Tax=-->
                                        <!--begin::Total=-->
                                        <td class=" pe-0"></td>
                                        <td class=" pe-0"></td>
                                        <td class=""></td>
                                        <!--end::Total=-->
                                    </tr>
                                    <tr class="even">
                                        <!--begin::Date=-->
                                        <td data-order="2023-09-22T00:00:00+05:30"></td>
                                        <!--end::Date=-->
                                        <!--begin::No Orders=-->
                                        <td class=" pe-0"></td>
                                        <!--end::No Orders=-->
                                        <!--begin::Products sold=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Products sold=-->
                                        <!--begin::Tax=-->
                                        <td class=" pe-0"></td>
                                        <!--end::Tax=-->
                                        <!--begin::Total=-->
                                        <td class=" pe-0"></td>
                                        <td class=" pe-0"></td>
                                        <td class=""></td>
                                        <!--end::Total=-->
                                    </tr>
                                </tbody>
                                <!--end::Table body-->
                            </table>
                        </div>
                        <!-- <div class="row">
                                        <div
                                            class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start">
                                            <div class="dataTables_length" id="kt_ecommerce_report_sales_table_length">
                                                <label><select name="kt_ecommerce_report_sales_table_length"
                                                        aria-controls="kt_ecommerce_report_sales_table"
                                                        class="form-select form-select-sm form-select-solid">
                                                        <option value="10">10</option>
                                                        <option value="25">25</option>
                                                        <option value="50">50</option>
                                                        <option value="100">100</option>
                                                    </select></label></div>
                                        </div>
                                        <div
                                            class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                id="kt_ecommerce_report_sales_table_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled"
                                                        id="kt_ecommerce_report_sales_table_previous"><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="0" tabindex="0" class="page-link"><i
                                                                class="previous"></i></a></li>
                                                    <li class="paginate_button page-item active"><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                                    <li class="paginate_button page-item "><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                                    <li class="paginate_button page-item "><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                                    <li class="paginate_button page-item "><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                                    <li class="paginate_button page-item "><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                                    <li class="paginate_button page-item next"
                                                        id="kt_ecommerce_report_sales_table_next"><a href="#"
                                                            aria-controls="kt_ecommerce_report_sales_table"
                                                            data-dt-idx="6" tabindex="0" class="page-link"><i
                                                                class="next"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> -->
                    </div>
                    <ul class="pagination pagination-outline d-flex justify-content-end">
                        <li class="page-item previous disabled m-1"><a href="#" class="page-link"><i
                                    class="previous"></i></a></li>
                        <li class="page-item active m-1"><a href="#" class="page-link">1</a></li>
                        <li class="page-item next m-1"><a href="#" class="page-link"><i class="next"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--end::Container-->
</div>
<div id="kt_drawer_chat_1" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_1"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-between">
                <h3 class="text-capitalize">Add User</h3>
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="mb-5">
                    <label for="exampleFormControlInput1" class="form-label">Name</label>
                    <input type="text" class="form-control form-control-solid" placeholder="Type Here" />
                </div>
                <div class="mb-5">
                    <label for="exampleFormControlInput1" class="form-label">Contact Number</label>
                    <input type="text" class="form-control form-control-solid" placeholder="Type Here" />
                </div>
                <div class="mb-10">
                    <label for="exampleFormControlInput1" class="form-label">Contact Email Address</label>
                    <input type="text" class="form-control form-control-solid" placeholder="Type Here" />
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Able to create template
                    </label>
                </div>
                <div class="form-check my-5">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Able to edit tempalte
                    </label>
                </div>
                <div class="form-check my-5">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Able to assign custom package to users
                    </label>
                </div>
                <div class="form-check mb-20">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                    <label class="form-check-label" for="flexCheckDefault">
                        Able to activate and block business users
                    </label>
                </div>
                <h5>Login Credentials</h5>
                <div class="form-check mt-5 mb-20">
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                    <label class="form-check-label" for="flexCheckChecked">
                        Generate automatically and send contact person email address
                    </label>
                </div>
                <!-- <div class="row">
                                <div class="col-6">
                                    <div class="mb-0">
                                        <label class="form-label">Start Date & Time</label>
                                        <input class="form-control form-control-solid" placeholder="Pick date rage"
                                            id="kt_daterangepicker_3" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="">
                                        <label class="form-label">End Date & Time</label>
                                        <input class="form-control form-control-solid" placeholder="Pick date rage"
                                            id="kt_daterangepicker_4" />
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-6 ">
                                    <h6 class="mt-7">
                                        Total number Of coupons availabe</h6>
                                    <div class="d-flex mt-3 mb-5">
                                        <a href="#" class="btn btn-sm btn-primary me-10">Unlimited</a>
                                        <a href="#"><button class="btn btn-sm btn-primary" disabled>
                                                Limited</button></a>
                                    </div>
                                    <h6>Limited Coupon Per Customer</h6>
                                    <div class="form-check form-switch form-check-custom form-check-solid my-2">
                                        <input class="form-check-input " type="checkbox" value=""
                                            id="flexSwitchChecked" />
                                        <label class="form-check-label" for="flexSwitchChecked">
                                            Limited Coupon per Customer
                                        </label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h6 class="my-5">Coupon Banner image</h6>
                                    <img src="./images/img_default.jpg" class="w-50 h-50" alt="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-check-label" for="flexSwitchChecked">
                                    Discount Details
                                </label>
                                <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="4"
                                    placeholder="Enter Description..."></textarea>
                            </div>
                            <div class="row my-7">
                                <div class="col-6">
                                    <label class="form-check-label" for="flexSwitchChecked">
                                        Discount Type
                                    </label>
                                    <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                        data-placeholder="%Percentage" data-allow-clear="true" multiple="multiple">
                                        <option></option>
                                        <option value="1">50%</option>
                                        <option value="2">80%</option>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label class="form-check-label" for="flexSwitchChecked">
                                        Discount if for
                                    </label>
                                    <select class="form-select form-select-sm form-select-solid" data-control="select2"
                                        data-placeholder="%Percentage" data-allow-clear="true" multiple="multiple">
                                        <option></option>
                                        <option value="1">New Subscription</option>
                                        <option value="2">Old Subscription</option>
                                        <option value="3">Both</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="exampleFormControlInput1" class="form-label">Enter Value</label>
                                    <input type="text" class="form-control form-control-solid" placeholder="%" />
                                </div>
                            </div>
                            <div class="form-check my-5">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                                <label class="form-check-label" for="flexCheckChecked">
                                    Send Alert to all existing users
                                </label>
                            </div> -->
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary btn-sm " href="tothiq_user.php">Create</a>
                    <a type="button" class="btn btn-primary btn-sm mx-5" href="#." Disable>Create & Send</a>
                    <a type="button" class="btn btn-primary btn-sm" href="#.">Clear</a>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path
                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function () {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function () {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function () {
        $('#addresstype').on('change', function () {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function () {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function () {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function () {
        var max_height = 0;
        $(this).each(function () {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function () {
            $(this).height(max_height);
        });
    };

    $(document).ready(function () {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer.php") ?>