
<?php include("header.php") ?>
      <div class="content d-flex flex-column p-0 mb-20" id="kt_content">
        <div class="post d-flex flex-column-fluid mb-10" id="kt_post">
          <div id="kt_content_container" class="container-fluid">
            <div class="row py-10">
              <div class="col">
                <span class="d-block fs-1 fw-bolder">Tothiq User - Dashboard</span>
              </div>
            </div>
            <div class="row gy-5 g-xl-8">
              <div class="col-xl-4">
                <div class="card mb-10">
                  <div class="card-header align-items-center border-0">
                    <h3 class="card-title align-items-start flex-column">
                      <span class="fw-bolder mb-2 text-dark">INDIVIDUAL</span>
                    </h3>
                    <div class="d-flex">
                      <input type="date" class="form-control form-control-sm form-control-solid me-3">
                      <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary">
                        <i class="fas fa-filter fs-4"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row g-5 mb-10">
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder  fs-3 text-dark">Starter</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">7894</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="primary"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder fs-3 text-dark">Basic</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">234</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="info"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder fs-3 text-dark">Premium</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">7894</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="primary"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-7">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Start Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-7">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Basic Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-5">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Premium Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header align-items-center border-0">
                    <h3 class="card-title align-items-start flex-column">
                      <span class="fw-bolder mb-2 text-dark">Business</span>
                    </h3>
                    <div class="d-flex">
                      <input type="date" class="form-control form-control-sm form-control-solid me-3">
                      <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary">
                        <i class="fas fa-filter fs-4"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row g-5 mb-10">
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder  fs-3 text-dark">Starter</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">7894</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="primary"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder fs-3 text-dark">Basic</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">234</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="info"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="ms-2 mb-3">
                          <span class="fw-bolder fs-3 text-dark">Premium</span>
                        </div>
                        <div class="border border-3 rounded p-1">
                          <div class="flex-grow-1">
                            <span class="fw-bolder fs-6 text-primary">7894</span>
                          </div>
                          <div class="mixed-widget-7-chart card-rounded-bottom" data-kt-chart-color="primary"
                            style="height: 80px; min-height: 80px;">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-7">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Start Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-7">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Basic Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="d-flex flex-column w-100 mb-5">
                      <div class="d-flex flex-stack mb-2 justify-content-between">
                        <h6>Premium Revenew</h6>
                        <span class="text-muted fs-7 fw-bold">50%</span>
                      </div>
                      <div class="progress h-6px w-100">
                        <div class="progress-bar bg-primary" role="progressbar" style="width: 50%" aria-valuenow="50"
                          aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-8">
                <div class="">
                  <div class="row">
                    <div class="col-md-2">
                      <div class="card">
                        <div class="card-body p-2">
                          <div>
                            <span class="fs-8">Sales with</span>
                            <span class="fs-8 d-block">coupons</span>
                            <span class="fs-3 fw-bolder d-block">2342</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="card">
                        <div class="card-body p-2">
                          <div>
                            <span class="fs-8">Sales without</span>
                            <span class="fs-8 d-block">coupons</span>
                            <span class="fs-3 fw-bolder d-block">2536</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="card">
                        <div class="card-body p-2">
                          <div>
                            <span class="fs-8">Net revenue with</span>
                            <span class="fs-8 d-block">Net coupons</span>
                            <span class="fs-3 fw-bolder d-block">KWD 4536</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="card">
                        <div class="card-body p-2">
                          <div>
                            <span class="fs-8">Discount from</span>
                            <span class="fs-8 d-block">coupons</span>
                            <span class="fs-3 fw-bolder d-block">KWD 768</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-body p-2">
                          <div class="mb-2">
                            <span class="fw-bold fs-6">Sub Category</span>
                            <select class="form-select form-select-sm form-select-solid p-2">
                              <option value="">All</option>
                              <option value="2">Individual</option>
                              <option value="3">Business</option>
                            </select>
                          </div>
                          <div class="">
                            <span class="fw-bold fs-6">Manager</span>
                            <select class="form-select form-select-sm form-select-solid">
                              <option></option>
                              <option value="1">All</option>
                              <option value="2">Accounting</option>
                              <option value="3">Developing</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card mt-5 p-5">
                    <div class="card-body p-0 d-flex justify-content-between flex-column overflow-hidden"
                      style="position: relative;">
                      <!--begin::Hidden-->
                      <div class="d-flex justify-content-between align-items-center border-0">
                        <div class="d-flex">
                          <div class="d-flex">
                            <span class="py-0 px-4 bg-primary me-2 rounded"></span>
                            <span>
                              <h6 class="mt-2">Coupon</h6>
                            </span>
                          </div>
                          <div class="d-flex ms-4">
                            <span class="py-0 px-4 bg-secondary me-2 rounded"></span>
                            <span>
                              <h6 class="mt-2">No Coupon</h6>
                            </span>
                          </div>
                        </div>
                        <div class="d-flex">
                          <input type="date" class="form-control form-control-sm form-control-solid me-3">
                          <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary">
                            <i class="fas fa-filter fs-4"></i>
                          </button>
                        </div>
                      </div>
                      <div class="mixed-widget-10-chart" data-kt-color="primary"
                        style="height: 175px; min-height: 183px;">

                      </div>
                    </div>

                  </div>
                  <div class="card mt-5">
                    <div class="card-header d-flex justify-content-between align-items-center border-0 p-5">
                      <h3 class="card-title align-items-start flex-column">
                        <span class="fw-bolder mb-2 text-dark">Notification</span>
                      </h3>
                      <div class="d-flex">
                        <input type="date" class="form-control form-control-sm form-control-solid">
                      </div>
                    </div>
                    <div class="card-body p-5 pt-0">
                      <div class="row">
                        <div class="col-10">
                          <div class="border rounded p-1 mixed-widget-10-chart" data-kt-color="Secondary"
                            style="height: 175px; min-height: 183px;">
                          </div>
                        </div>
                        <div class="col-2">
                          <select class="form-select form-select-sm">
                            <option value="">Impression</option>
                            <option value="2">Option 1</option>
                            <option value="3">Option 2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card mt-5">

                    <div class="card-body">
                      <div class="d-flex justify-content-between mb-5 align-items-center">
                        <h3>Admin Internal Users</h3>
                        <button class="btn btn-sm btn-primary" disabled>Add+</button>
                      </div>
                      <div class="table-responsive">
                        <!--begin::Table-->
                        <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
                          <!--begin::Table head-->
                          <thead>
                            <tr class="fw-bolder text-muted">
                              <th class="">Name</th>
                              <th class="">Email Address</th>
                              <th class="">Role</th>
                              <th class="">Created</th>
                              <th class="">Status</th>
                              <th class=" ">Actions</th>
                            </tr>
                          </thead>
                          <!--end::Table head-->
                          <!--begin::Table body-->
                          <tbody>
                            <tr>
                              <td>gdf fdv</td>
                              <td>fbg@gmail.com</td>
                              <td>admin</td>
                              <td>01-2-2020</td>
                              <td>Active</td>
                              <td>
                                <a href="#." class="text-hover-dark"><i class="fas fa-trash-alt fs-3"></i></a>
                                <a href="#." class="text-hover-dark"><i class="fas fa-wrench fs-5 ms-4"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>cad cv</td>
                              <td>fbg@gmail.com</td>
                              <td>user</td>
                              <td>04-2-2020</td>
                              <td>Inactive</td>
                              <td>
                                <a href="#." class="text-hover-dark"><i class="fas fa-trash-alt fs-3"></i></a>
                                <a href="#." class="text-hover-dark"><i class="fas fa-wrench fs-5 ms-4"></i></a>
                              </td>
                            </tr>
                            <tr>
                              <td>dcasfv</td>
                              <td>fbg@gmail.com</td>
                              <td>user</td>
                              <td>24-2-2020</td>
                              <td>Active</td>
                              <td>
                                <a href="#." class="text-hover-dark"><i class="fas fa-trash-alt fs-3"></i></a>
                                <a href="#." class="text-hover-dark"><i class="fas fa-wrench fs-5 ms-4"></i></a>
                              </td>
                            </tr>

                          </tbody>
                          <!--end::Table body-->
                        </table>
                        <!--end::Table-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     



  <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
        <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
        <path
          d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
          fill="black" />
      </svg>
    </span>
  </div>
  
  <script>
    ClassicEditor
      .create(document.querySelector('#ckeditor_1'))
      .then(editor => {
        console.log(editor);
      })
      .catch(error => {
        console.error(error);
      });
    ClassicEditor
      .create(document.querySelector('#ckeditor_2'))
      .then(editor => {
        console.log(editor);
      })
      .catch(error => {
        console.error(error);
      });
    ClassicEditor
      .create(document.querySelector('#ckeditor_3'))
      .then(editor => {
        console.log(editor);
      })
      .catch(error => {
        console.error(error);
      });
  </script>
  <script>
    $("#business-tab").click(function () {
      $("div#myTabContent1").hide();
      $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function () {
      $("div#myTabContent1").show();
      $("div#myTabContent2").hide();
    });
    $(document).ready(function () {
      $('#addresstype').on('change', function () {
        var demovalue = $(this).val();
        $("div.myDiv").hide();
        $("#show" + demovalue).show();
      });
      $('#addresstype1').on('change', function () {
        var demovalue1 = $(this).val();
        $("div.myDiv1").hide();
        $("#show" + demovalue1).show();
      });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function () {
      $(".userprofile").removeClass("border-danger");
      $(".userprofile").addClass("border-success");
      $("#headererror").addClass("d-none");

    });
  </script>
  <script>
    $.fn.equalHeights = function () {
      var max_height = 0;
      $(this).each(function () {
        max_height = Math.max($(this).height(), max_height);
      });
      $(this).each(function () {
        $(this).height(max_height);
      });
    };

    $(document).ready(function () {
      $('.userdasboardbox ul li a .card').equalHeights();
    });
  </script>
  <script>
    $("#kt_daterangepicker_3").daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      minYear: 1901,
      maxYear: parseInt(moment().format("YYYY"), 12)
    }, function (start, end, label) {
      var years = moment().diff(start, "years");
      alert("You are " + years + " years old!");
    }
    );
  </script>

<?php include("footer.php") ?>
