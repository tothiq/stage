<!DOCTYPE html>
<html lang="en">

<head>
    <title>Tothiq - Digital Contracts Platform</title>
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="../assets/images/Favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
</head>

<body id="kt_body" class="bg-body">
    <div class="d-flex flex-column flex-root">
        <div class="d-flex flex-column flex-lg-row flex-column-fluid align-items-center">
            <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center h-100">
                <!--begin::Content-->
                <div class="d-flex flex-column flex-center py-7 py-lg-15 px-5 px-md-15 w-100 "
                    style="background-color:#eff6ff;">
                    <!--begin::Logo-->
                    <a href="/metronic8/demo1/../demo1/index.php" class="mb-0 mb-lg-12">
                        <img alt="Logo" src="../assets/images/logo.png">
                    </a>
                    <h1 class="d-none d-lg-block fs-2qx fw-bolder text-center mb-7 " style="color: #0b3856;">Welcome to
                        Tothiq</h1>
                    <!--end::Text-->
                </div>
                <!--end::Content-->
            </div>
            <div
                class="d-block flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
                <div class="ps-10 align-middle">
                    <div
                        class="rounded  ps-10 p-lg-15 ms-auto h-100 d-flex align-items-center justify-content-center">
                        <div class="content_inr">
                            <div class="logo text-center pb-10">
                            </div>
                            <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="#">
                                <h2 class="mb-10">Log In to your Tothiq Account</h2>
                                <div class="fv-row mb-10">
                                    <label class="form-label fs-6 fw-bolder text-dark required">Username</label>
                                    <input class="form-control form-control-lg form-control-solid" type="text" name="ID"
                                        placeholder="Username" autocomplete="off" />
                                </div>
                                <div class="fv-row mb-10">
                                    <label class="form-label fs-6 fw-bolder text-dark required">Password</label>
                                    <input class="form-control form-control-lg form-control-solid" type="password"
                                        name="pw" placeholder="Password" autocomplete="off" />
                                </div>
                                <div class="d-flex justify-content-end align-items-center"> <a href="dashboard.php"
                                        class="btn btn-primary text-capitalize">Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>var hostUrl = "../assets/";</script>
    <script src="../assets/plugins/global/plugins.bundle.js"></script>
    <script src="../assets/js/scripts.bundle.js"></script>
    <script src="../assets/js/custom/authentication/sign-in/general.js"></script>
</body>

</html>