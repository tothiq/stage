<?php include("header.php") ?>

<div class="content d-flex flex-column p-0" id="kt_content">
  <div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
      <div id="kt_app_toolbar" class="app-toolbar py-8">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container px-3 d-flex flex-stack">
          <!--begin::Page title-->
          <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Reports</h1>
            <!--end::Title-->
          </div>
          <!--end::Page title-->
          <!--begin::Actions-->
          <div class="d-flex align-items-center gap-2 gap-lg-3">
          </div>
          <!--end::Actions-->
        </div>
        <!--end::Toolbar container-->
      </div>
      <div class="card">
        <div class="card-body">

        </div>
      </div>

    </div>
  </div>
</div>

<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
  <span class="svg-icon">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
      <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
      <path
        d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
        fill="black" />
    </svg>
  </span>
</div>

<script>
  ClassicEditor
    .create(document.querySelector('#ckeditor_1'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_2'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
  ClassicEditor
    .create(document.querySelector('#ckeditor_3'))
    .then(editor => {
      console.log(editor);
    })
    .catch(error => {
      console.error(error);
    });
</script>
<script>
  // $(document).ready(function () {
  //     $(".companyform").click(function () {
  //         var test = $(this).val();
  //         if(test =='yes'){
  //             $("div.desc2").hide();
  //             $("div.desc1").show();
  //         }else{
  //             $("div.desc1").hide();
  //             $("div.desc2").show();

  //         }
  //     });
  // });
  $("#business-tab").click(function () {
    $("div#myTabContent1").hide();
    $("div#myTabContent2").show();
  });
  $("#individual-tab").click(function () {
    $("div#myTabContent1").show();
    $("div#myTabContent2").hide();
  });
  $(document).ready(function () {
    $('#addresstype').on('change', function () {
      var demovalue = $(this).val();
      $("div.myDiv").hide();
      $("#show" + demovalue).show();
    });
    $('#addresstype1').on('change', function () {
      var demovalue1 = $(this).val();
      $("div.myDiv1").hide();
      $("#show" + demovalue1).show();
    });
  });
  // var profileborder = "border-danger";
  $(".userprofile").addClass("border-danger");
  // function changeuserborder() {
  //     $(".userprofile").removeClass(profileborder);
  //     var profileborder = "border-success";
  // }
  $("#userheaderchange").click(function () {
    $(".userprofile").removeClass("border-danger");
    $(".userprofile").addClass("border-success");
    $("#headererror").addClass("d-none");

  });
</script>
<script>
  $.fn.equalHeights = function () {
    var max_height = 0;
    $(this).each(function () {
      max_height = Math.max($(this).height(), max_height);
    });
    $(this).each(function () {
      $(this).height(max_height);
    });
  };

  $(document).ready(function () {
    $('.userdasboardbox ul li a .card').equalHeights();
  });
</script>

<?php include("footer.php") ?>