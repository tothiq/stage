<div class="footer py-4 d-flex flex-lg-column app-footer position-fixed bottom-0 end-0 w-100" id="kt_footer">
     <!--begin::Container-->
     <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-end">
         <!--begin::Copyright-->
         <div class="text-dark order-2 order-md-1">
             <span class="text-muted fw-bold me-1">© 2023</span>
             <a href="#" target="_blank" class="text-gray-800 text-hover-primary">tothiq.com</a>
         </div>
         <!--end::Copyright-->
     </div>
     <!--end::Container-->tothiq.com
 </div>
</div>



<script>
    var hostUrl = "../assets/";
</script>
<script src="../assets/plugins/global/plugins.bundle.js"></script>
<script src="../assets/js/scripts.bundle.js"></script>
<script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
<script src="../assets/js/custom/widgets.js"></script>
<script src="../assets/js/custom/apps/chat/chat.js"></script>
<script src="../assets/js/custom/modals/create-app.js"></script>
<script src="../assets/js/custom/modals/upgrade-plan.js"></script>
<script src="../assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
<script src="../assets/plugins/custom/ckeditor/ckeditor-inline.bundle.js"></script>
<script src="../assets/plugins/custom/ckeditor/ckeditor-balloon.bundle.js"></script>
<script src="../assets/plugins/custom/ckeditor/ckeditor-balloon-block.bundle.js"></script>
<script src="../assets/plugins/custom/ckeditor/ckeditor-document.bundle.js"></script>

</body>

</html>