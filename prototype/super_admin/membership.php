
<?php include("header.php") ?>
<div id="kt_app_toolbar" class="app-toolbar py-8">
    <!--begin::Toolbar container-->
    <div id="kt_app_toolbar_container" class="app-container px-6 d-flex flex-stack">
        <!--begin::Page title-->
        <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Membership
            </h1>
            <!--end::Title-->
        </div>
        <!--end::Actions-->
    </div>
    <!--end::Toolbar container-->
</div>
<div class="card  mx-5">
    <div class="card-body p-15">
        <div class="fv-row p-5 col-12">
            <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold text-center">
                <li class="nav-item">
                    <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1 active" data-bs-toggle="tab"
                        id="individual-tab" href="#Individualform">For Individual</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1" data-bs-toggle="tab"
                        id="business-tab" href="#Businessform">For Business</a>
                </li>
            </ul>
        </div>
        <div class="tab-content w-100 mt-5 mb-20" id="myTabContent11">
            <div class="tab-pane fade show active" id="Individualform" role="tabpanel">
                <div class="row">
                    <div class="col bg-light pt-15 pb-10 px-5 position-relative border border-primary rounded ">
                        <h2>Tothiq - Starter</h2>
                        <h5>Free</h5>
                        <ul>
                            <li>Free sign up</li>
                            <li>1 Free contract</li>
                            <li>Free contract templates</li>
                            <li>Add up to 2 parties per contract</li>
                            <li>Save contact on address book</li>
                            <li>Free contract storage</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_1"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm  btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                    <div class="col bg-light pt-10 pb-10 position-relative px-5 mx-15 border border-primary rounded">
                        <a href="#."><span
                                class="position-absolute top-0 start-50 translate-middle h-30px  badge fs-5 fw-light text-dark pt-2 badge-light-danger border border-2 border-danger">Random
                                Special - 20% OFF</span></a>
                        <span class="badge badge-light-warning text-dark">Best value Individual</span>
                        <h2>Tothiq - Basic</h2>
                        <p class="fs-4">KD 9.000/Year</p>
                        <ul>
                            <li>Free sign up</li>
                            <li>2 Free contracts</li>
                            <li>Free and Premium contract templates (limited)</li>
                            <li>Add up to 3 parties per contract</li>
                            <li>Save contacts on address book</li>
                            <li>Create Blank Contract</li>
                            <li>Free contract storage</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_2"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                    <div class="col bg-light pt-15 pb-10 position-relative px-5 border border-primary rounded">
                        <h2>Tothiq - Premium</h2>
                        <p class="fs-4">KD 19.000/Year</p>
                        <ul>
                            <li>Free sign up</li>
                            <li>3 Free contracts</li>
                            <li>Unlimited parties</li>
                            <li>Chat between parties</li>
                            <li>Unlimited contract templates</li>
                            <li>Save contacts on address book</li>
                            <li>Create Blank Contract</li>
                            <li>Free contract storage</li>
                            <li>Upload contract</li>
                            <li>View log</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_3"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade " id="Businessform" role="tabpanel">
                <div class="row">
                    <div class="col bg-light pt-15 pb-10 px-5 position-relative border border-primary rounded">
                        <h2>Free</h2>
                        <p class="fs-4">KD 0.000/Year</p>
                        <ul>
                            <li>Free sign up</li>
                            <li>1 Free contract</li>
                            <li>Free contract templates</li>
                            <li>Add up to 2 parties per contract</li>
                            <li>Save contact on address book</li>
                            <li>Free contract storage</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_4"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                    <div class="col bg-light pt-10 pb-10 position-relative px-5 mx-15 border border-primary rounded">
                        <a href="#."><span
                                class="position-absolute top-0 start-50 translate-middle h-30px  badge fs-5 fw-light text-dark pt-2 badge-light-danger border border-2 border-danger">Random
                                Special - 20% OFF</span></a>
                        <span class="badge badge-light-warning text-dark">Best value Individual</span>
                        <h2>Basic</h2>
                        <p class="fs-4">KD 49.000/Year</p>
                        <ul>
                            <li>Free sign up</li>
                            <li>2 Free contracts</li>
                            <li>Free and Premium contract templates (limited)</li>
                            <li>Add up to 3 parties per contract</li>
                            <li>Save contacts on address book</li>
                            <li>Create Blank Contract</li>
                            <li>Free contract storage</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_5"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                    <div class="col bg-light pt-15 pb-10 position-relative px-5 border border-primary rounded">
                        <h2>Premium</h2>
                        <p class="fs-4">KD 89.000/Year</p>
                        <ul>
                            <li>Free sign up</li>
                            <li>3 Free contracts</li>
                            <li>Unlimited parties</li>
                            <li>Chat between parties</li>
                            <li>Unlimited contract templates</li>
                            <li>Save contacts on address book</li>
                            <li>Create Blank Contract</li>
                            <li>Free contract storage</li>
                            <li>Upload contract</li>
                            <li>View log</li>
                        </ul>
                        <a id="kt_drawer_chat_toggle_6"><button
                                class="position-absolute top-100 start-50 translate-middle btn-sm  btn btn-warning text-dark">Edit
                                Membership</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="kt_drawer_chat_1" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_1"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end p-0">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
    <!--end::Messenger-->
</div>
<div id="kt_drawer_chat_2" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_2"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end p-0">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
    <!--end::Messenger-->
</div>
<div id="kt_drawer_chat_3" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_3"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end p-0">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
    <!--end::Messenger-->
</div>
<div id="kt_drawer_chat_4" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_4"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties (For Unlimited insert
                            value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number Authorized Person (For Unlimited
                            insert value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="kt_drawer_chat_5" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_5"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties (For Unlimited insert
                            value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number Authorized Person (For Unlimited
                            insert value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="kt_drawer_chat_6" class="bg-body" data-kt-drawer="true" data-kt-drawer-name="chat"
    data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'md': '500px'}"
    data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_drawer_chat_toggle_6"
    data-kt-drawer-close="#kt_drawer_chat_close">
    <!--begin::Messenger-->
    <div class="card w-100 rounded-0" id="kt_drawer_chat_messenger">
        <div class="card-body">
            <div class="modal-header d-flex justify-content-end">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="kt_drawer_chat_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1"
                                transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)"
                                fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body">
                <div class="align-center w-100 ">
                    <h2>Membership Management</h2>
                    <div class="my-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Name</label>
                        <input type="email" class="form-control form-control-solid" value="Free" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Membership Amount</label>
                        <input type="email" class="form-control form-control-solid" value="KWD 0.000" />
                    </div>
                    <div class="mb-10 ">
                        <input class="form-check-input" type="checkbox" value="" checked>
                        <label class="form-check-label" for="flexCheckChecked">
                            Highlighted Text
                        </label>
                        <input type="email" class="form-control form-control-solid mt-3"
                            value="Get Start with Free Account" />
                    </div>
                    <div class="mb-10 ">
                        <h5>Payment Gateway</h5>
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Checkout Process Required
                        </label>
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Contract</label>
                        <input type="email" class="form-control form-control-solid" value="3" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number of Parties (For Unlimited insert
                            value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="1" />
                    </div>
                    <div class="mb-10">
                        <label for="exampleFormControlInput1" class="form-label">Number Authorized Person (For Unlimited
                            insert value 0000)</label>
                        <input type="email" class="form-control form-control-solid" value="" />
                    </div>
                    <h5>Other Features</h5>
                    <div class="mb-3 mt-5">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Chat between Parties
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Edit Templates
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Address Book
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Create Blank Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Upload Contract
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View log
                        </label>
                    </div>
                    <div class="mb-3">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            View Contract Storage (Just Text)
                        </label>
                    </div>
                    <div class="mb-10">
                        <input class="form-check-input" type="checkbox" value="">
                        <label class="form-check-label" for="flexCheckChecked">
                            Free Sign Up(Just Text)
                        </label>
                    </div>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary" href="#." type="button">Update Membership</a>
                </div>
            </div>
        </div>
    </div>
</div>



<!--end::Root-->

<!-- Modal Invite Contact  -->
<div class="modal fade" id="exampleModalNoti" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-between">
                <h3 class="text-capitalize">Add New Notification</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5 mt-5">
                        <label class="form-check-label" for="flexCheckChecked">Select Notification
                            Type</label>
                        <select class="form-select form-select-sm form-select-solid" data-control="select2"
                            data-placeholder="Select an option">
                            <option></option>
                            <option value="1">Renewal Notification</option>
                            <option value="2">New Notification</option>
                        </select>
                    </div>
                    <div class="col-5 mt-5">
                        <label class="form-check-label" for="flexCheckChecked">Select Recipient</label>
                        <select class="form-select form-select-sm form-select-solid" data-control="select2"
                            data-placeholder="Select an option">
                            <option></option>
                            <option value="1">Everyone</option>
                            <option value="2">Individual User</option>
                            <option value="3">Business User</option>
                        </select>
                    </div>
                    <div class="col-2 position-relative">
                        <div class="position-absolute start-75 bottom-0">
                            <i class="fas fa-camera-retro fa-2x text-dark"></i>
                        </div>
                        <img src="./images/img_default.jpg" class="w-100" alt="">
                    </div>
                </div>
                <div class="mb-5">
                    <label for="exampleFormControlInput1" class="form-label">Notification Title</label>
                    <input type="email" class="form-control form-control-solid" placeholder="Type Here" />
                </div>
                <div class="form-group mb-5">
                    <label for="exampleFormControlInput1" class="form-label">Description</label>
                    <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="4"
                        placeholder="Type Here"></textarea>
                </div>
                <div class="pt-3 text-end">
                    <a type="button" class="btn btn-primary me-5" href="notification.php">Send</a>
                    <a type="button" class="btn btn-danger" href="#.">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2"
    tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="form w-100">
                    <div class=" d-flex flex-wrap">
                        <div class="fv-row p-5 col-12">
                            <label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
                            <input class="form-control form-control-lg form-control-solid" type="email"
                                placeholder="Email Address" autocomplete="off" />
                        </div>
                    </div>
                    <div class="text-center btncolorblue pt-10">
                        <a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Invite Contact  -->

<!--end::Modals-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path
                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                fill="black" />
        </svg>
    </span>
</div>

<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function () {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function () {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function () {
        $('#addresstype').on('change', function () {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function () {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function () {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function () {
        var max_height = 0;
        $(this).each(function () {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function () {
            $(this).height(max_height);
        });
    };

    $(document).ready(function () {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer.php") ?>