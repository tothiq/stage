<?php include("header.php") ?>

<div class="content d-flex flex-column p-0 " id="kt_content">
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <div id="kt_content_container" class="container-xxl ">
            <div id="kt_app_toolbar" class="app-toolbar py-8">
                <!--begin::Toolbar container-->
                <div id="kt_app_toolbar_container" class="app-container px-3 d-flex flex-stack">
                    <!--begin::Page title-->
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                            Settings</h1>
                        <!--end::Title-->
                    </div>
                </div>
                <!--end::Toolbar container-->
            </div>
            <div class="kt_content_containe_inr d-flex justify-content-between my-5 card">
                <div class="card-body">

                    <div class="fv-row  col-12 ">
                        <ul
                            class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold text-center">
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1 active"
                                    data-bs-toggle="tab" id="individual-tab" href="#general">Maintenance
                                    Mode</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1"
                                    data-bs-toggle="tab" id="business-tab" href="#membership">Application
                                    Setting</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1"
                                    data-bs-toggle="tab" id="business-tab" href="#BusinessUser">Company
                                    Setting</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-active-primary fs-4 fw-bolder text-dark py-1"
                                    data-bs-toggle="tab" id="business-tab" href="#custome">Payment
                                    Gateway</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content ms-2 mb-20" id="myTabContent">
                <div class="tab-pane fade active show card" id="general" role="tabpanel">
                    <div class=" card-body px-10 ">
                        <div class="d-flex justify-content-between">
                            <h1 class="text-primary">Maintenance Mode</h1>
                            <a href="#" type="button"><BUtton class="btn btn-primary btn-sm" disabled>
                                    Save</BUtton></a>
                        </div>
                        <div class="row my-7">
                            <div class="col-3">
                                <label class="form-check-label" for="flexSwitchChecked">
                                    Maintenance Enable
                                </label>
                                <select class="form-select form-select-md form-select-solid" data-control="select2"
                                    data-placeholder="Inactive" data-allow-clear="true" multiple="multiple">
                                    <option></option>
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="settings_me_block my-10 d-flex">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Individual User
                                </label>
                            </div>
                            <div class="form-check mx-10">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Business User
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Business Admin
                                </label>
                            </div>
                        </div>
                        <h5>Maintenance mode web panel Image</h5>
                        <a href="#." class="text-center "><i
                                class="fas fa-image text-dark bg-secondary py-20 w-100 fs-1"></i></a>
                        <input type="text" class="form-control my-10" placeholder="Maintenance page title" />
                        <textarea class="form-control rounded-0" id="exampleFormControlTextarea1" rows="7"
                            placeholder="Maintenance page contain"></textarea>
                    </div>
                </div>
                <div class="tab-pane fade card" id="membership" role="tabpanel">
                    <div class=" card-body">
                        <div class="d-flex justify-content-between mb-5">
                            <h1 class="text-primary">Application Settings</h1>
                            <a href="#" type="button"><BUtton class="btn btn-primary btn-sm" disabled>
                                    Save</BUtton></a>
                        </div>
                        <h6>General Mobile Configuration</h6>
                        <div class="row my-5">
                            <div class="col-6">
                                <label for="exampleFormControlInput1" class="form-label">Production Server
                                    API-End Point</label>
                                <input type="text" class="form-control form-control-solid"
                                    placeholder="Enter push notificaiton key" />
                            </div>
                            <div class="col-6">
                                <label for="exampleFormControlInput1" class="form-label">Pre-Production
                                    Server API-End Point</label>
                                <input type="text" class="form-control form-control-solid"
                                    placeholder="Enter push notificaiton key" />
                            </div>
                        </div>
                        <div class="row my-5">
                            <div class="col-6">
                                <label for="exampleFormControlInput1" class="form-label">Test Server API-End
                                    Point</label>
                                <input type="text" class="form-control form-control-solid"
                                    placeholder="Enter push notificaiton key" />
                            </div>
                        </div>
                        <div class="separator border-3 my-10"></div>
                        <div class="d-flex">
                            <div class="col-6  pe-15">
                                <h5 class="text-primary">iPhone Mobile Application Setting</h5>
                                <label class="form-check-label mt-5 mb-3" for="flexSwitchChecked">
                                    Application Update
                                </label>
                                <select class="form-select form-select-md form-select-solid" data-control="select2"
                                    data-placeholder="Inactive" data-allow-clear="true" multiple="multiple">
                                    <option></option>
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                                <h6 class="mt-5 mb-3">Update Mandatory</h6>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault"
                                        id="flexRadioDefault1">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check my-3">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault"
                                        id="flexRadioDefault1">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        No
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="exampleFormControlInput1" class="form-label">iPhone
                                            Current Version
                                        </label>
                                        <input type="text" class="form-control form-control-solid"
                                            placeholder="23.0.4" />
                                    </div>
                                    <div class="col-6">
                                        <label for="exampleFormControlInput1" class="form-label">iPhone New
                                            Version
                                        </label>
                                        <input type="text" class="form-control form-control-solid" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 ">
                                <h5 class="text-primary">Android Mobile Application Setting</h5>
                                <label class="form-check-label mt-5 mb-3" for="flexSwitchChecked">
                                    Application Update
                                </label>
                                <select class="form-select form-select-md form-select-solid" data-control="select2"
                                    data-placeholder="Inactive" data-allow-clear="true" multiple="multiple">
                                    <option></option>
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                                <h6 class="mt-5 mb-3">Update Mandatory</h6>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault"
                                        id="flexRadioDefault1">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Yes
                                    </label>
                                </div>
                                <div class="form-check my-3">
                                    <input class="form-check-input" type="radio" name="flexRadioDefault"
                                        id="flexRadioDefault1">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        No
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="exampleFormControlInput1" class="form-label">Android
                                            Current Version
                                        </label>
                                        <input type="text" class="form-control form-control-solid"
                                            placeholder="21.0.4" />
                                    </div>
                                    <div class="col-6">
                                        <label for="exampleFormControlInput1" class="form-label">Android New
                                            Version
                                        </label>
                                        <input type="text" class="form-control form-control-solid" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade card" id="BusinessUser" role="tabpanel">
                    <div class=" card-body px-10">
                        <div class="d-flex justify-content-between">
                            <h1 class="text-primary">Company Setting</h1>
                            <a href="#" type="button"><BUtton class="btn btn-primary btn-sm" disabled>
                                    Save</BUtton></a>
                        </div>
                        <h5>Logo Management</h5>
                        <div class="d-flex mt-7">
                            <div>
                                <p>Web Panel Header Logo</p>
                                <img src="../assets/images/img_default.jpg" class="h-75 w-25" alt="">
                            </div>
                            <div>
                                <p>Login Page Logo</p>
                                <img src="../assets/images/img_default.jpg" class="h-75 w-25" alt="">
                            </div>
                        </div>
                        <div class="my-10">
                            <label for="exampleFormControlInput1" class="form-label">WebPanel Title
                                Text</label>
                            <input type="text" class="form-control form-control-solid w-50" placeholder="Type Here" />
                        </div>
                        <div>
                            <label for="exampleFormControlInput1" class="form-label">WebPanel Copyright
                                Text</label>
                            <input type="text" class="form-control form-control-solid w-50" placeholder="Type Here" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade card" id="custome" role="tabpanel">
                    <div class=" card-body">
                        <div class="d-flex justify-content-between">
                            <h1 class="text-primary">Payment Gateway</h1>
                            <a href="#" type="button"><BUtton class="btn btn-primary btn-sm" disabled>
                                    Save</BUtton></a>
                        </div>
                        <div class="my-10">
                            <label for="exampleFormControlInput1" class="form-label">Payment Gateway</label>
                            <input type="text" class="form-control form-control-solid w-50" placeholder="Knet" />
                        </div>
                        <div class="my-10">
                            <label for="exampleFormControlInput1" class="form-label">Transportal ID</label>
                            <input type="text" class="form-control form-control-solid w-50" placeholder="Enter ID" />
                        </div>
                        <div class="my-10">
                            <label for="exampleFormControlInput1" class="form-label">Transportal
                                Password</label>
                            <input type="password" class="form-control form-control-solid w-50"
                                placeholder="Enter Password" />
                        </div>
                        <div class="my-10">
                            <label for="exampleFormControlInput1" class="form-label">Terminal Resource
                                Key</label>
                            <input type="text" class="form-control form-control-solid w-50"
                                placeholder="Enter Resource Key" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path
                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                fill="black" />
        </svg>
    </span>
</div>


<script>
    ClassicEditor
        .create(document.querySelector('#ckeditor_1'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_2'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
    ClassicEditor
        .create(document.querySelector('#ckeditor_3'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
    // $(document).ready(function () {
    //     $(".companyform").click(function () {
    //         var test = $(this).val();
    //         if(test =='yes'){
    //             $("div.desc2").hide();
    //             $("div.desc1").show();
    //         }else{
    //             $("div.desc1").hide();
    //             $("div.desc2").show();

    //         }
    //     });
    // });
    $("#business-tab").click(function () {
        $("div#myTabContent1").hide();
        $("div#myTabContent2").show();
    });
    $("#individual-tab").click(function () {
        $("div#myTabContent1").show();
        $("div#myTabContent2").hide();
    });
    $(document).ready(function () {
        $('#addresstype').on('change', function () {
            var demovalue = $(this).val();
            $("div.myDiv").hide();
            $("#show" + demovalue).show();
        });
        $('#addresstype1').on('change', function () {
            var demovalue1 = $(this).val();
            $("div.myDiv1").hide();
            $("#show" + demovalue1).show();
        });
    });
    // var profileborder = "border-danger";
    $(".userprofile").addClass("border-danger");
    // function changeuserborder() {
    //     $(".userprofile").removeClass(profileborder);
    //     var profileborder = "border-success";
    // }
    $("#userheaderchange").click(function () {
        $(".userprofile").removeClass("border-danger");
        $(".userprofile").addClass("border-success");
        $("#headererror").addClass("d-none");

    });
</script>
<script>
    $.fn.equalHeights = function () {
        var max_height = 0;
        $(this).each(function () {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function () {
            $(this).height(max_height);
        });
    };

    $(document).ready(function () {
        $('.userdasboardbox ul li a .card').equalHeights();
    });
</script>

<?php include("footer.php") ?>