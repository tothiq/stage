<?php include("header.php") ?>
			<div class="content d-flex flex-column p-0 mb-20" id="kt_content">
				<div class="" id="">
					<div id="kt_content_container">
						<div id="kt_app_toolbar" class="app-toolbar py-8">
							<!--begin::Toolbar container-->
							<div id="kt_app_toolbar_container" class="app-container px-7 d-flex flex-stack">
								<!--begin::Page title-->
								<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
									<!--begin::Title-->
									<h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Create Template</h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center gap-2 gap-lg-3">
									<!--begin::Secondary button-->
									
									<!--end::Secondary button-->
									<!--begin::Primary button-->
									
									<!--end::Primary button-->
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Toolbar container-->
						</div>
						<div class="kt_content_containe_inr d-flex justify-content-between m-5 card">
							<div class="card-body">
								<div class="d-flex justify-content-between align-items-center">
									<h4>Template Information</h4>
									<div>
										<a href="template_create_contract.php"
											class="btn btn-primary me-3 btn-sm">Continue</a>
										<a href="template.php"
											class="btn btn-sm btn-outline btn-outline-dashed btn-outline-primary btn-active-light-primary">Cancel</a>
									</div>
								</div>
								<div class="row ">
									<div class="col-9">
										<div class="fv-row mb-10 ">
											<label class="form-label fs-6 fw-bolder text-dark w-100 mt-5">
												<div class="d-flex justify-content-between">
													<h5>Template Title</h5>
													<p>250 Characters</p>
												</div>
											</label>
											<input
												class="form-control form-control-lg form-control-solid border-secondary"
												type="text" name="ID" placeholder="Enter Template Title"
												autocomplete="off" />
										</div>
										<div class="fv-row mb-10 ">
											<label class="form-label fs-6 fw-bolder text-dark w-100">Category</label>
											<input
												class="form-control form-control-lg form-control-solid border-secondary"
												type="text" name="ID" placeholder="Enter Category"
												autocomplete="off" />
										</div>
										<div>
											<h4>Template Availability</h4>
											<div class="d-flex mt-5 mb-10">
												<div>
													<h6>Individual Membership</h6>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Free Membership
														</label>
													</div>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Basic Membership
														</label>
													</div>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Premium Membership
														</label>
													</div>
												</div>
												<div class="ms-20">
													<h6>Business Membership</h6>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Free Membership
														</label>
													</div>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Basic Membership
														</label>
													</div>
													<div class="form-check mt-5">
														<input class="form-check-input" type="checkbox" value=""
															id="flexCheckChecked">
														<label class="form-check-label" for="flexCheckChecked">
															Premium Membership
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-3 p-10">
										<h4>Template Cover Image</h4>
										<p class="mx-3">400PX BY 500PX</p>
										<img src="../assets/images/img_default.jpg"
											class="w-100 border border-dashed border-2 border-secondary" alt="">
										<a href="#." class="btn btn-primary mt-7 mx-3 btn-sm">Click to Upload Image</a>
									</div>
								</div>
								<div>
									<h4 class="mb-5">Tempate</h4>
									<div class="form-group">
										<textarea class="form-control rounded" id="exampleFormControlTextarea1" rows="7"
											placeholder="Enter Description..."></textarea>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

	
	<div class="modal fade " id="invitecontactmodal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2"
		tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xs">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Invite User</h1>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<form class="form w-100">
						<div class=" d-flex flex-wrap">
							<div class="fv-row p-5 col-12">
								<label class="form-label required fs-6 fw-bolder text-dark">Email Address</label>
								<input class="form-control form-control-lg form-control-solid" type="email"
									placeholder="Email Address" autocomplete="off" />
							</div>
						</div>
						<div class="text-center btncolorblue pt-10">
							<a href="contacts.php" class="btn btncolorblues mb-5">Invite User</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END Modal Invite Contact  -->

	<!--end::Modals-->
	<!--begin::Scrolltop-->
	<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
		<span class="svg-icon">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
				<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)"
					fill="black" />
				<path
					d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
					fill="black" />
			</svg>
		</span>
	</div>
	
	<script>
		ClassicEditor
			.create(document.querySelector('#ckeditor_1'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
		ClassicEditor
			.create(document.querySelector('#ckeditor_2'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
		ClassicEditor
			.create(document.querySelector('#ckeditor_3'))
			.then(editor => {
				console.log(editor);
			})
			.catch(error => {
				console.error(error);
			});
	</script>
	<script>
		$("#business-tab").click(function () {
			$("div#myTabContent1").hide();
			$("div#myTabContent2").show();
		});
		$("#individual-tab").click(function () {
			$("div#myTabContent1").show();
			$("div#myTabContent2").hide();
		});
		$(document).ready(function () {
			$('#addresstype').on('change', function () {
				var demovalue = $(this).val();
				$("div.myDiv").hide();
				$("#show" + demovalue).show();
			});
			$('#addresstype1').on('change', function () {
				var demovalue1 = $(this).val();
				$("div.myDiv1").hide();
				$("#show" + demovalue1).show();
			});
		});
		// var profileborder = "border-danger";
		$(".userprofile").addClass("border-danger");
		$("#userheaderchange").click(function () {
			$(".userprofile").removeClass("border-danger");
			$(".userprofile").addClass("border-success");
			$("#headererror").addClass("d-none");

		});
	</script>
	<script>
		$.fn.equalHeights = function () {
			var max_height = 0;
			$(this).each(function () {
				max_height = Math.max($(this).height(), max_height);
			});
			$(this).each(function () {
				$(this).height(max_height);
			});
		};

		$(document).ready(function () {
			$('.userdasboardbox ul li a .card').equalHeights();
		});
	</script>
<?php include("footer.php") ?>